#!/bin/bash

cd /var/www/erms

composer install

php artisan migrate:install --no-interaction
php artisan migrate:refresh --no-interaction
php artisan db:seed --no-interaction

exit 0