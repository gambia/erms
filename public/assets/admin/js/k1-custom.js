;(function($, window, undefined){

    "use strict";

    $(document).ready(function() {
                // Wysiwyg Editor
        if($.isFunction($.fn.wysihtml5)) {
            $(".wysihtml5").each(function(i, el){
                $(".wysihtml5").wysihtml5({
                    "color": false,
                });
                //$(".wysihtml5-sandbox").wysihtml5_size_matters();
            });
        }
    });

})(jQuery, window);