#!/bin/bash

sudo echo '@erms ubuntu@erms' > /etc/postfix/virtual_aliases
sudo echo 'erms    mytransportname:' > /etc/postfix/transport

sudo postmap /etc/postfix/virtual_aliases
sudo postmap /etc/postfix/transport

echo 'mytransportname   unix  -       n       n       -       -       pipe  flags=FR user=ubuntu argv=php /var/www/erms/artisan mail:catcher $original_recipient' >> /etc/postfix/master.cf
echo 'transport_maps = hash:/etc/postfix/transport' >> /etc/postfix/main.cf
echo 'virtual_alias_maps = hash:/etc/postfix/virtual_aliases' >> /etc/postfix/main.cf

sudo service postfix restart

exit 0