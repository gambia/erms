#SwaziCMS

##Setup
You need
- mcrypt (apt-get install php5-mcrypt on debian or ubuntu)
- PDO Mysql driver (apt-get install php5-mysql)

Setup db with name SwaziCMS
- Change app/config/database.php with your credentials (do not commit)
- git update-index --assume-unchanged app/config/database.php


Debug is set to true in app/config/app.php. Should be set to false when we go to prod.

Useful php.ini settings:
- display_errors=On

In case you're getting strange errors while running migrations, do:

./composer.phar dump-autoload

To seed production data run:

php artisan db:seed --class=ProductionSeeder

##Testing
To setup tests create a database epcifically for this purpose (e.g. swazicms-testing) and tweak the configuration in app/config/testing/database.php so that the app can authenticate.

php artisan migrate --env=testing

vendor/phpunit/phpunit/phpunit

##Mandrill

Username/Password: sedliak.milan@gmail.com / WXmyU5yO6ENXkTxo

Key: a8ATfTLAhEP58RgGgabCRA

## K1CMS Domain (GoDaddy.com)

User/pass: felc0011 / Z123x456c789

## DB access

su postgres
psql swazicms
