<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

/**
 * @deprecated DEPRECATED
 * */
class Activity extends Eloquent {
    public static function log($data)
    {
        if(!Auth::Guest()){
            if(!isset($data['user_id'])){
                $data['user_id'] = Auth::User()->id;
            }

            if(!isset($data['user_email'])){
                $data['user_email'] = Auth::User()->email;
            }

            if(!isset($data['user_name'])){
                $data['user_name'] = Auth::User()->name;
            }
        }

        if(!isset($data['user_agent'])){
            $data['user_agent'] = isset($_SERVER['HTTP_USER_AGENT']) ?
                $_SERVER['HTTP_USER_AGENT'] : 'CLI';
        }

        return History::create($data);
    }
}
