<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/',  ['as' => 'home', 'uses' => 'HomeController@dashboard']);

Route::get('login', 'UserSessionsController@create');
Route::post('login', 'UserSessionsController@store');
Route::get('logout', 'UserSessionsController@destroy');

Route::get('forgotten_password', 'ForgottenPasswordsController@create');
Route::post('forgotten_password', 'ForgottenPasswordsController@store');

Route::get(
    'users/change_password',
    [
        'as' => 'users.forgottenPassword',
        'uses' => 'UsersController@changePassword'
    ]
);

Route::get('pages/download/{id}', ['as' => 'pages.download', 'uses' => 'PagesController@downloadFile']);

Route::group(array('before' => 'auth'), function()
{

    Route::resource('users', 'UsersController', array('except' => 'destroy'));
    Route::get('users/{users}/change_password', ['as' => 'users.changePassword', 'uses' => 'UsersController@changePassword']);
    Route::put('users/{users}/change_password', ['as' => 'users.changePassword', 'uses' => 'UsersController@handleChangePassword']);
    Route::put('users/{users}/block', ['as' => 'users.block', 'uses' => 'UsersController@block']);
    Route::put('users/{users}/unblock', ['as' => 'users.unblock', 'uses' => 'UsersController@unblock']);

    Route::resource('securitygroups','SecurityGroupsController', array('except' => 'destroy'));
    Route::post('cases/{id}/comment', ['as' => 'cases.postComment', 'uses' => 'CasesController@postComment']);
    Route::post('pages/{type}/{id}/comment', ['as' => 'pages.postComment', 'uses' => 'PagesController@postComment']);

    Route::get('records/create/{case_id}', ['as' => 'records.create', 'uses' => 'RecordsController@create']);
    Route::post('records/create/{case_id}', ['as' => 'records.store', 'uses' => 'RecordsController@store']);
    Route::post('records/{id}/attachFile', ['as' => 'records.attachFile', 'uses' => 'RecordsController@attachFile']);
    Route::put('records/{id}/chunk_upload_finished', ['as' => 'records.chunk_upload_finished', 'uses' => 'RecordsController@chunkUploadFinished']);
    Route::get('records/download/{id}', ['as' => 'records.download', 'uses' => 'RecordsController@downloadFile']);
    Route::get('records/deleteFile/{id}', ['as' => 'records.deleteFile', 'uses' => 'RecordsController@deleteFile']);
    Route::delete('records/{id}', ['as' => 'records.destroy', 'uses' => 'RecordsController@destroy']);

    Route::resource('projects',     'ProjectsController',       array('except' => 'destroy'));
    Route::get('projects/{id}/{direction}', ['as' => 'projects.move', 'uses' => 'ProjectsController@move']);

    Route::resource('cases',        'CasesController');
    Route::resource('records',      'RecordsController',        array('except' => 'store', 'create', 'index', 'destroy'));
    Route::resource('clients',      'ClientsController',        array('except' => 'destroy'));
    Route::resource('documentation',      'DocumentationController');

    Route::post('documentation/{id}/attachFile', ['as' => 'documentation.attachFile', 'uses' => 'DocumentationController@attachFile']);
    Route::get('documentation/download/{id}', ['as' => 'documentation.download', 'uses' => 'DocumentationController@downloadFile']);
    Route::get('documentation/deleteFile/{id}', ['as' => 'documentation.deleteFile', 'uses' => 'DocumentationController@deleteFile']);
    Route::get('documentation/{id}/{direction}', ['as' => 'documentation.move', 'uses' => 'DocumentationController@move']);

    Route::get('statistics', ['as' => 'statistics.index', 'uses' => 'StatisticsController@index']);

    Route::get('pages/deleteFile/{id}', ['as' => 'pages.deleteFile', 'uses' => 'PagesController@deleteFile']);
    Route::get('pages/{type}', ['as' => 'pages.index', 'uses' => 'PagesController@index']);
    Route::get('pages/{type}/projects', ['as' => 'pages.projects', 'uses' => 'PagesController@getProjectsForPage']);
    Route::get('pages/{type}/create', ['as' => 'pages.create', 'uses' => 'PagesController@create']);
    Route::post('pages/{type}', ['as' => 'pages.store', 'uses' => 'PagesController@store']);
    Route::get('pages/{type}/{resource}', ['as' => 'pages.show', 'uses' => 'PagesController@show']);
    Route::get('pages/{type}/{resource}/edit', ['as' => 'pages.edit', 'uses' => 'PagesController@edit']);
    Route::put('pages/{type}/{resource}', ['as' => 'pages.update', 'uses' => 'PagesController@update']);
    Route::post('pages/{type}/{id}/attachFile', ['as' => 'pages.attachFile', 'uses' => 'PagesController@attachFile']);
    Route::put('pages/{type}/{id}/chunk_upload_finished', ['as' => 'pages.chunk_upload_finished', 'uses' => 'PagesController@chunkUploadFinished']);

    Route::delete('pages/{id}', ['as' => 'pages.destroy', 'uses' => 'PagesController@destroy']);

    Route::resource('search',   'SearchController',     array('only' => 'index'));

    // visible only to super admins
    Route::resource('activity', 'ActivityController', array('except' => 'destroy'));
});

Event::listen('illuminate.query', function($query, $bindings, $time, $name)
{
    $data = compact('bindings', 'time', 'name');

    // Format binding data for sql insertion
    foreach ($bindings as $i => $binding)
    {
        if ($binding instanceof \DateTime)
        {
            $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
        }
        else if (is_string($binding))
        {
            $bindings[$i] = "'$binding'";
        }
    }

    // Insert bindings into query
    $query = str_replace(array('%', '?'), array('%%', '%s'), $query);
    $query = vsprintf($query, $bindings);

    Log::debug($query . ";");
});

/* FRONT END ROUTES */
Route::get('{clientKey}',                           array('as'=>'clientHome',    'uses'=>'ClientHomeController@index'));
Route::get('{clientKey}/article/{slug}',       array('as'=>'clientArticle', 'uses'=>'ClientHomeController@article'));
Route::get('{clientKey}/search',       array('as'=>'clientSearch', 'uses'=>'ClientHomeController@search'));
Route::get('{clientKey}/contact',       array('as'=>'clientContact', 'uses'=>'ClientHomeController@contact'));
Route::post('{clientKey}/contact',       array('as'=>'clientContact', 'uses'=>'ClientHomeController@contactSubmit'));
Route::get('{clientKey}/{type}',       array('as'=>'clientHome',    'uses'=>'ClientHomeController@index'));
Route::get('{clientKey}/{type}/{page_number}',       array('as'=>'clientHome',    'uses'=>'ClientHomeController@index'));

