@extends('layouts.client')

@section('content')
<h2>Change Password for {{ $user->getName() }}</h2>
<br />

<div class="row">
    <div class="col-md-6">

    @if ($errors->has())
    <div class="alert alert-danger">
        <h4>Please fix following errors:</h4>
        <ul>
        @foreach ($errors->all('<li>:message</li>') as $error)
            {{ $error }}
        @endforeach
        </ul>
    </div>
    @endif

    {{ Form::model($user, array('route' => array('users.changePassword', $user->id), 'class'=>'form-horizontal form-groups-bordered', 'method' => 'put', 'files' => true)) }}

        @if (!$passwordForgotten)
        <div class="row">
            <div class="col-md-12">
            {{ Form::label('old_password','Old Password:', array('class'=>'control-label')) }}<br />
            {{ Form::password('old_password', array('class'=>'form-control input-lg')) }}
            </div>
        </div>
        <br />
        @endif
        <div class="row">
            <div class="col-md-12">
            {{ Form::label('password','New Password:', array('class'=>'control-label')) }}<br />
            {{ Form::password('password', array('class'=>'form-control input-lg')) }}
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
            {{ Form::label('password_confirmation','Confirm New Password:', array('class'=>'control-label')) }}<br />
            {{ Form::password('password_confirmation', array('class'=>'form-control input-lg')) }}
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">

                {{ Form::submit('Change Password', array('class'=>'btn btn-success pull-right')) }}
            </div>
        </div>
    {{ Form::close() }}
    </div>
</div>

@endsection
