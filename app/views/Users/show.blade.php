@extends('layouts.client')

@section('content')

@include('Users/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-8 col-sm-7 col-xs-6">
        <h1>
            {{ $user->getName() }} ({{ $user->job_title }})
        </h1>
    </div>

    <div class="col-md-4 col-sm-5 col-xs-6 actionbar">
        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary"><i class="entypo-pencil"></i>Edit Profile</a>


        @if (Auth::user()->isClientAdmin() || Auth::user()->isAdmin())
            @if ($canBlock)
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-danger blockUserModalBtn" data-toggle="modal" data-target="#myModal">
                Block Account
            </button>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    Are you sure you want to block this account?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" id="blockUser" data-link="{{ action('users.block', $user->id) }}" class="btn btn-danger hidden">
                        Block Account
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <button type="button" id="unblockUser" data-link="{{ action('users.unblock', $user->id) }}" class="btn btn-info hidden">
                Unblock Account
            </button>
            @endif
        @endif

        
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-3">
        <a href="{{ route('users.edit', $user->id) }}" class="profile-picture">
            <img src="{{$user->getAvatarLink('/assets/admin/images/member.jpg')}}" class="img-circle" width="115px" height="115px">
        </a><br /><br />
    </div>

    <div class="col-md-5 col-sm-5 profile-projects">
        <h4>Projects</h4>
        @if ($user->profileProjects()->count())
            @foreach ($user->profileProjects() as $project)
                <div class="btn-row">
                    <a href="{{ action('projects.show', $project->project_key) }}">{{ $project->project_key }} - {{ $project->name }}</a>
                    @if (count($project->children))
                    <div class="btn-group">
                        <a data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            @foreach ($project->children as $p)
                            <li>
                                <a href="{{ URL::route('projects.show', $p->project_key) }}">
                                    {{ $p->project_key }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div><br>
            @endforeach
        @else
            <p>User doesn't have any assigned projects yet.</p>
        @endif
        <br /><br />
    </div>

    <div class="col-md-4 col-sm-4">
        <h4>Records &amp; Cases</h4>

        <h5><i class="entypo-calendar"></i> Created records: {{ $user->createdRecordsCount() }}</h5>
        <h5><i class="entypo-calendar"></i> Closed cases: {{ $user->closedCasesCount() }}</h5>

        <br>

        <h4>Security Groups</h4>
        <p>
            @if ($user->securityGroups->count())
                @foreach ($user->securityGroups as $sg)
                    {{ $sg->name }}<br>
                @endforeach
            @else
                @if ($user->isClientAdmin() || $user->isAdmin())
                    <strong>User is administrator.</strong>
                @else
                    User isn't in any of the available Security Groups.
                @endif
            @endif
        </p><br /><br />
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <h2>Recent Activity</h2>
        @include ('includes/admin/activity-feed')
    </div>
</div>

  <script type="text/javascript">
    $(document).ready(function(){
        var blocked = {{ $user->is_blocked ? 'true' : 'false' }};

        var blockButton = $('#blockUser'),
            unblockButton = $('#unblockUser'),
            blockModalBtn = $('.blockUserModalBtn');
        

        if(!blockButton || !unblockButton) return;

        blockButton.click(function(){
            $.ajax({
                type: 'PUT',
                url: blockButton.attr('data-link'),
                success: function(){
                    blockButton.addClass('hidden');
                    unblockButton.removeClass('hidden');
                    blockModalBtn.hide();
                    $('#myModal').modal('hide')
                }
            });
        });

        unblockButton.click(function(){
            $.ajax({
                type: 'PUT',
                url: unblockButton.attr('data-link'),
                success: function(){
                    blockModalBtn.show();
                    unblockButton.addClass('hidden');
                    blockButton.removeClass('hidden');
                }
            });
        });

        if(blocked){
            unblockButton.removeClass('hidden');
            blockModalBtn.hide();
        } else {
            blockButton.removeClass('hidden');
            blockModalBtn.show();
        }
    });
  </script>
@endsection
