@extends('layouts.client')

@section('content')

@include('Users/crumbs')

<h2>Create a New User</h2>
<br />

@include('includes/admin/errors')
@include('Users/form')

@endsection
