@extends('layouts.client')

@section('content')

@include('Users/crumbs')

<h2>Editing {{ $user->getName() }}</h2>

@include('includes/admin/errors')
@include('Users/form')

@endsection
