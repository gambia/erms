<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="{{ route('users.index') }}">Users</a>
    </li>
    @if (isset($user))
    @if (is_null($user->id))

        <li>
            <a href="{{ action('users.create') }}">Create a New User</a>
        </li>

    @else

        <li>
            <a href="{{ route('users.show', array('id' => $user->id)) }}">{{ $user->getName() }}</a>
        </li>
        @endif

    @endif
</ol>
