@extends('layouts.client')

@section('content')

@include('Users/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-9 col-sm-7 col-xs-7">
        <h1>Users</h1>
    </div>

    @if($user->isAdminOrClientAdmin())
    <div class="col-md-3 col-sm-5 col-xs-5">
        <a href={{ URL::route('users.create') }} class="btn btn-primary pull-right">
            <i class="entypo-plus"></i>
            Create a New User
        </a>
    </div>
    @endif
</div>

 @include ('Users/list')

@endsection
