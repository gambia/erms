<div class="table-wrapper">
    <table class="table users-table mobileFriendly">
        <thead>
            <tr>
                <th>Name</th>
                <th>Job Title</th>
                <th>User Type</th>
                <th>Email</th>
                <th>Last Login</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                @if(!$user->isAdmin())
                <tr>
                    <td>
                        <img src="{{$user->getAvatarLink('assets/admin/images/member.jpg')}}" class="img-rounded avatar" width="36px" height="36px">
                        @if(Auth::User()->isAdminOrClientAdmin())
                            <a href="{{ URL::route('users.show', $user->id) }}">
                                {{ $user->getName() }}
                            </a>
                        @else
                            {{ $user->getName() }}
                        @endif
                    </td>
                    <td>
                        {{ $user->job_title }}
                    </td>
                    <td>
                        @if ($user->isAdmin())
                            <span class="label label-danger">Super Admin</span>
                        @elseif ($user->isClientAdmin())
                            <span class="label label-warning">Admin</span>
                        @elseif  ($user->isManagerOrRepresentative())
                            <span class="label label-success">Manager</span>
                        @else
                            <span class="label label-info">User</span>
                        @endif
                    </td>
                    <td>
                        {{ $user->email }}
                    </td>
                    <td>
                        @if ($user->lastLogin())
                            {{ Dates::showTimeAgo($user->lastLogin()) }}
                        @endif
                    </td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
