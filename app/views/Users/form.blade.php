{{
  Form::model(
      $user,
      array(
          'route' => is_null($user->id) ? array('users.store') : array('users.update', $user->id),
          'class'=>'form-horizontal form-groups-bordered',
          'method' => is_null($user->id) ? 'POST' : 'PUT',
          'files' => true
      )
  )
}}

<div class="row">
  <div class="col-md-6">
    {{ Form::label('First Name:','', array('class'=>'control-label ')) }}
    {{ Form::text('firstname', $user->firstname, array('class'=>'form-control')) }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::label('Last Name:','', array('class'=>'control-label ')) }}
    {{ Form::text('lastname', $user->lastname, array('class'=>'form-control')) }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::label('Job Title:','', array('class'=>'control-label')) }}
    {{ Form::text('job_title', $user->job_title, array('class'=>'form-control')) }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::label('Email:','', array('class'=>'control-label')) }}
    {{ Form::email('email', $user->email, array('autocomplete' => 'off', 'class'=>'form-control')) }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    @if ($currentUser->isAdmin())
      {{ Form::label('Client:','', array('class'=>'control-label')) }}
      {{ Form::select('client_id', $clients, $user->client_id, array('class'=>'form-control')) }}
    @else
      {{ Form::hidden('client_id', $currentUser->client_id) }}
    @endif
  </div>
</div>

@if ($currentUser->isAdmin())
<div class="row">
  <div class="col-md-6">

      {{ Form::label('Admin Level:','', array('class'=>'control-label')) }}
      {{ Form::select('admin_level', $adminLevels, $user->admin_level, array('class'=>'form-control')) }}

  </div>
</div>
@endif

<div class="row">
  <div class="col-md-6">
    {{ Form::label('Profile Picture:','', array('class'=>'control-label')) }}
    {{ Form::file('profile_picture','', array('class'=>'form-control')) }}
  </div>
</div>

<br>

<div class="row">
    <div class="col-md-6">
      @if ($user->id)
        {{ Form::submit('Update User', array('class'=>'btn btn-success')) }}
      @else
        {{ Form::submit('Create User', array('class'=>'btn btn-success')) }}
      @endif
    </div>
</div>

{{ Form::close() }}

<script type="text/javascript">
  $(document).ready(function() {
    $('#securityGroups').select2();
  });
</script>
