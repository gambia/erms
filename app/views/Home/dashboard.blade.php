@extends('layouts.client')

@section('content')

    <div class="row k1-content-header">
        <div class="col-md-6">
            <section class="profile">
                <div class="avatar-container">
                    <img src="{{$user->getAvatarLink('assets/admin/images/member.jpg')}}" class="avatar"/>
                </div>

                <div class="info">
                    <h2 class="user-name">{{ $user->getName() }} <span>({{ $user->email }})</span></h2>
                    <div class="user-job_title">{{ $user->job_title }}</div>
                    <div class="user-client">{{ $user->client->name }}</div>
                    <div class="user-last-login"><i>Last login: {{ Dates::showTimeAgo($user->lastLogin()) }}</i></div>
                </div>

            </section>
        </div>

        @if ($projects->count())
        <div class="col-md-3">
            <h2>Managed Projects</h2>
            @foreach ($projects as $project)
            <div class="btn-row">
                <a href="{{ action('projects.show', $project->project_key) }}">{{ $project->project_key }} - {{ $project->name }}</a>
                @if (count($project->children))
                <div class="btn-group">
                    <a data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        @foreach ($project->children as $p)
                        <li>
                            <a href="{{ URL::route('projects.show', $p->project_key) }}">
                                {{ $p->project_key }}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            @endforeach
        </div>
        @endif

        <div class="col-md-3">
            @if (Auth::user()->isNormalUser())
            <h2>Security Groups ({{count($user->securityGroups)}})</h2>
            <ul>
                @foreach ($user->securityGroups as $sg)
                    <li>{{ $sg->name }}</li>
                @endforeach
            </ul>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h2>Latest Case Updates</h2>
            @include('Cases/list', array('excludes'=> ['created_at', 'assigned_to']))
        </div>

        <div class="col-md-6">

            <h2>Latest Case Comments</h2>
            @include('Projects/show/comments')

            <h2>Your Recent Activity</h2>
            @include ('includes/admin/activity-feed', array('user'=>$user))

        </div>
    </div>


@stop
