@include('includes/admin/errors')

@if (!is_null($client->id))
    {{ Form::model($client, array('route' => array('clients.update', $client->id), 'class'=>'form-horizontal form-groups-bordered', 'method' => 'put', 'role' => 'form')) }}
@else
<div class="row">
    <div class="col-sm-12"><h2>Administrator data</h2></div>
</div>
    {{ Form::open(array('route' => 'clients.store', 'class'=>'form-horizontal form-groups-bordered', 'role' => 'form')) }}

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('admin_firstname', 'First name:') }}<br>
        {{ Form::text('admin_firstname', Input::old('admin_firstname'), array('placeholder' => 'Admin first name', 'class' => 'form-control input-lg')) }}
    </div>
    <div class="col-sm-6">
        {{ Form::label('admin_lastname', 'Last name:') }}<br>
        {{ Form::text('admin_lastname', Input::old('admin_lastname'), array('placeholder' => 'Admin last name', 'class' => 'form-control input-lg')) }}
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('admin_job_title', 'Job Title:') }}<br>
        {{ Form::text('admin_job_title', Input::old('admin_job_title'), array('placeholder' => 'Admin job title', 'class' => 'form-control input-lg')) }}
    </div>
    <div class="col-sm-6">
        {{ Form::label('admin_email', 'Administrator email:') }}<br>
        {{ Form::email('admin_email', Input::old('admin_email'), array('placeholder' => 'Admin email', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>
<hr>
@endif


<br>

<div class="row">
    <div class="col-sm-12"><h2>Client data</h2></div>
</div>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('name', 'Name:') }}<br>
        {{ Form::text('name', $client->name, array('placeholder' => 'Client Name', 'class' => 'form-control input-lg')) }}
    </div>
    <div class="col-sm-2">
        {{ Form::label('client_key', 'Client key:') }}<br>
        {{ Form::text('client_key', $client->client_key, array('placeholder' => 'Client Key', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('about', 'About:') }}<br>
        {{ Form::textarea('about', $client->about, array('placeholder' => 'About', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('opening_hours', 'Opening hours:') }}<br>
        {{ Form::text('opening_hours', $client->opening_hours, array('placeholder' => 'Opening hours', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('logo_url', 'Logo url:') }}<br>
        {{ Form::text('logo_url', $client->logo_url, array('placeholder' => 'Logo url', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('website', 'Website:') }}<br>
        {{ Form::text('website', $client->website, array('placeholder' => 'Website', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('billing_address_line_1', 'Billing address line 1:') }}<br>
        {{ Form::text('billing_address_line_1', $client->billing_address_line_1, array('placeholder' => 'Billing address line 1', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('billing_address_line_2', 'Billing address line 2:') }}<br>
        {{ Form::text('billing_address_line_2', $client->billing_address_line_2, array('placeholder' => 'Billing address line 2', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('billing_city', 'Billing city:') }}<br>
        {{ Form::text('billing_city', $client->billing_city, array('placeholder' => 'Billing city', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('billing_state', 'Billing state:') }}<br>
        {{ Form::text('billing_state', $client->billing_state, array('placeholder' => 'Billing state', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('billing_country', 'Billing country:') }}<br>
        {{ Form::text('billing_country', $client->billing_country, array('placeholder' => 'Billing country', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('billing_zip', 'Billing zip:') }}<br>
        {{ Form::text('billing_zip', $client->billing_zip, array('placeholder' => 'Billing zip', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('shipping_address_line_1', 'Shipping address line 1:') }}<br>
        {{ Form::text('shipping_address_line_1', $client->shipping_address_line_1, array('placeholder' => 'Shipping address line 1', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('shipping_address_line_2', 'Shipping address line 2:') }}<br>
        {{ Form::text('shipping_address_line_2', $client->shipping_address_line_2, array('placeholder' => 'Shipping address line 2', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('shipping_city', 'Shipping city:') }}<br>
        {{ Form::text('shipping_city', $client->shipping_city, array('placeholder' => 'Shipping city', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('shipping_state', 'Shipping state:') }}<br>
        {{ Form::text('shipping_state', $client->shipping_state, array('placeholder' => 'Shipping state', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('shipping_country', 'Shipping country:') }}<br>
        {{ Form::text('shipping_country', $client->shipping_country, array('placeholder' => 'Shipping country', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('shipping_zip', 'Shipping zip:') }}<br>
        {{ Form::text('shipping_zip', $client->shipping_zip, array('placeholder' => 'Shipping zip', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('phone', 'Phone:') }}<br>
        {{ Form::text('phone', $client->phone, array('placeholder' => 'Phone', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('fax', 'Fax:') }}<br>
        {{ Form::text('fax', $client->fax, array('placeholder' => 'Fax', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('email', 'Email:') }}<br>
        {{ Form::email('email', $client->email, array('placeholder' => 'Email', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('map_longitude', 'Map Longitude:') }}<br>
        {{ Form::text('map_longitude', $client->map_longitude, array('placeholder' => 'Map longitude', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('map_latitude', 'Map Latitude:') }}<br>
        {{ Form::text('map_latitude', $client->map_latitude, array('placeholder' => 'Map latitude', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('map_zoom', 'Map Zoom:') }}<br>
        {{ Form::text('map_zoom', $client->map_zoom, array('placeholder' => 'Map zoom', 'class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-6">
        {{ Form::label('timezone', 'Timezone:') }}<br>

        {{
            Form::select(
                'timezone',
                $timezones,
                $client->timezone,
                array(
                    'class'=>'form-control input-lg',
                    'name' => 'timezone',
                    'id' => 'timezone'
                )
            )
        }}
    </div>
</div>
<br>

<div class="checkbox checkbox-primary">
    {{ Form::checkbox('enable_intranet', 1, $client->enable_intranet, array('id' => 'enable_intranet')) }}
    <label for="enable_intranet">Enable Intranet</label>
</div>
<br>
<div class="checkbox checkbox-primary">
    {{ Form::checkbox('enable_wiki', 1, $client->enable_wiki, array('id' => 'enable_wiki')) }}
    <label for="enable_wiki">Enable Wiki</label>
</div>
<br>
<div class="checkbox checkbox-primary">
    {{ Form::checkbox('enable_forms', 1, $client->enable_forms, array('id' => 'enable_forms')) }}
    <label for="enable_forms">Enable Forms</label>
</div>
<br>

<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-success pull-left">
            @if ($client->id) Update Client @else Create Client @endif
        </button>
    </div>
</div>

<br /><br />

{{ Form::close() }}

<script type="text/javascript">
    $(document).ready(function() {
        $("#timezone").select2();
    });
</script>
