@extends('layouts.client')

@section('content')

<div class="row k1-heading-row">
    <div class="col-md-7 col-sm-7 col-xs-7">
        <h1>
            {{ $client->name }}
        </h1>
    </div>

    <div class="col-md-5 col-sm-5 col-xs-5 actionbar">
        <a href="{{ route('clients.edit', $client->id) }}" class="btn  btn-primary"
            <i class="entypo-pencil"></i>Edit Client</a>
    </div>
</div>

<div class="row client-attr">
    <div><strong>About:</strong></div>
    <div>{{ $client->about }}</div>
</div>

<div class="row client-attr">
    <div><strong>Client key:</strong></div>
    <div>{{ $client->client_key }}</div>
</div>

<div class="row client-attr">
    <div><strong>Logo url:</strong></div>
    <div>{{ $client->logo_url }}</div>
</div>

<div class="row client-attr">
    <div><strong>Opening hours:</strong></div>
    <div>{{ $client->opening_hours }}</div>
</div>

<div class="row client-attr">
    <div><strong>Billing address:</strong></div>
    <div>{{ $client->billing_address_line_1 }}</div>
    <div>{{ $client->billing_address_line_2 }}</div>
</div>

<div class="row client-attr">
    <div><strong>Billing city:</strong></div>
    <div>{{ $client->billing_city }}</div>
</div>

<div class="row client-attr">
    <div><strong>Billing state:</strong></div>
    <div>{{ $client->billing_state }}</div>
</div>

<div class="row client-attr">
    <div><strong>Billing country:</strong></div>
    <div>{{ $client->billing_country }}</div>
</div>

<div class="row client-attr">
    <div><strong>Billing zip:</strong></div>
    <div>{{ $client->billing_zip }}</div>
</div>

<div class="row client-attr">
    <div><strong>Shipping address:</strong></div>
    <div>{{ $client->shipping_address_line_1 }}</div>
    <div>{{ $client->shipping_address_line_2 }}</div>
</div>

<div class="row client-attr">
    <div><strong>Shipping city:</strong></div>
    <div>{{ $client->shipping_city }}</div>
</div>

<div class="row client-attr">
    <div><strong>Shipping state:</strong></div>
    <div>{{ $client->shipping_state }}</div>
</div>

<div class="row client-attr">
    <div><strong>Shipping country:</strong></div>
    <div>{{ $client->shipping_country }}</div>
</div>

<div class="row client-attr">
    <div><strong>Shipping zip:</strong></div>
    <div>{{ $client->shipping_zip }}</div>
</div>

<div class="row client-attr">
    <div><strong>Phone:</strong></div>
    <div>{{ $client->phone }}</div>
</div>

<div class="row client-attr">
    <div><strong>Fax:</strong></div>
    <div>{{ $client->fax }}</div>
</div>

<div class="row client-attr">
    <div><strong>Email:</strong></div>
    <div><a href="mailto:{{ $client->email }}">{{ $client->email }}</a></div>
</div>

<div class="row client-attr">
    <div><strong>Website:</strong></div>
    <div><a href="{{ $client->website }}" target="_blank">{{ $client->website }}</a></div>
</div>

<div class="row client-attr">
    <div><strong>Map longitude:</strong></div>
    <div>{{ $client->map_longitude }}</div>
</div>

<div class="row client-attr">
    <div><strong>Map latitude:</strong></div>
    <div>{{ $client->map_latitude }}</div>
</div>

<div class="row client-attr">
    <div><strong>Map zoom:</strong></div>
    <div>{{ $client->map_zoom }}</div>
</div>

<div class="row client-attr">
    <div><strong>Timezone:</strong></div>
    <div>{{ $client->timezone }}</div>
</div>

<div class="row client-attr">
    <div><strong>Created at:</strong></div>
    <div>{{ $client->created_at }}</div>
</div>

<div class="row client-attr">
    <div><strong>Updated at:</strong></div>
    <div>{{ $client->updated_at }}</div>
</div>



@endsection
