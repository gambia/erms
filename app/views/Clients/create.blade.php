@extends('layouts.client')

@section('content')

	<div class="row">
	  <div class="col-md-9 col-sm-7">
	    <h2>Create a New Client</h2>
	  </div>
	</div>

	@include('Clients/form')

@endsection
