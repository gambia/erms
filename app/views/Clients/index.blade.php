@extends('layouts.client')

@section('content')

<div class="row k1-heading-row">
    <div class="col-md-7 col-sm-6 col-xs-5"><h1>Clients</h1></div>
    <div class="col-md-5 col-sm-6 col-xs-7">
        <div class="pull-right">

            <a href={{ URL::route('clients.create') }} class="btn btn-primary">
                <i class="entypo-plus"></i>
                Create a New Client
            </a>
        </div>
    </div>
</div>

<table>
    <thead>
        <tr>
            <th>
                Name
            </th>
            <th>
                Links
            </th>
        </tr>
    </thead>

    @foreach ($clients as $client)
        <tr>
            <td class="nowrap" width="90%">
                {{ $client->name }}
            </td>
            <td class="nowrap" colspan="2">
                <a href="{{ URL::route('clients.show', $client->id) }}">
                    Show
                </a> |
                <a href="{{ URL::route('clients.edit', $client->id) }}">
                    Edit
                </a>
            </td>
        </tr>

    @endforeach
</table>

@endsection
