@extends('layouts.login')

@section('content')
    <div class="error-page text-center headline">
        <h1>Page Not Found</h1>
        <p><strong>The URL might be mistyped or the page has been moved.</strong></p>

        @if (Auth::user())
            <h2><a href="/">Continue to dashboard</a></h2>
        @else
            <h2><a href="/">Continue to login</a></h2>
        @endif
    </div>
@endsection