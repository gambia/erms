@extends('layouts.login')

@section('content')
    <div class="error-page text-center headline">
        <h1>System error</h1>
        <p><strong>We are sorry, something seems to be broken.</strong></p>

        <p>The administrators of this page have been notified about the error and are working on a solution. If this problem persists, please <a href="#">contact us</a>.</p>
    </div>
@endsection