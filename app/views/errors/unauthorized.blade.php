@extends('layouts.login')

@section('content')
    <div class="error-page text-center headline">
        <h1>Access Error</h1>
        <p><strong>We are sorry, you don't have succificient privileges to access this page.</strong></p>

        <p>If you think you should have access for this page, please contact your administrator.</p>
    </div>
@endsection