@if ($projects->count())
    @foreach ($projects as $project)
        @include('Projects/grid-item', array('projects' => $projects))
    @endforeach

    <script type="text/javascript">
    $(function(){
        $('project').click(function(event){

            if($(event.target).closest(".btn-group").length) return;
            if ($(this).data('access') == 1) {
                location.href = $(this).attr("rel");
            }
        });
    });
    </script>
@endif
