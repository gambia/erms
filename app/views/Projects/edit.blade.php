@extends('layouts.client')

@section('content')
@include('Projects/crumbs')

<h1>{{ $project->name }} ({{$project->project_key}})</h1>
<p>
	@if ($project->enable_cases)<div class="label label-info">Cases</div>@endif
	@if ($project->enable_wiki)<div class="label label-info">Wiki</div>@endif
	@if ($project->enable_pages)<div class="label label-info">Pages</div>@endif
	@if ($project->enable_forms)<div class="label label-info">Forms</div>@endif
</p>


@include('Projects/form')

@endsection
