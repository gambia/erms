@extends('layouts.client')

@section('content')

@include('Projects/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-7 col-sm-6 col-xs-5"><h1>Projects</h1></div>
    <div class="col-md-5 col-sm-6 col-xs-7">
        <div class="pull-right">
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i {{{ ($user->view_preference == 0 ? 'class=entypo-list' : 'class=entypo-layout') }}} ></i> View <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li {{{ ($user->view_preference == 0 ? 'class=active' : '') }}}>
                        <a href="{{ route('projects.index', array('view'=>'0')) }}"><i class="entypo-list"></i> List</a>
                    </li>
                    <li {{{ ($user->view_preference == 1 ? 'class=active' : '') }}}>
                        <a href="{{ route('projects.index', array('view'=>'1')) }}"><i class="entypo-layout"></i> Grid</a>
                    </li>
                </ul>
            </div>

            @if (Auth::user()->isClientAdmin() || Auth::user()->isAdmin())
                <a href={{ URL::route('projects.create') }} class="btn btn-primary">
                    <i class="entypo-plus"></i>
                    Create a New Project
                </a>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @if ($user->view_preference == 0)
            @include('Projects/list', array('projects' => $projects))
        @else
            @include('Projects/grid', array('projects' => $projects))
        @endif
    </div>
</div>

@endsection
