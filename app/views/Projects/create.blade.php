@extends('layouts.client')

@section('content')

@include('Projects/crumbs')

	<div class="row">
	  <div class="col-md-9 col-sm-7">
	    <h2>Create a New Project</h2>
	  </div>
	</div>

	@include('Projects/form')

@endsection
