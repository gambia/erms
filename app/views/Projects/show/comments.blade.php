<div class="row">
    <div class="col-md-12">
        @if ($comments->count())
        <section class="activity-feed">
            @foreach ($comments as $comment)
                <article class="story">
                <aside class="user-avatar">
                    <a href="{{ URL::route('users.show', $comment->user->id) }}">
                        <img src="{{ $comment->user->getAvatarLink('/assets/admin/images/member.jpg') }}" width="32px" alt="" class="img-circle" />
                    </a>
                </aside>
                <div class="story-content">
                    <header>
                        <div class="publisher">
                            <a href="{{ URL::route('cases.show', $comment->projectCase->shortcut) }}">{{ $comment->user->getName() }} in {{ $comment->projectCase->shortcut }}</a>
                            <em>{{ Dates::showTimeAgo($comment->created_at) }}</em>
                        </div>
                    </header>
                    <div class="story-main-content">
                        <p>{{ $comment->comment }}</p>
                    </div>
                </div>
            </article>
            @endforeach
        </section>

        @else
            <p>No comments yet</p>
        @endif
    </div>
</div>
