<div class="row k1-heading-row">
    <div class="col-md-12">
        @include('Cases/list')
    </div>

    <div class="col-md-12">
        <center>
            <a href="{{ action('cases.index', array('project' => $project->project_key)) }}">Show all cases in this project</a>
        </center>
    </div>
</div>