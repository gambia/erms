@if ($subprojects->count())
<div class="row">
    <div class="col-md-12">
        <h2>Subprojects</h2>
        @include('Projects/list', array('projects' => $subprojects))
    </div>
</div>
@endif