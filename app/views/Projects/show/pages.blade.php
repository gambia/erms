<div class="row k1-heading-row">
    <div class="col-md-12">
    	@if ($pages->count())
	        <table class="table">
	        	<thead>
	        		<tr>
	        			<th>Name</th>
	        			<th width="100px">Type</th>
	        			<th width="100px">Access</th>
	        			<th width="120px">Created</th>
	        		</tr>
	        	</thead>
	        	<tbody>
	        		@foreach ($pages as $page)
	        			<tr>
	        				<td><a href="{{ action('pages.show', array('id' => $page->id, 'type' => Page::$pageTypeNames[$page->type] )) }}">{{ $page->project->project_key }} - {{ $page->title }}</a></td>
	        				<td>{{ $page->getTypeNameForEmail(true) }}</td>
	        				<td>{{ $page->is_public ? 'Public' : 'Internal' }}</td>
	        				<td>{{ Dates::localizeDateTime($page->created_at)->format('d M Y') }}</td>
	        			</tr>
	        		@endforeach
	        	</tbody>
	        </table>
        @endif
    </div>
</div>
