
<div class="row">
  <div class="col-md-12">
  	<div class="alert alert-danger">
  	<h4>Please fix following errors:</h4>
  	<ul>
    @foreach ($errors->all() as $err)
    	<li>{{ $err }}</li>
	@endforeach
	</ul>
	</div>
  </div>
</div>

<hr>
