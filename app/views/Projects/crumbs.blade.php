<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="{{ route('projects.index') }}">Projects</a>
    </li>
    @if (isset($project))

        @if (is_null($project->id))

            <li>
                <a href="{{ URL::route('projects.create') }}">Create new project</a>
            </li>

        @else
            @foreach ($project->getAncestors() as $key => $parent)
              @if ($parent->depth > 0)
                <li><a href="{{ URL::route('projects.show', $parent->project_key) }}">{{ $parent->name }} ({{ $parent->project_key }})</a></li>
              @endif
            @endforeach

            <li>
                <a href="{{ URL::route('projects.show', $project->project_key) }}">{{ $project->name }} ({{ $project->project_key }})</a>
            </li>
        @endif

    @endif

</ol>