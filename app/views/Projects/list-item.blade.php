@if ($projects->count())
@foreach ($projects as $project)
    <tr {{{ ($project->depth===1 ? 'class=root' : '') }}} >
        <td class="nowrap">
            @if ($user->hasAccessToProject($project))
                <a href="{{ URL::route('projects.show', $project->project_key) }}">
                    {{ str_repeat('-', $project->depth-1) }} {{$project->project_key}}
                </a>
            @else
                {{ str_repeat('-', $project->depth-1) }} {{$project->project_key}}
            @endif
        </td>
        <td class="nowrap">
            @if ($user->hasAccessToProject($project))
                <a href="{{ URL::route('projects.show', $project->project_key) }}">
                    {{ str_repeat('-', $project->depth-1) }} {{ $project->name }}
                </a>
            @else
                {{ str_repeat('-', $project->depth-1) }} {{ $project->name }}
            @endif
        </td>
        <td class="nowrap noMobile">
            @foreach ($project->managers as $manager)
                @if ($manager->is_main_manager) {{ $manager->user->getName() }} @endif
            @endforeach
        </td>
        <td class="nowrap noMobile">{{ Dates::localizeDateTime($project->created_at)->format('d M Y') }}</td>
        @if (Auth::user()->isClientAdmin() || Auth::user()->isAdmin())
        <td>
            <a href="{{ action('projects.move', array('id' => $project->id, 'direction' => 'up')) }}" class="move move-up"><span class="entypo-up"></span></a>
            <a href="{{ action('projects.move', array('id' => $project->id, 'direction' => 'down')) }}" class="move move-down"><span class="entypo-down"></span></a>
        </td>
        @endif
    </tr>
    @include('Projects/list-item', array('projects' => $project->children))

@endforeach
@endif
