<project {{{ ($project->depth===1 ? 'class=root' : '') }}}
    rel="{{ route('projects.show', $project->project_key, false) }}"

    {{{ ($project->theme_color ? 'style=border-bottom-color:'.$project->theme_color : '') }}}

    data-access="{{ $user->hasAccessToProject($project) }}">
    <h2>
        @if ($user->hasAccessToProject($project))
        <a href="{{ URL::route('projects.show', $project->project_key) }}">
            {{ str_repeat('-', $project->depth-1) }} {{$project->project_key}}
        </a>
        @else
            {{ str_repeat('-', $project->depth-1) }} {{$project->project_key}}
        @endif
    </h2>
    <h3>
        @if ($user->hasAccessToProject($project))
        <a href="{{ URL::route('projects.show', $project->project_key) }}">
            {{ str_repeat('-', $project->depth-1) }} {{ $project->name }}
        </a>
        @else
            {{ str_repeat('-', $project->depth-1) }} {{ $project->name }}
        @endif
    </h3>

    <div class="btn-row">
        @if (count($project->children))
        <div class="btn-group">
            <a data-toggle="dropdown" aria-expanded="false">
                Subprojects <span class="badge">{{count ($project->children) }}</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                @foreach ($project->children as $p)
                <li>
                    <a href="@if ($user->hasAccessToProject($p)){{ URL::route('projects.show', $p->project_key) }}@else # @endif" @if (!$user->hasAccessToProject($p)) class="disabled" @endif>
                        {{ $p->project_key }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>

    <h4>
    @foreach ($project->managers as $manager)
        @if ($manager->is_main_manager)
            <span>Manager: {{ $manager->user->getName() }}</span>
        @endif
    @endforeach
    </h4>

    <em>Last Update: {{ date("d M Y",strtotime($project->updated_at)) }}</em>

</project>
