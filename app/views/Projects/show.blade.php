@extends('layouts.client')

@section('content')
@include('Projects/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-7 col-sm-6 col-xs-12">
        <h1>
            {{ $project->name }} ({{$project->project_key}})
        </h1>
        
            @if ($project->enable_cases)<div class="label label-info">Cases</div>@endif
            @if ($project->enable_wiki)<div class="label label-info">Wiki</div>@endif
            @if ($project->enable_pages)<div class="label label-info">Intranet</div>@endif
            @if ($project->enable_forms)<div class="label label-info">Forms</div>@endif
        
    </div>

    @if ($canEdit)
    <div class="col-md-5 col-sm-6 col-xs-12 actionbar">
        <a href="{{ route('projects.edit', array('project_key' => $project->project_key)) }}" class="btn  btn-primary">
            <i class="entypo-pencil"></i>Edit Project</a>

        @if ($canCreateCase)
        &nbsp; <a href={{ URL::route('cases.create') }} class="btn btn-success pull-right">
            <i class="entypo-plus"></i>
            Create a New Case
        </a>
        @endif
    </div>
    @endif

</div>

<div class="row">
    <div class="col-md-8">
        <h2>Summary</h2>
        <p>
            {{ $project->summary }}
        </p>
    </div>
    <div class="col-md-3">
        <h2>Project Details</h2>
        <p>
            <strong>Project Key:</strong> {{ $project->project_key }}<br>
            <strong>Manager:</strong>
                @foreach ($project->managers as $manager)
                    @if ($manager->is_main_manager) {{$manager->user->getName()}} @endif
                @endforeach
            <br>
            <strong>Manager Representative(s):</strong><br />
                @foreach ($project->managers as $manager)
                    @if (!$manager->is_main_manager) {{$manager->user->getName()}}<br /> @endif
                @endforeach

            <strong>Security Group(s): </strong>
            <?php $grps = []; ?>
            @foreach ($project->securityGroups as $group)
                <?php $grps[] = $group->name; ?>
            @endforeach
            <?php echo implode(', ', $grps); ?>
            <br>

            <strong>Created:</strong> {{ Dates::localizeDateTime($project->created_at)->format('d M Y') }}<br>
            <strong>Updated:</strong> {{ Dates::showTimeAgo($project->updated_at) }}<br>
        </p>
    </div>
</div>

@include('Projects/show/subprojects')

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="#cases">Cases</a></li>
            <li role="presentation"><a href="#comments">Latest Comments</a></li>
            @if ($pages->count()) <li role="presentation"><a href="#pages">Intranet</a></li> @endif
            <li role="presentation"><a href="#history">History</a></li>
        </ul>
    </div>
</div>


<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="cases">
        @include('Projects/show/cases', array('project' => $project))
    </div>

    <div role="tabpanel" class="tab-pane" id="comments">
        @include('Projects/show/comments')
    </div>

    <div role="tabpanel" class="tab-pane" id="pages">
        @include('Projects/show/pages')
    </div>

    <div role="tabpanel" class="tab-pane" id="history">
        @include('includes/admin/history')
    </div>
</div>

<script type="text/javascript">
    var bindClickListener = function (object) {
        object.click(function (e) {
            e.preventDefault()
            object.tab('show')
        });
    }

    $(function() {
        $('.nav.nav-tabs').find('a').each(function(){
            bindClickListener($(this));
        });

        $('.nav.nav-tabs li:first a').tab('show');
    });
</script>
@endsection
