<div class="table-wrapper project-list">
    <table class="table">
        <thead>
            <tr>
                <th width="100px">Key</th>
                <th>Name</th>
                <th class="noMobile" width="200px">Manager</th>
                <th class="noMobile" width="120px">Created</th>
                @if (Auth::user()->isClientAdmin() || Auth::user()->isAdmin()) <th width="70px"></th> @endif
            </tr>
        </thead>
        <tbody>
            @include('Projects/list-item', array('projects' => $projects))
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        var moveEventListener = function(e)
        {
            e.preventDefault();

            var a = $(this);

            $.get(a.attr('href'), function() {
                $.get('{{ action("projects.index") }}', function(response) {
                    var $html = $(response);

                    $('body').find('.move').off('click');
                    $('body').find('.project-list').html($html.find('.project-list').html());
                    $('body').find('.move').on('click', moveEventListener);
                });
            });
        };

        $('body').find('.move').on('click', moveEventListener);
    });
</script>