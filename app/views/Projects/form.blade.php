@include('includes/admin/errors')

@if (isset($project) && !is_null($project->project_key))
    {{ Form::model($project, array('route' => array('projects.update', $project->project_key), 'class'=>'form-horizontal form-groups-bordered', 'method' => 'put', 'role' => 'form')) }}
@else
    {{ Form::open(array('route' => 'projects.store', 'class'=>'form-horizontal form-groups-bordered', 'role' => 'form')) }}
@endif


<br>

<div class="row">
    <div class="col-sm-2">
        {{ Form::label('project_key', 'Project key:') }}<br>
        {{ Form::text('project_key', $project->project_key, array('placeholder' => 'Project Key', 'class' => 'form-control input-lg')) }}
    </div>
    <div class="col-sm-6">
        {{ Form::label('name', 'Title:') }}<br>
        {{ Form::text('name', $project->name, array('placeholder' => 'Project Name', 'class' => 'form-control input-lg')) }}
    </div>
    <div class="col-sm-4">
        <?php
            $parentsTree = array();
            foreach ($availableParents as $parent) {
              if ($parent->depth === 1) {
                $parentsTree[$parent->id] = $parent->project_key . ' - ' . $parent->name;
                foreach ($parent->getDescendants() as $sub) {
                  $parentsTree[$sub->id] = str_repeat('--', $sub->depth-1) . ' ' . $sub->project_key . ' - ' . $sub->name;
                }
              }
            }
          ?>
        {{ Form::label('parent_project_id', 'Parent Project:') }}<br>
        {{ Form::select('parent_project_id', [$rootProject->id => 'None'] + $parentsTree, $project->parent_project_id, array('class' => 'form-control input-lg')) }}
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-4">
        {{ Form::label('main_manager', 'Manager:') }}<br>
        <select name="main_manager" id="main_manager" class="form-control input-lg">
            @foreach ($managers as $key => $manager)
                <option value="{{ $manager->id }}" @if ($project->isMainManager($manager)) selected="selected" @endif>{{ $manager->name }} ({{ $manager->job_title }})</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-4">
        {{ Form::label('managers', 'Manager Representative(s):') }}<br>
        <select multiple="multiple" name="managers[]" id="managers" class="form-control input-lg">
            @foreach ($managers as $key => $manager)
                <option value="{{ $manager->id }}" @if ($project->hasManager($manager)) selected="selected" @endif>{{ $manager->name }} ({{ $manager->job_title }})</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-4">
        {{ Form::label('security_groups', 'Security Group(s):') }}<br>
        <select multiple="multiple" name="security_groups[]" id="security_groups" class="form-control input-lg">
            @foreach ($securityGroups as $group)
                <option value="{{ $group->id }}" @if ($project->isInSecurityGroup($group)) selected="selected" @endif>{{ $group->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-8">
        {{ Form::label('summary', 'Description:') }}<br>
        {{ Form::textarea('summary', $project->summary, array('placeholder' => 'Description', 'class' => 'form-control wysihtml5', 'data-stylesheet-url' => 'assets/css/wysihtml5-color.css', 'rows' => 9)) }}

        <br><br>
    </div>

    <div class="col-sm-4">
        <strong>Extras:</strong>
        <div class="checkbox checkbox-primary">
            {{ Form::checkbox('enable_cases', 1, $project->enable_cases || true, array('id' => 'enable_cases')) }}
            <label for="enable_cases">Enable Cases</label>
        </div>

        @if(Auth::User()->client->enable_wiki)
        <div class="checkbox checkbox-primary">
            {{ Form::checkbox('enable_wiki', 1, $project->enable_wiki || true, array('id' => 'enable_wiki')) }}
            <label for="enable_wiki">Enable Wiki</label>
        </div>
        @endif

        @if(Auth::User()->client->enable_intranet)
        <div class="checkbox checkbox-primary">
            {{ Form::checkbox('enable_pages', 1, $project->enable_pages || true, array('id' => 'enable_pages')) }}
            <label for="enable_pages">Enable Pages</label>
        </div>
        @endif

        @if(Auth::User()->client->enable_forms)
        <div class="checkbox checkbox-primary">
            {{ Form::checkbox('enable_forms', 1, $project->enable_forms || true, array('id' => 'enable_forms')) }}
            <label for="enable_forms">Enable Forms</label>
        </div>
        @endif

        <br><br>
        {{ Form::label('theme_color', 'Project Color:') }}<br>
        {{ Form::text('theme_color', $project->theme_color, array('placeholder' => 'Project Color', 'id' => 'theme_color', 'class' => 'form-control input-lg')) }}
    </div>

</div>
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-success pull-right">
            @if ($project->id) Update Project @else Create Project @endif
        </button>
    </div>
</div>

<br /><br />



{{ Form::close() }}

<script type="text/javascript">
    $(document).ready(function() {
        $("#security_groups").select2();
        $("#managers").select2();
        $('#theme_color').colorpicker();
    });
</script>