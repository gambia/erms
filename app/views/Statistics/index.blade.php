@extends('layouts.client')

@section('content')

<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        Statistics
    </li>
</ol>

<div class="row k1-heading-row">
    <div class="col-md-9 col-sm-7">
        <h1>
            Statistics
        </h1>
    </div>

</div>


<div class="row">
    <div class="col-md-6">
        <strong>User Logins</strong>
        <div id="chart-user-logins" style="height: 300px; margin-bottom:80px;"></div>
    </div>
    <div class="col-md-6">
        <strong>Cases Created / Closed</strong>
        <div id="chart-cases" style="height: 300px; margin-bottom:80px;"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <strong>Records Created</strong>
        <div id="chart-records" style="height: 300px; margin-bottom:80px;"></div>
    </div>
    <div class="col-md-6">
        <strong>Comments Added</strong>
        <div id="chart-comments" style="height: 300px; margin-bottom:80px;"></div>

    </div>
</div>

<script type="text/javascript">
    var loginData = [
        @foreach ($userLogins as $date => $count)
            {"day": '{{ $date }}', "value": {{ $count }} },
        @endforeach
    ];

    Morris.Line({
        element: 'chart-user-logins',
        data: loginData,
        xkey: 'day',
        ykeys: ['value'],
        labels: ['Logins'],
        parseTime: false,
        lineColors: ['#242d3c']
    });

    var casesData1 = [
        @foreach ($cases as $date => $counts)
            {"x": '{{ $date }}', "y": {{ $counts['created'] }}, "z": {{ $counts['closed'] }} },
        @endforeach
    ];

    Morris.Bar({
        element: 'chart-cases',
        data: casesData1,
        xkey: 'x',
        ykeys: ['y', 'z'],
        labels: ['Created', 'Closed'],
        stacked: true,
        barColors: ['#ffaaab', '#ff6264']
    });

    var casesData2 = [
        @foreach ($records as $date => $counts)
            {"x": '{{ $date }}', "y": {{ $counts['created'] }} },
        @endforeach
    ];

    Morris.Line({
        element: 'chart-records',
        data: casesData2,
        xkey: 'x',
        ykeys: ['y'],
        labels: ['Created'],
        stacked: false,
        barColors: ['#ffaaab']
    });

    var casesData3 = [
        @foreach ($comments as $date => $count)
            {"x": '{{ $date }}', "y": {{ $count }} },
        @endforeach
    ];

    Morris.Line({
        element: 'chart-comments',
        data: casesData3,
        xkey: 'x',
        ykeys: ['y'],
        labels: ['Created'],
        stacked: false,
        barColors: ['#ffaaab']
    });
</script>


@endsection
