@extends('layouts.login')

@section('content')

{{ Form::open( array('url' => action('ForgottenPasswordsController@store'), 'method' => 'post') ) }}
<div class="form-signin">

    <div class="form-signin-heading">

        <div class="login-content center">
            <img src="/assets/admin/images/k1cms_logo_big.png" alt="K1 CMS" class="logo" /><br />
            @if (Session::has('email_not_found_error'))
            <div class="row">
                <p>
                    We could not find the e-mail you provided.
                </p>
            </div>
            @endif

            <p class="description">Please provide your email address:</p>
        </div>

    </div>

    <div class="login-form" style="max-width:350px; margin:0 auto;">
        <div class="login-content center">

            <form method="post" role="form" id="form_login" novalidate="novalidate">

                <div class="form-group">

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>

                        {{ Form::text('email','',array('class' => 'form-control', 'placeholder'=>'Your Email')) }}
                    </div>

                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="entypo-login"></i>
                        Submit
                    </button>
                </div>

            </form>

        </div>

    </div>

</div>

{{ Form::close() }}
@stop
