@extends('layouts.login')

@section('content')

{{ Form::open( array('url' => '/login', 'method' => 'post') ) }}
<div class="form-signin">

    <div class="form-signin-heading">

        <div class="login-content">
            <img src="/assets/admin/images/k1cms_logo_big.png" alt="K1 CMS" class="logo" />
            <p class="description">Please log in to continue:</p>
        </div>

    </div>

    <div class="login-progressbar">
        <div></div>
    </div>

    <div class="login-form">
        <div class="login-content center">
            @if (Session::has('login_error'))
            <div class="row">
                <p>Your login credentials do not match our recods. <br /><br />Try again or
                    <a href="{{ action('ForgottenPasswordsController@create') }}" class="link"><u>Reset Your Password</u></a><br />
                </p>
            </div>
            @endif

            @if (Session::has('forgotten_password_email_sent'))
            <div class="row">
                <p>
                    You should receive an e-mail with further instructions shortly.
                </p>
            </div>
            @endif

            @if (Session::has('account_blocked'))
            <div class="row">
                <div>
                    We apologize but your account has been blocked.
                </div>
                <div>
                    Please, contact your superior.
                </div>
            </div>
            @endif

            <form method="post" role="form" id="form_login" novalidate="novalidate">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>

                        {{ Form::text('email','',array('class' => 'form-control', 'placeholder'=>'Your Email')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>

                        {{ Form::password('password',array('class' => 'form-control', 'placeholder'=>'Your Password')) }}
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="entypo-login"></i>
                        Login
                    </button>
                </div>
            </form>


            <div class="login-bottom-links">
                <a href="{{ action('ForgottenPasswordsController@create') }}" class="link">Forgot your password?</a>
            </div>
            <div class="copyright">
                EDRC GmbH © 2015
            </div>
        </div>

    </div>

</div>


{{ Form::close() }}
@stop
