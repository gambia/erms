@extends('layouts.client')

@section('content')

<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        Search
    </li>
</ol>

<h1>Search</h1>

<div class="row">
	<div class="col-md-12">
		<form action="{{ action('search.index') }}" method="GET">
		<input type="text" name="q" class="form-control input-lg" value="{{ $query }}" />
		</form>

		<hr>
	</div>
</div>

@if ($query)
	<div class="row">
		<div class="col-md-12">
			@if ($results > 0)

				@if ($projects->count())
					<h2>Projects</h2>
					@foreach ($projects as $proj)
						<a href="{{ action('projects.show', $proj->project_key) }}">{{ $proj->getFullName() }}</a><br>
					@endforeach

					<hr>
				@endif

				@if ($cases->count())
					<h2>Cases</h2>
					@foreach ($cases as $case)
						<a href="{{ action('cases.show', $case->shortcut) }}">{{ $case->getFullName() }}</a><br>
					@endforeach

					<hr>
				@endif

				@if ($records->count())
					<h2>Records</h2>
					@foreach ($records as $record)
						<a href="{{ action('records.show', $record->id) }}">{{ $record->title }}</a><br>
					@endforeach

					<hr>
				@endif

				@if ($pages->count())
					<h2>Pages</h2>
					@foreach ($pages as $page)
						<a href="{{ action('pages.show', array('type' => Page::$pageTypeNames[$page->type], 'id' => $page->id)) }}">{{ $page->title }}</a><br>
					@endforeach

					<hr>
				@endif

				@if ($users->count())
					<h2>Users</h2>
					@foreach ($users as $user)
						<a href="{{ action('users.show', $user->id) }}">{{ $user->getName() }}</a><br>
					@endforeach

					<hr>
				@endif

			@else
				<h3>Your search query didn't match any of our records.</h3>
			@endif
		</div>
	</div>
@endif


@endsection
