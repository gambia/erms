@extends('layouts.client')

@section('content')

<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="#">System History</a>
    </li>
</ol>

<div class="row k1-heading-row">
    <div class="col-md-9 col-sm-7 col-xs-7">
        <h1>System History</h1>
    </div>

    @if (Auth::user()->isAdmin())
        <div class="col-md-3 col-sm-5 col-xs-5">
            <form method="GET" action="{{ action('ActivityController@index') }}" class="pull-right">
                <select name="client">
                    @foreach ($clients as $client)
                        <option value="{{ $client->id }}" @if ($f_client == $client->id) selected="selected" @endif>{{ $client->name }}</option>
                    @endforeach
                </select>
            </form>
        </div>
    @endif
</div>

<form method="GET" action="{{ action('ActivityController@index') }}">
  <table class="table mobileFriendly">
    <thead>
        <tr>
            <th>Time</th>
            <th>
                <select name="user">
                    <option value="">User</option>
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}" @if ($f_user == $user->id) selected="selected" @endif>{{ $user->getName() }}</option>
                    @endforeach
                </select>
            </th>
            <th>
                <select name="action">
                    <option value="">Activity</option>
                    @foreach (History::$actionMapping as $key => $actionName)
                        <option value="{{ $key }}" @if ($f_action == $key) selected="selected" @endif >{{ $actionName }}</option>
                    @endforeach
                </select>
                @if (Auth::user()->isAdmin())
                    <input type="hidden" name="client" value="{{ $f_client }}">
                @endif
            </th>
            <th>User Agent</th>
            <th>IP Address</th>
        </tr>
    </thead>
    <tbody>
    @if ($count > 0)
        @foreach($history as $activity)
            <tr>
                <td>{{ Dates::localizeDateTime($activity->updated_at)}}</td>
                <td>
                    @if ($activity->user_id)
                        {{ $activity->user->getName() }}
                    @else
                        Anonymous
                    @endif
                </td>
                <td class="firstWordUppercase">
                    @Include('includes/admin/activity-action', array('activity' => $activity))
                </td>
                <td>
                    <?php preg_match('/^([^\(]+)/', $activity->user_agent, $browser); ?>
                    <span data-toggle="tooltip" data-placement="top" title="{{$activity->user_agent}}">{{ $browser[1] }}</span>
                </td>
                <td>{{$activity->ip_address}}</td>
            </tr>
        @endforeach
    @else
        <tr><td colspan="5"><h2>No activity records found. <a href="{{ action('ActivityController@index', array('client' => $f_client)) }}">Reset filter</a></h2></td></tr>
    @endif
    </tbody>
    <tfoot>
        <tr>
            <th>Time</th>
            <th>User</th>
            <th>Activity</th>
            <th>User Agent</th>
            <th>IP Address</th>
        </tr>
    </tfoot>
</table>
</form>

@if ($count > $limit)
<div class="row">
    <div class="col-md-12">

        <?php
            $pages = ceil($count/$limit);

            $startPage = ($page < 5)? 1 : $page - 4;
            $endPage = 8 + $startPage;
            $endPage = ($pages < $endPage) ? $pages : $endPage;
            $diff = $startPage - $endPage + 8;
            $startPage -= ($startPage - $diff > 0) ? $diff : 0;
        ?>

        <div class="text-center">
            <ul class="pagination">
                @if ($from > 0 && $from >= $limit)
                    <li><a href="{{ URL::route('activity.index', array('from' => 0, 'action' => $f_action, 'user' => $f_user, 'client' => $f_client)) }}">First</a></li>
                    <li><a href="{{ URL::route('activity.index', array('from' => $from - $limit, 'action' => $f_action, 'user' => $f_user, 'client' => $f_client)) }}"><i class="entypo-left-open-mini">&nbsp;</i></a></li>
                @endif

                @for ($i=$startPage; $i<=$endPage; $i++)
                    <li @if ($page == $i-1) class="active" @endif><a href="{{ URL::route('activity.index', array('from' => ($i-1) * $limit, 'action' => $f_action, 'user' => $f_user, 'client' => $f_client)) }}">{{ $i }}</a></li>
                @endfor

                @if (($from + $limit) <= $count)
                    <li><a href="{{ URL::route('activity.index', array('from' => $from + $limit, 'action' => $f_action, 'user' => $f_user, 'client' => $f_client)) }}"><i class="entypo-right-open-mini">&nbsp;</i></a></li>
                    <li><a href="{{ URL::route('activity.index', array('from' => ($pages-1) * $limit, 'action' => $f_action, 'user' => $f_user, 'client' => $f_client)) }}">Last</a></li>
                @endif

            </ul>
        </div>
    </div>
</div>
@endif

<script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').css('textDecoration', 'underline').tooltip();
      $('select').on('change', function() {
        $(this).parents('form').submit();
      });
    })
    /*
    jQuery(document).ready(function($)
    {
        var table = $("#table-3").dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true
        });

        table.columnFilter({
            "sPlaceHolder" : "head:after"
        });
    }); */
</script>
@stop
