<article class="story">
    <div class="thumb">
        <i class="entypo-{{$icon}}"></i>
    </div>
    <div class="text">
        <strong class="bold">{{ $user->getName() }}</strong>
        {{ $text }}

        @if (isset($link))
            {{ $link }}
        @endif

        @if (isset($link2))
            in {{ $link2 }}
        @endif

        @if (isset($additional_data))
            {{ $additional_data }}
        @endif

        <em>{{ Dates::showTimeAgo($activity->created_at) }}</em>
    </div>
</article>
