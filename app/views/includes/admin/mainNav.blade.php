<ul>
    <li {{{ (Request::is('/') ? 'class=active' : '') }}}>
        <a href="/">
            <i class="entypo-gauge"></i><span>Dashboard</span>
        </a>
    </li>
    @if (Auth::user()->isAdmin())
    <li {{{ (Request::is('clients/*') ? 'class=active' : '') }}}>
        <a href="{{ route('clients.index') }}">
            <i class="entypo-window"></i><span>Clients</span>
        </a>
    </li>
    @endif
    <li {{{ (Request::is('projects*') ? 'class=active' : '') }}}>
        <a href="{{ route('projects.index') }}">
            <i class="entypo-archive"></i><span>Projects</span>
        </a>
    </li>
    <li {{{ (Request::is('cases*') ? 'class=active' : '') }}}>
        <a href="{{ route('cases.index') }}">
            <i class="entypo-folder"></i><span>Cases</span>
        </a>
    </li>

    @if (Auth::user()->client->enable_wiki)
    <li {{{ (Request::is('pages/wiki*') ? 'class=active' : '') }}}>
        <a href="{{ route('pages.index', 'wiki') }}">
            <i class="entypo-doc-text"></i><span>Wiki</span>
        </a>
    </li>
    @endif

    @if (Auth::user()->client->enable_intranet)
    <li {{{ (Request::is('pages/intranet*') ? 'class=active' : '') }}}>
        <a href="{{ route('pages.index', 'intranet') }}">
            <i class="entypo-window"></i><span>Intranet</span>
        </a>
    </li>
    @endif

    @if (Auth::user()->client->enable_forms)
    <li {{{ (Request::is('pages/forms*') ? 'class=active' : '') }}}>
        <a href="{{ route('pages.index', 'forms') }}">
            <i class="entypo-newspaper"></i><span>Forms</span>
        </a>
    </li>
    @endif

    @if (Auth::user()->isAdmin() || Auth::user()->isClientAdmin())
    <li {{{ (Request::is('statistics*') ? 'class=active' : '') }}}>
        <a href="{{ route('statistics.index') }}">
            <i class="entypo-chart-bar"></i><span>Statistics</span>
        </a>
    </li>
    @endif

    <li {{{ (Request::is('users*') ? 'class=active' : '') }}}>
        <a href="{{ route('users.index') }}">
            <i class="entypo-users"></i><span>Users</span>
        </a>
    </li>


    @if (Auth::user()->isAdminOrClientAdmin() || Auth::user()->isManagerOrRepresentative())
    <li {{{ (Request::is('securitygroups*') ? 'class=active' : '') }}}>
        <a href="{{ route('securitygroups.index') }}">
            <i class="entypo-key"></i><span>Security Groups</span>
        </a>
    </li>
    @endif

    @if (Auth::user()->isAdmin() || Auth::user()->isClientAdmin())
    <li {{{ (Request::is('activity*') ? 'class=active' : '') }}}>
        <a href="{{ route('activity.index') }}">
            <i class="entypo-clock"></i><span>System History</span>
        </a>
    </li>
    @endif

    @if (Auth::user()->isAdmin())
    <li {{{ (Request::is('documentation*') ? 'class=active' : '') }}}>
        <a href="{{ route('documentation.index') }}">
            <i class="entypo-window"></i><span>User Manual</span>
        </a>
    </li>
    @endif
</ul>
