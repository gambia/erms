<!-- Modal -->
<div class="modal fade" id="{{ $modalId }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Deleting {{ $modalTitle }}</h4>
            </div>
            <div class="modal-body">
                <p>
                    Do you really want to delete {{ $modalTitle }}? You <b>CANNOT</b> undo this action.
                <p>
            </div>

            <div class="modal-footer">
                {{ Form::open($modalFormRoute) }}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                    <button type="submit" class="btn btn-danger">
                        Delete
                    </button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var bindClickListener = function (object) {
        object.click(function (e) {
            e.preventDefault()
            object.tab('show')
        });
    }

    $(function() {
        $('.nav.nav-tabs').find('a').each(function(){
            bindClickListener($(this));
        });

        $('.nav.nav-tabs li:first a').tab('show');
    });
</script>