<ul>
    <li>
        <form action="{{ action('search.index') }}" method="GET">
            <i class="entypo-search"></i>
            <input type="text" name="q" class="header-search" value="{{ isset($query) ? $query : '' }}" placeholder="Search" />
        </form>
    </li>

    <li class="dropdown">
        <a href="#"  data-toggle="dropdown">
            <i class="entypo-help-circled"></i><span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            @include('includes/admin/nav/support')
        </ul>
    </li>

    <li style="display:none">
        <a href="{{ route('documentation.index') }}">
            <i class="entypo-info-circled"></i>
        </a>
    </li>

    <li class="profile-info dropdown">

        <a href="#" class="dropdown-toggle" style="margin-right: 30px;" data-toggle="dropdown">
            <span style="margin-right: 30px;">{{Auth::user()->getName()}}
                <span class="caret"></span>
            </span>
            <img src="{{Auth::user()->getAvatarLink('/assets/admin/images/member.jpg')}}" alt="" class="avatar" width="44" height="44" />
        </a>

        <ul class="dropdown-menu">
            @include('includes/admin/nav/profile')
        </ul>
    </li>
</ul>
