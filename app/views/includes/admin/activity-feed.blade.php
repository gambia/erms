<section class="activity-feed">
    @if ($latestActivities->count())
        @foreach ($latestActivities as $activity)

                {{-- PAGES --}}
                @if ($activity->action == History::PAGE_CREATED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'star-empty',
                        'text'=>'created page',
                        'link'=> link_to_route('pages.show', $activity->target_content, ['id' => $activity->target_id, 'type' => Page::$pageTypeNames[$activity->additional_data]])
                    ))

                @elseif ($activity->action == History::PAGE_UPDATED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'pencil',
                        'text'=>'updated page',
                        'link'=> link_to_route('pages.show', $activity->target_id, ['id' => $activity->target_id, 'type' => Page::$pageTypeNames[$activity->additional_data]])
                    ))

                @elseif ($activity->action == History::PAGE_CHUNK_UPLOAD_FINISHED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'attach',
                        'text'=>'uploaded new files to',
                        'link'=> link_to_route('pages.show', $activity->ref_content, ['id' => $activity->target_id, 'type' => Page::$pageTypeNames[$activity->additional_data]])
                    ))

                @elseif ($activity->action == History::PAGE_FILE_DELETED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'cancel',
                        'text'=>'deleted an attachment from',
                        'link'=> link_to_route('pages.show', $activity->ref_content, ['id' => $activity->target_id, 'type' => Page::$pageTypeNames[$activity->additional_data]]),
                    ))

                @elseif ($activity->action == History::PAGE_COMMENTED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'chat',
                        'text'=>'posted a comment in',
                        'link'=> link_to_route('pages.show', $activity->ref_content, ['id' => $activity->ref_content])
                    ))

                {{-- RECORDS --}}
                @elseif ($activity->action == History::RECORD_CREATED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'star-empty',
                        'text'=>'created record',
                        'link'=> link_to_route('records.show', $activity->target_content, ['id' => $activity->target_id]),
                        'link2' => link_to_route('cases.show', $activity->ref_content, ['id' => $activity->ref_id])
                    ))

                @elseif ($activity->action == History::RECORD_UPDATED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'pencil',
                        'text'=>'updated record',
                        'link'=> link_to_route('records.show', $activity->target_content, ['id' => $activity->target_id]),
                        'link2' => link_to_route('cases.show', $activity->ref_content, ['id' => $activity->ref_id])
                    ))

                @elseif ($activity->action == History::RECORD_CHUNK_UPLOAD_FINISHED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'attach',
                        'text'=>'uploaded new files to',
                        'link'=> link_to_route('records.show', $activity->target_content, ['id' => $activity->target_id]),
                        'link2' => link_to_route('cases.show', $activity->ref_content, ['id' => $activity->ref_id])
                    ))

                @elseif ($activity->action == History::RECORD_FILE_DELETED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'cancel',
                        'text'=>'deleted file in',
                        'link'=> link_to_route('records.show', $activity->target_content, ['id' => $activity->target_id]),
                        'link2' => link_to_route('cases.show', $activity->ref_content, ['id' => $activity->ref_id])
                    ))

                @elseif ($activity->action == History::RECORD_DELETED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'cancel',
                        'text'=>'deleted record '.$activity->ref_content.' in',
                        'link'=> link_to_route('cases.show', $activity->target_content, ['id' => $activity->target_id])
                    ))

                {{-- CASES --}}
                @elseif ($activity->action == History::CASE_CREATED)
                   @include ('Activity/feed-item', array(
                        'icon'=>'star-empty',
                        'text'=> 'created case',
                        'link'=> link_to_route('cases.show', $activity->target_content, ['id' => $activity->target_id])
                    ))

                @elseif ($activity->action == History::CASE_UPDATED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'pencil',
                        'text'=>'updated case',
                        'link'=> link_to_route('cases.show', $activity->target_content, ['id' => $activity->target_id])
                    ))

                @elseif ($activity->action == History::CASE_COMMENTED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'chat',
                        'text'=>'posted a comment in',
                        'link'=> link_to_route('cases.show', $activity->ref_content, ['id' => $activity->ref_id])
                    ))

                @elseif ($activity->action == History::CASE_CLOSED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'lock',
                        'text'=> 'closed case',
                        'link'=> link_to_route('cases.show', $activity->target_content, ['id' => $activity->target_id])
                    ))

                @elseif ($activity->action == History::CASE_DELETED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'cancel',
                        'text'=> 'deleted case '.$activity->ref_id.' '.$activity->ref_content
                    ))

                {{-- PROJECTS --}}
                @elseif ($activity->action == History::PROJECT_CREATED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'star-empty',
                        'text'=>'created project',
                        'link'=> link_to_route('projects.show', $activity->target_content, ['id' => $activity->ref_content])
                    ))

                @elseif ($activity->action == History::PROJECT_UPDATED)
                    @include ('Activity/feed-item', array(
                        'icon'=>'pencil', 'text'=>'updated project',
                        'link'=> link_to_route('projects.show', $activity->target_content, ['id' => $activity->ref_content])
                    ))

                @else
                    @include ('Activity/feed-item', array(
                        'icon'=>'info',
                        'text'=> History::$actionMapping[$activity->action]
                    ))


                {{-- commenting out for now
                   @elseif ($activity->action == History::USER_PASSWORD_CHANGED)
                      changed the password of {{ link_to_route('users.show', $activity->target_id, ['id' => $activity->target_id]) }}
                   @elseif ($activity->action == History::USER_BLOCKED)
                      blocked user {{ link_to_route('users.show', $activity->target_content, ['id' => $activity->target_id]) }}
                   @elseif ($activity->action == History::USER_UNBLOCKED)
                      unblocked user {{ link_to_route('users.show', $activity->target_content, ['id' => $activity->target_id]) }}
                   @elseif ($activity->action == History::USER_UPDATED)
                      updated user {{ link_to_route('users.show', $activity->target_content, ['id' => $activity->target_id]) }}
                --}}


                @endif
       @endforeach
    @else
       <h3>User has no activities yet.</h3>
    @endif
</section>
