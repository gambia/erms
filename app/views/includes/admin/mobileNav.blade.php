<ul>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown">
            <i class="entypo-menu"></i>
        </a>

        <div class="dropdown-menu">
            @include('includes/admin/mainNav')

            <ul>
                <li class="divider"></li>
                <li>
                    <a href="{{ route('documentation.index') }}">
                        <i class="entypo-info-circled"></i>
                        Documentation
                    </a>
                </li>

                {{-- @include('includes/admin/nav/support') --}}
                <li class="divider"></li>
                @include('includes/admin/nav/profile')
            </ul>
        </div>
    </li>
</ul>