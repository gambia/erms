 <header class="k1-page-header">
    <a href="/" class="logo">
        <img src="/assets/admin/images/k1cms_logo_small.png" alt="K1 CMS" />
    </a>

    <nav class="headerNav">
        @include('includes/admin/headerNav')
    </nav>

    <nav class="mobileNav">
        @include('includes/admin/mobileNav')
    </nav>
</header>
