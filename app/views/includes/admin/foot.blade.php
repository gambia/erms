<link rel="stylesheet" href="/assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="/assets/admin/js/rickshaw/rickshaw.min.css">
<link rel="stylesheet" href="/assets/admin/js/wysihtml5/bootstrap-wysihtml5.css">
<link rel="stylesheet" href="/assets/admin/js/selectboxit/jquery.selectBoxIt.css">
<link rel="stylesheet" href="/assets/admin/js/vertical-timeline/css/component.css">
<link rel="stylesheet" href="/assets/admin/js/dropzone/basic.css">
<link rel="stylesheet" href="/assets/admin/js/dropzone/dropzone.css">

<!-- Bottom Scripts -->

<script src="/assets/admin/js/bootstrap.js"></script>

<script src="/assets/admin/js/wysihtml5/wysihtml5-0.4.0pre.min.js"></script>
<script src="/assets/admin/js/wysihtml5/bootstrap-wysihtml5.js"></script>
<script src="/assets/admin/js/jquery.wysihtml5_size_matters.js"></script>

<script src="/assets/admin/js/k1-custom.js"></script>

<script src="/assets/admin/js/jquery.peity.min.js"></script>

<link rel="stylesheet" href="/assets/admin/js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="/assets/admin/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="/assets/admin/js/select2/select2.css">

<!-- Bottom Scripts -->
<script src="/assets/admin/js/jquery.dataTables.min.js"></script>
<script src="/assets/admin/js/datatables/TableTools.min.js"></script>
<script src="/assets/admin/js/dataTables.bootstrap.js"></script>
<script src="/assets/admin/js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="/assets/admin/js/datatables/lodash.min.js"></script>
<script src="/assets/admin/js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="/assets/admin/js/select2/select2.min.js"></script>

<script src="/assets/admin/js/fileinput.js"></script>
<script src="/assets/admin/js/dropzone/dropzone.js"></script>



@include('includes.shared.analytics')
