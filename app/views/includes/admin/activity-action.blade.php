@if ($activity->action === History::PROJECT_CREATED)
    created new project <a href="{{ action('projects.show', $activity->ref_content) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::PROJECT_UPDATED)
    updated project <a href="{{ action('projects.show', $activity->ref_content) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::CASE_CREATED)
    created new case <a href="{{ action('cases.show', $activity->target_id) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::CASE_UPDATED)
    updated case <a href="{{ action('cases.show', $activity->target_id) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::CASE_CLOSED)
    closed case <a href="{{ action('cases.show', $activity->target_id) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::CASE_COMMENTED)
    added comment to case <a href="{{ action('cases.show', $activity->ref_id) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::PAGE_CREATED)
    <?php $pageType = Page::$pageTypeNames[$activity->additional_data]; ?>
    created new {{ $pageType }} page <a href="{{ action('pages.show', ['type' => $pageType, 'id' => $activity->target_id]) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::PAGE_UPDATED)
    <?php $pageType = Page::$pageTypeNames[$activity->additional_data]; ?>
    updated {{ $pageType }} page <a href="{{ action('pages.show', ['type' => $pageType, 'id' => $activity->target_id]) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::PAGE_FILE_UPLOADED)
    <?php $pageType = Page::$pageTypeNames[$activity->additional_data]; ?>
    uploaded new attachment for {{ $pageType }} page <a href="{{ action('pages.show', ['type' => $pageType, 'id' => $activity->ref_id]) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::PAGE_FILE_DOWNLOADED)
    <?php $pageType = Page::$pageTypeNames[$activity->additional_data]; ?>
    downloaded attachment in {{ $pageType }} page <a href="{{ action('pages.show', ['type' => $pageType, 'id' => $activity->ref_id]) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::PAGE_FILE_DELETED)
    <?php $pageType = Page::$pageTypeNames[$activity->additional_data]; ?>
    deleted attachment in {{ $pageType }} page <a href="{{ action('pages.show', ['type' => $pageType, 'id' => $activity->ref_id]) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::PAGE_COMMENTED)
    <?php $pageType = Page::$pageTypeNames[$activity->additional_data]; ?>
    added comment to page <a href="{{ action('pages.show', ['type' => $pageType, 'id' => $activity->target_id]) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::RECORD_CREATED)
    created new record <a href="{{ action('records.show', $activity->target_id) }}">{{ $activity->target_content }}</a> in Case <a href="{{ action('cases.show', $activity->ref_id) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::RECORD_UPDATED)
    updated record <a href="{{ action('records.show', $activity->target_id) }}">{{ $activity->target_content }}</a> in Case <a href="{{ action('cases.show', $activity->ref_id) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::RECORD_FILE_UPLOADED)
    uploaded new attachment for record <a href="{{ action('records.show', $activity->ref_id) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::RECORD_FILE_DOWNLOADED)
    downloaded an attachment in record <a href="{{ action('records.show', $activity->ref_id) }}">{{ $activity->ref_content }}</a>
@elseif ($activity->action === History::RECORD_FILE_DELETED)
    deleted an attachment in record <a href="{{ action('records.show', $activity->target_id) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::USER_CREATED)
    created new user <a href="#">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::USER_UPDATED)
    updated user <a href="#">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::USER_BLOCKED)
    blocked <a href="#">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::USER_UNBLOCKED)
    unblocked <a href="#">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::USER_PASSWORD_CHANGED)
    changed his password
@elseif ($activity->action === History::USER_PASSWORD_RESET_INIT)
    initialized password reset
@elseif ($activity->action === History::SECURITY_GROUP_CREATED)
    created new security group <a href="{{ action('SecurityGroupsController@show', $activity->target_id) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::SECURITY_GROUP_UPDATED)
    updated security group <a href="{{ action('SecurityGroupsController@show', $activity->target_id) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::SECURITY_GROUP_USER_ADDED)
    added <a href="#">{{ $activity->ref_content}}</a> to <a href="{{ action('SecurityGroupsController@show', $activity->target_id) }}">{{ $activity->target_content }}</a>
@elseif ($activity->action === History::SECURITY_GROUP_USER_REMOVED)
    removed <a href="#">{{ $activity->ref_content }}</a> from <a href="{{ action('SecurityGroupsController@show', $activity->target_id) }}">{{ $activity->target_content }}</a>
@else
    {{ History::$actionMapping[$activity->action] }}
@endif
