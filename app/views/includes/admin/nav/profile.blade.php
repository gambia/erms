<li>
    <a href="{{ route('users.show', Auth::User()->id) }}">
        <i class="entypo-user"></i>
        View Profile
    </a>
</li>

<li>
    <a href="{{ route('users.changePassword', Auth::User()->id) }}">
        <i class="entypo-lock"></i>
        Change Password
    </a>
</li>
<li>
    <a href={{ URL::to('logout') }}>
        <i class="entypo-logout"></i>
        Log Out
    </a>
</li>