<li>
    <a href="{{ route('documentation.index') }}">
        <i class="entypo-info-circled"></i>
        User Manual
    </a>
</li>

<li>
    <a href="" data-toggle="modal" data-target="#support">
        <i class="entypo-help-circled"></i>
        Support
    </a>
</li>