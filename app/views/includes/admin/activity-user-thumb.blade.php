<aside class="user-avatar">
    <a href="{{ route('users.show', $user->id) }}">
        <img src="{{ $user->getAvatarLink('/assets/admin/images/member.jpg') }}" alt="" class="img-circle" width="32" height="32">
    </a>
</aside>