<div class="row">
    <div class="col-md-12">
        <table class="table invert-border">
            @foreach ($history as $key => $item)
                <tr>
                    <td>
                    @if (isset($item->user_id))
                        {{ $item->user->getName() }}
                    @else
                        Anonymous
                    @endif

                    @include('includes/admin/activity-action', array('activity' => $item))

                    &mdash; <small>{{ Dates::showTimeAgo($item->created_at) }} </small>
                    </td>
                </tr>
        @endforeach
        </table>
    </div>
</div>
