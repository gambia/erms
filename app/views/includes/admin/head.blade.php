<head>
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta name="description" content="K1CMS Admin Panel" />
    <meta name="author" content="" />

    <title>K1CMS</title>

    <link rel="stylesheet" href="/assets/admin/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="/assets/admin/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="/assets/admin/css/font-icons/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="/assets/admin/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/admin/css/bootstrap-theme.css">

    <link rel="stylesheet" href="/assets/admin/css/awesome-bootstrap-checkbox.css">

    <link rel="stylesheet" href="/assets/admin/js/bootstrap-color-picker/css/bootstrap-colorpicker.min.css">

    <link rel="stylesheet" href="/assets/admin/css/custom.css">

    <script src="/assets/admin/js/jquery-1.11.0.min.js"></script>
    <script src="/assets/admin/js/jquery-1.11.0.min.js"></script>

    <script src="/assets/admin/js/raphael-min.js"></script>
    <script src="/assets/admin/js/morris.min.js"></script>
    <script src="/assets/admin/js/bootstrap-color-picker/js/bootstrap-colorpicker.min.js"></script>

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>