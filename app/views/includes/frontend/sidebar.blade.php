<div class="sidebar">
    
    <h3>
        <i class="entypo-list"></i>
        Categories
    </h3>
    
    
    <div class="sidebar-content">
        
        <ul>
            <li @if ($type === 'pages') class="active" @endif>
                <a href="{{ action('clientHome', array('clientKey' => $client->client_key)) }}">Reports <span>({{ $pagesCount }})</span></a>
            </li>
            <li @if ($type === 'forms') class="active" @endif>
                <a href="{{ action('clientHome', array('clientKey' => $client->client_key, 'type' => 'forms')) }}">Forms <span>({{ $formsCount }})</span></a>
            </li>
        </ul>
        
    </div>
    
</div>