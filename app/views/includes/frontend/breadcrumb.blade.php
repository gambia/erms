<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h1>{{$client->name}}</h1>
                <ol class="breadcrumb bc-3" >
                    <li>
                        <a href="{{ action('ClientHomeController@index', $client->client_key) }}"><i class="fa-home"></i>Home</a>
                    </li>
                    @if ($crumb === 'pages')
                    <li class="active">
                        <strong>Latest Posts</strong>
                    </li>
                    @elseif ($crumb === 'forms')
                    <li class="active">
                        <strong>Latest Forms</strong>
                    </li>
                    @elseif ($crumb === 'contact')
                    <li class="active">
                        <strong>Contact</strong>
                    </li>
                    @elseif ($crumb === 'search')
                    <li class="active">
                        <strong>Search for "<em>{{$query}}</em>"</strong>
                    </li>
                    @elseif ($crumb === 'article')
                    <li>
                        @if ($article->type === Page::TYPE_INTRANET)
                            <a href="{{ action('clientHome', $client->client_key) }}">Reports</a>
                        @elseif ($article->type === PAGE::TYPE_FORM)
                            <a href="{{ action('clientHome', array('client_key' => $client->client_key, 'type' => 'forms')) }}">Forms</a>
                        @endif
                    </li>
                    <li class="active">
                        <strong>{{ $article->title }}</strong>
                    </li>
                    @endif
                </ol>
            </div>
            <div class="col-sm-3">

            </div>
        </div>
    </div>
</section>