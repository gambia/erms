@if (isset($client))
<section class="footer-widgets">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">

                <h5>{{ $client->name }}</h5>
                <p>
                    {{$client->about}}
                </p>

            </div>

            <div class="col-sm-3">

                <h5>Address</h5>

                <p>
                    {{$client->billing_address_line_1}} <br />

                    @if ($client->billing_address_line_2)
                        {{$client->billing_address_line_2}} <br />
                    @endif

                    {{$client->billing_city}} {{$client->billing_state}} {{$client->billing_zip}}<br />
                    {{$client->billing_country}}
                </p>

            </div>

            <div class="col-sm-3">

                <h5>Contact</h5>

                <p>
                    Phone: {{$client->phone}}<br />
                    Fax: {{$client->fax}}<br />
                    <a href="mailto:{{$client->email}}">{{$client->email}}</a><br />
                    <a href="http://{{$client->website}}" target="_blank">{{$client->website}}</a>
                </p>

            </div>

        </div>

    </div>

</section>
@endif

<!-- Site Footer -->
<footer class="site-footer">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">
                Copyright &copy; EDRC GmbH - All Rights Reserved. 
            </div>

        </div>

    </div>

</footer>