<div class="site-header-container container">
    <div class="row">
        <div class="col-md-12">
            <header class="site-header clearfix">
                <section class="site-logo pull-left">
                    @if(isset($client))
                        <a href="{{ action('ClientHomeController@index', $client->client_key) }}">
                            <img src="{{$client->logo_url}}" class="logo" />
                        </a>
                    @endif
                </section>

                <nav class="site-nav pull-right">
                    <ul class="main-menu hidden-xs" id="main-menu">
                        <li @if ($crumb !== 'contact') class="active" @endif>
                            <a href="{{ action('clientHome', array('clientKey' => $client->client_key)) }}">
                                <span>Home</span>
                            </a>
                        </li>
                        <li @if ($crumb === 'contact') class="active" @endif>
                            <a href="{{ action('clientContact', array('clientKey' => $client->client_key)) }}">
                                <span>Contact</span>
                            </a>
                        </li>
                        <li>
                            <form action="{{ action('clientSearch', array('clientKey' => $client->client_key)) }}"method="GET" class="search-form">
                                <i class="entypo-search"></i>
                                <input name="q" placeholder="Search" value="@if (isset($query)){{$query}}@endif">
                            </form>
                        </li>
                    </ul>

                    <div class="visible-xs mobileNav">
                        <ul>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown">
                                    <i class="entypo-menu"></i>
                                </a>

                                <div class="dropdown-menu">
                                    <ul>
                                        <li @if ($crumb !== 'contact') class="active" @endif>
                                            <a href="{{ action('clientHome', array('clientKey' => $client->client_key)) }}">
                                                <span>Home</span>
                                            </a>
                                        </li>
                                        <li @if ($crumb === 'contact') class="active" @endif>
                                            <a href="{{ action('clientContact', array('clientKey' => $client->client_key)) }}">
                                                <span>Contact</span>
                                            </a>
                                        </li>
                                        <li>
                                            <form action="{{ action('clientSearch', array('clientKey' => $client->client_key)) }}"method="GET" class="search-form">
                                                <i class="entypo-search"></i>
                                                <input name="q" placeholder="Search" value="@if (isset($query)){{$query}}@endif">
                                            </form>
                                        </li>
                                    </ul>

                                    @include('includes.frontend.sidebar')
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        </div>
    </div>
</div>