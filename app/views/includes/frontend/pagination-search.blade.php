<div class="text-center">
    <ul class="pagination">
        @if ($pageNumber > 1)
            <li><a href="{{ URL::route('clientSearch', array('client_key' => $client->client_key, 'q' => $query)) }}">First</a></li>
            <li><a href="{{ URL::route('clientSearch', array('client_key' => $client->client_key, 'q' => $query, 'page' => $pageNumber - 1)) }}"><i class="entypo-left-open-mini">&nbsp;</i></a></li>
        @endif

        @for ($i=$startPage; $i<=$endPage; $i++)
            <li @if ($pageNumber == $i) class="active" @endif><a href="{{ URL::route('clientSearch', array('client_key' => $client->client_key, 'q' => $query, 'page' => $i)) }}">{{ $i }}</a></li>
        @endfor

        @if (($pageNumber + 1) <= $count)
            <li><a href="{{ URL::route('clientSearch', array('client_key' => $client->client_key, 'q' => $query, 'page' => $pageNumber + 1)) }}"><i class="entypo-right-open-mini">&nbsp;</i></a></li>
            <li><a href="{{ URL::route('clientSearch', array('client_key' => $client->client_key, 'q' => $query, 'page' => $count)) }}">Last</a></li>
        @endif

    </ul>
</div>