    <!-- Bottom scripts (common) -->
    <script src="/assets/fe/js/gsap/main-gsap.js"></script>
    <script src="/assets/fe/js/bootstrap.js"></script>
    <script src="/assets/fe/js/joinable.js"></script>
    <script src="/assets/fe/js/resizeable.js"></script>


    <!-- JavaScripts initializations and stuff -->
    @include('includes.shared.analytics')