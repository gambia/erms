@extends('layouts.frontend')

@section('content')

<section class="blog">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="blog-posts">

                    <!-- Blog Post -->
                    <div class="blog-post">
                        <div class="post-thumb">
                            <a href="blog-post.html">
                                <img src="/assets/fe/images/blog-thumb-1.png" class="img-rounded" />
                                <span class="hover-zoom"></span>
                            </a>
                        </div>

                        <div class="post-details">
                            <h3>
                                <a href="blog-post.html">We hill lady will both sang room by</a>
                            </h3>
                            <div class="post-meta">
                                <div class="meta-info">
                                    <i class="entypo-calendar"></i> 09 December 2014                                </div>
                                <div class="meta-info">
                                    <i class="entypo-comment"></i>
                                    3 comments
                                </div>
                            </div>
                            <p>Paid was hill sir high. For him precaution any advantages dissimilar comparison few terminated projecting. Prevailed discovery immediate objection of ye at. Repair summer one winter living feebly pretty his. In so sense am known these since.</p>
                        </div>
                    </div>

                    <!-- Blog Post -->
                    <div class="blog-post">
                        <div class="post-thumb">
                            <a href="blog-post.html">
                                <img src="/assets/fe/images/blog-thumb-1.png" class="img-rounded" />
                                <span class="hover-zoom"></span>
                            </a>
                        </div>
                        <div class="post-details">
                            <h3>
                                <a href="blog-post.html">Allowance sweetness direction to as...</a>
                            </h3>
                            <div class="post-meta">
                                <div class="meta-info">
                                    <i class="entypo-calendar"></i> 07 December 2014                                </div>
                                <div class="meta-info">
                                    <i class="entypo-comment"></i>
                                    0 comments
                                </div>
                            </div>
                            <p>Paid was hill sir high. For him precaution any advantages dissimilar comparison few terminated projecting. Prevailed discovery immediate objection of ye at. Repair summer one winter living feebly pretty his. In so sense am known these since.</p>
                        </div>
                    </div>

                    <!-- Blog Post -->
                    <div class="blog-post">
                        <div class="post-thumb">
                            <a href="blog-post.html">
                                <img src="/assets/fe/images/blog-thumb-1.png" class="img-rounded" />
                                <span class="hover-zoom"></span>
                            </a>
                        </div>
                        <div class="post-details">
                            <h3>
                                <a href="blog-post.html">Is we miles ready he might going</a>
                            </h3>
                            <div class="post-meta">
                                <div class="meta-info">
                                    <i class="entypo-calendar"></i> 06 December 2014                                </div>
                                <div class="meta-info">
                                    <i class="entypo-comment"></i>
                                    1 comment
                                </div>
                            </div>
                            <p>Paid was hill sir high. For him precaution any advantages dissimilar comparison few terminated projecting. Prevailed discovery immediate objection of ye at. Repair summer one winter living feebly pretty his. In so sense am known these since.</p>
                        </div>
                    </div>

                    <!-- Blog Pagination -->
                    <div class="text-center">
                        <ul class="pagination">
                            <li class="active">
                                <a href="#">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                            <li>
                                <a href="#">3</a>
                            </li>
                            <li>
                                <a href="#">4</a>
                            </li>
                            <li>
                                <a href="#">5</a>
                            </li>
                            <li>
                                <a href="#">Next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                @include('includes.frontend.sidebar')
            </div>
        </div>
    </div>
<section>


@endsection
