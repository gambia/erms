@extends('layouts.frontend')

@section('content')

@include('includes.frontend.breadcrumb', array('client'=>$client))

<section class="blog">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="blog-posts">

                    @if (isset($search))
                        <div class="blog-post">
                            <div class="post-details">
                                @if ($itemsCount > 0)
                                <h3>Results for your search query "<em>{{ $query }}</em>"</h3>
                                <p><small>Showing {{ (($pageNumber-1) * $limit) + 1 }} -
                                    <?php $intervalTop = (($pageNumber-1) * $limit) + $limit; ?>
                                    @if ($intervalTop <= $itemsCount) {{ $intervalTop }} @else {{ $itemsCount }} @endif
                                    of {{ $itemsCount }} @if ($itemsCount === 1) result @else results @endif</small></p>
                                @else
                                <h3>We didn't find any pages matching "<em>{{$query}}</em>"</h3>
                                @endif
                                <hr>
                            </div>
                        </div>
                    @endif

                    @if (isset($items) && $items->count())
                        @foreach ($items as $item)
                            <div class="blog-post">
                                <div class="post-details">
                                    <h3>
                                        <a href="{{ action('clientArticle', array('clientKey' => $client->client_key, 'slug' => $item->slug)) }}">{{ $item->title }}</a>
                                    </h3>
                                    <div class="post-meta">
                                        <div class="meta-info">
                                            <i class="entypo-calendar"></i> {{ Dates::localizeDateTime($item->created_at)->format('d F Y') }}
                                            @if ($item->files->count()) |
                                                <i class="entypo-download"></i> {{ $item->files->count() }} Attachments
                                            @endif
                                        </div>
                                    </div>
                                    <p>{{{ str_limit(strip_tags($item->description), 300, $end = '...') }}}</p>
                                </div>
                            </div>
                        @endforeach
                    @elseif (isset($article))
                        <div class="blog-post">
                            <div class="post-details">
                                <h3>
                                    <a href="{{ action('clientArticle', array('clientKey' => $client->client_key, 'slug' => $article->slug)) }}">{{ $article->title }}</a>
                                </h3>
                                <div class="post-meta">
                                    <div class="meta-info">
                                        <i class="entypo-calendar"></i> {{ Dates::localizeDateTime($article->created_at)->format('d F Y') }}
                                        @if ($article->files->count()) |
                                            <i class="entypo-download"></i> {{ $article->files->count() }} Attachments
                                        @endif
                                    </div>
                                </div>
                                <p>{{ $article->description }}</p>

                                @if ($article->files->count())
                                <hr>
                                <div class="post-additionals">
                                    <h4>Available for download:</h4>
                                    <ul>
                                        @foreach ($article->files as $file)
                                            <li><a href="{{ action('pages.download', $file->id) }}">{{ $file->filename }} [{{ $file->getHumanSize() }}]</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <?php $children = $article->getDescendants(); ?>
                                @if ($children->count())
                                <hr>
                                <div class="post-additionals">
                                    <h4>Read Next:</h4>
                                    <ul>
                                        @foreach ($children as $child)
                                            <li><a href="{{ action('clientArticle', array('clientKey' => $client->client_key, 'slug' => $child->slug)) }}">{{ $child->title }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>
                        </div>
                    @else
                        @if (!isset($search))
                            <h2>We are sorry!<br><Br>There are no public posts yet.</h2>
                        @endif
                    @endif

                @if (isset($items))
                    <?php
                        $count = ceil($itemsCount / $limit);

                        $startPage = ($pageNumber < 5)? 1 : $pageNumber - 4;
                        $endPage = 8 + $startPage;
                        $endPage = ($count < $endPage) ? $count : $endPage;
                        $diff = $startPage - $endPage + 8;
                        $startPage -= ($startPage - $diff > 0) ? $diff : 0;
                    ?>

                    @if ($count > 1)
                        @if (isset($search))
                            @include('includes.frontend.pagination-search')
                        @else
                            @include('includes.frontend.pagination-items')
                        @endif
                    @endif
                @endif

                </div>
            </div>

            <div class="col-sm-4">
                @include('includes.frontend.sidebar')
            </div>
        </div>
    </div>
<section>

@endsection
