@extends('layouts.frontend')

@section('content')

<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function initialize()
{
    var mapDiv = document.getElementById('map');
    var map = new google.maps.Map(mapDiv, {
        center: new google.maps.LatLng({{$client->map_longitude}}, {{$client->map_latitude}}),
        zoom: {{$client->map_zoom}},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    });
    
    new google.maps.Marker({
        position: new google.maps.LatLng({{$client->map_longitude}}, {{$client->map_latitude}}),
        map: map
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<section class="contact-map" id="map"></section>

<section class="contact-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-7 sep contact-form-wrapper">

                <h4>Get in touch with us, write us an e-mail!</h4>

                @if (Session::get('submitSuccess'))
                    <div class="alert alert-success" role="alert">
                        <p>Your message has been successfully sent. We will be contacting you shortly.</p>
                    </div>
                @elseif ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <h4>Please fix the following errors:</h4>
                        <ul>
                            @foreach ($errors->all() as $err)
                                <li>{{ $err }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="contact-form" role="form" method="post" action="" enctype="application/x-www-form-urlencoded">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="Name:" />
                    </div>

                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="E-mail:" />
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Message:" rows="6"></textarea>
                    </div>

                    <div class="form-group text-right">
                        <button class="btn btn-primary" name="send">Send</button>
                    </div>
                </form>
            </div>

            <div class="col-sm-offset-1 col-sm-4">
                <div class="info-entry">
                    <h4>Address</h4>

                    <p>
                        {{$client->billing_address_line_1}} <br />

                        @if ($client->billing_address_line_2)
                            {{$client->billing_address_line_2}} <br />
                        @endif

                        {{$client->billing_city}} {{$client->billing_state}} {{$client->billing_zip}}<br />
                        {{$client->billing_country}}

                        <br />
                        <br />
                    </p>
                    @if ($client->opening_hours)
                    <p>
                        <strong>Working Hours:</strong><br />
                        {{$client->opening_hours}}
                        <br />
                        <br />
                    <p>
                    @endif
                </div>

                <div class="info-entry">
                    <h4>Call Us</h4>
                    <p>
                        Phone: {{$client->phone}}<br />
                        Fax: {{$client->fax}}<br />
                        <a href="mailto:{{$client->email}}">{{$client->email}}</a><br />
                        <a href="http://{{$client->website}}" target="_blank">{{$client->website}}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection