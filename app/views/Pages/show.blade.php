@extends('layouts.client')

@section('content')

@include('Pages/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-7 col-sm-7 col-xs-7">
        <h1>{{$page->project->project_key}} - {{ $page->title }}</h1>
    </div>

    <div class="col-md-5 col-sm-5 col-xs-5 actionbar">
        <a href={{ action('pages.edit', array('type' => $pageType, 'id' => $page->id)) }} class="btn btn-primary">
            <i class="entypo-plus"></i>
            @if ($pageType == 'wiki')
                Edit Page
            @elseif ($pageType == 'intranet')
                Edit Page
            @elseif ($pageType == 'forms')
                Edit Form Page
            @endif
        </a>

        <a class="btn btn-danger" data-toggle="modal" data-target="#deletePage-{{ $page->id }}">
            <i class="entypo-minus"></i>Delete
        </a>
    </div>

</div>

@include('includes/admin/modalDelete', array(
    'modalId'=> 'deletePage-'.$page->id,
    'modalFormRoute' => ['route' => ['pages.destroy', $page->id], 'method' => 'delete'],
    'modalTitle' => $page->title
))

<div class="row">
    <div class="col-md-12 documentation-page">
        {{ $page->description }}
    </div>
</div>

@foreach ($page->getDescendants() as $sub)
<div class="row">
    <div class="col-md-12 documentation-page">
        <h2><a href="{{ action('pages.show', array('type' => $pageType, 'id' => $sub->id)) }}">{{ $sub->title }}</a></h2>
        {{ $sub->description }}
    </div>
</div>

@endforeach

<br>

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="#attachments">Files</a></li>
            <li role="presentation"><a href="#comments">Comments</a></li>
            <li role="presentation"><a href="#history">History</a></li>
        </ul>
    </div>
</div>

<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="attachments">
        @include('Pages/show/attachments')
    </div>

    <div role="tabpanel" class="tab-pane" id="comments">
        <!-- Button trigger modal -->
        <div class="row">
            <div class="col-md-12">
                <a class="pull-right btn btn-success" data-toggle="modal" data-target="#myModal">
                  <i class="entypo-plus"></i>Add Comment
                </a>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            {{ Form::open(array('route' => array('pages.postComment', $pageType, $page->id), 'class'=>'comment-form', 'role' => 'form')) }}
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Add Comment</h4>
              </div>
              <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::textarea('comment', null, array('placeholder' => 'Your comment', 'class' => 'form-control', 'rows' => 2)) }}
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
              </div>

              <div class="modal-footer">
                <button type="submit" class="btn btn-success pull-right">
                   Submit Comment
                </button>
              </div>
            </div>
            {{ Form::close() }}
          </div>
        </div>

        @include('Pages/show/comments', array('comments'=>$page->comments))

        <br /><br />
    </div>



    <div role="tabpanel" class="tab-pane" id="history">
        @include('includes/admin/history')
    </div>
</div>

<script type="text/javascript">
    var bindClickListener = function (object) {
        object.click(function (e) {
            e.preventDefault()
            object.tab('show')
        });
    }

    $(function() {
        $('.nav.nav-tabs').find('a').each(function(){
            bindClickListener($(this));
        });

        $('.nav.nav-tabs li:first a').tab('show');
    });
</script>

@endsection
