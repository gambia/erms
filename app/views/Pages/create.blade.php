@extends('layouts.client')

@section('content')

@include('Pages/crumbs')

<h2>
    @if ($pageType == 'wiki')
        Create a New Wiki Page
    @elseif ($pageType == 'intranet')
        Create a New Intranet Page
    @elseif ($pageType == 'forms')
        Create a New Form
    @endif
</h2>

<br />

@include('Pages/form')

@endsection
