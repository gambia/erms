@extends('layouts.client')

@section('content')

@include('Pages/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-9 col-sm-7 col-xs-7">
        <h1>
            @if ($pageType == 'wiki')
                Wiki Pages
            @elseif ($pageType == 'intranet')
                Intranet Pages
            @elseif ($pageType == 'forms')
                Forms
            @endif
        </h1>
    </div>

    @if ($canCreate)
    <div class="col-md-3 col-sm-5 col-xs-5">
        <a href={{ URL::route('pages.create', $pageType) }} class="btn btn-primary pull-right">
            <i class="entypo-plus"></i>
            @if ($pageType == 'wiki')
                Create a New Wiki Page
            @elseif ($pageType == 'intranet')
                Create a New Intranet Page
            @elseif ($pageType == 'forms')
                Create a New Form Page
            @endif
        </a>
    </div>
    @endif
</div>

@if ($pages->count())
    <?php $previousProject = null; ?>
    <div id="list-1" class="nested-list dd with-margins">
        @include('Pages/tree', array('pages' => $pages->filter(function($item) { return $item->depth === 1; }), 'level' => 1, 'previousLevel' => 0))
    </div>
@else
    <p>
        @if ($pageType == 'wiki')
            There are no Wiki pages created yet.
        @elseif ($pageType == 'intranet')
            There are no Intranet pages created yet.
        @elseif ($pageType == 'forms')
            There are no Forms created yet.
        @endif

        @if ($canCreate)
        <a href="{{ action('pages.create', array('type' => $pageType)) }}">Click here to create one.</a>
        @endif
    </p>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var markers = $('.project-marker.active');
        markers.on('click', function() {
            var m = $(this),
                li = $(this).parent('li');

                m.find('i').toggleClass('entypo-minus-circled');
                li.toggleClass('collapsed');

        });
    });
</script>

@endsection
