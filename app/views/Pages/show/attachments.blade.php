<div class="row">
    <div class="col-md-12">
        @if ($page->files->count())
            <div class="table-wrapper">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Filename</th>
                            <th>Size</th>
                            <th><center>Downloads</center></th>
                            <th>Uploaded</th>
                            <th width="130px"></th>
                            <th width="110px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($page->files as $file)
                            <?php $downloads = $file->getDownloads(); ?>
                            <tr>
                                <td>{{ $file->filename }}</a></td>
                                <td>{{ $file->getHumanSize() }}</td>
                                <td><center><span data-toggle="tooltip" data-placement="top" title="@foreach ($downloads as $down) {{$down->ip_address}} - @if ($down->user_id) {{ $down->user->getName() }} @else Anonymous @endif - {{ Dates::localizeDateTime($down->created_at)->format('Y-m-d H:i') }}<br> @endforeach">{{ $downloads->count() }}</span></center></td>
                                <td>{{ Dates::localizeDateTime($file->created_at)->format('Y-m-d H:i') }}</td>
                                <td><a href="{{ URL::route('pages.download', $file->id) }}" class="btn btn-primary" target="_blank">Download File</a></td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal-{{ $file->id }}">
                                      Delete File
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal-{{ $file->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Deleting {{ $file->filename }}</h4>
                                          </div>
                                          <div class="modal-body">
                                            Do you really want to delete this file? You can't undo this action.
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <a href="{{ URL::route('pages.deleteFile', $file->id) }}"
                                                class="btn btn-danger">Delete File</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <p>There are no attachments in this page.</p>
        @endif

        @include ('Pages/fileupload')

    </div>
</div>

<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip({
    'html': true,
    'template': '<div class="tooltip bigger" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    });
});
</script>
