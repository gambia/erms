@if ($pages->count())
	<ul class="dd-list">
	@foreach ($pages as $idx => $page)
	<?php $isNewProject = ($previousProject !== $page->project->project_key); ?>
	    <li class="dd-item collapsed" data-project="{{ $page->project->project_key }}-level-{{ $idx }}">

	    	@if (($isNewProject || $previousLevel !== $level) && $page->children->count() > 0)
	    		<div class="project-marker active" data-project="{{ $page->project->project_key }}-level-{{ $idx }}"><i class="entypo-plus-circled"></i></div>
	    	@else
	    		<div class="project-marker">&nbsp;</div>
	    	@endif

	    	<a href="{{ action('pages.show', array('type' => $pageType, 'id' => $page->id)) }}"><div class="dd-handle">
	            {{ $page->project->project_key}} - {{ $page->title }}
	        </div></a>
	        @include('Pages/tree', array('pages' => $page->children, 'previousProject' => $page->project->project_key, 'previousLevel' => $level,'level' => $level+1))
	        <?php $previousProject = $page->project->project_key; ?>
	    </li>
	@endforeach
	</ul>
@endif