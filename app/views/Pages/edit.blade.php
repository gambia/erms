@extends('layouts.client')

@section('content')

@include('Pages/crumbs')

<h2>{{ $page->project->project_key }} - {{ $page->title }}</h2>


@include('Pages/form')

@endsection
