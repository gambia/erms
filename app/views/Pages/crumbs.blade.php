<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="{{ route('pages.index', $pageType) }}">
        @if ($pageType == 'wiki')
            Wiki
        @elseif ($pageType == 'intranet')
            Intranet
        @elseif ($pageType == 'forms')
            Forms
        @endif
        </a>
    </li>
    @if (isset($page))
    @if (is_null($page->id))
        <li>
        <a href="{{ route('pages.create', $pageType) }}">
        @if ($pageType == 'wiki')
            Create a New Wiki Page
        @elseif ($pageType == 'intranet')
            Create a New Intranet Page
        @elseif ($pageType == 'forms')
            Create a New Form
        @endif
        </a>
        </li>
    @else

        @if ($page->depth > 1)
            @foreach ($page->getAncestors() as $parent)
                @if ($parent->depth > 0)
                <li>
                    <a href="{{ route('pages.show', array('type' => $pageType, 'id' => $parent->id)) }}">{{ $parent->project->project_key }} - {{ $parent->title }}</a>
                </li>
                @endif
            @endforeach
        @endif
        <li>
            <a href="{{ route('pages.show', array('type' => $pageType, 'id' => $page->id)) }}">{{ $page->project->project_key }} - {{ $page->title }}</a>
        </li>
        @endif

    @endif
</ol>