@include('includes/admin/errors')

@if (isset($page) && !is_null($page->id))
    {{ Form::model($page, array('route' => array('pages.update', $pageType, $page->id), 'class'=>'form-horizontal form-groups-bordered', 'method' => 'put', 'role' => 'form')) }}
@else
    {{ Form::open(array('route' => array('pages.store', $pageType), 'class'=>'form-horizontal form-groups-bordered', 'role' => 'form')) }}
@endif

    <div class="row">
        <div class="col-sm-6">
            {{ Form::label('Title:','', array('class'=>'control-label')) }}<br />
            {{ Form::text('title', $page->name, array('class'=>'form-control input-lg')) }}
        </div>

        <div class="col-sm-3">
          {{ Form::label('Project:','', array('class'=>'control-label')) }}

          <?php
            $projectsTree = array();
            foreach ($projects as $project)
            {
              if ($project->hasEnabledCases()) {
                $projectsTree[$project->id] = str_repeat('--', $project->depth-1) . ' ' . $project->getFullName();
              }
            }
          ?>

          {{
              Form::select(
                  'project_id',
                  $projectsTree,
                  $page->project_id,
                  array(
                      'class'=>'form-control input-lg',
                      'name' => 'project_id',
                      'id' => 'project_id'
                  )
              )
          }}
        </div>

        <div class="col-sm-3">
          <?php
            function getPageTree($pages) {
              $items = array();
              foreach ($pages as $page) {
                $items[$page->id] = str_repeat('--', $page->depth - 1) . ' ' . $page->project->project_key . ' ' . $page->title;
                $items = $items + getPageTree($page->children);
              }
              return $items;
            }
            $tree = [$rootPage->id => 'None'] + getPageTree($pages);
          ?>
          {{ Form::label('parent_id','Parent Page:', array('class'=>'control-label')) }}
          {{
              Form::select(
                  'parent_id',
                  $tree,
                  $page->parent_id,
                  array(
                      'class'=>'form-control input-lg',
                      'name' => 'parent_id',
                      'id' => 'parent_id'
                  )
              )
          }}
        </div>

    </div>

    @if (false) // Hidden functionality
    <br />
      <div class="row">
        <div class="col-sm-9">

        </div>
        <div class="col-sm-3">
          <?php
            $types = Page::$pageTypeNames;
            if (!Auth::user()->isClientAdmin() && !Auth::user()->isAdmin()) {
              unset($types[Page::TYPE_INTRANET]);
              unset($types[Page::TYPE_FORM]);
            }

            foreach ($types as $k=>$type) {
              $types[$k] = ucfirst($type);
            }
          ?>
          {{ Form::label('type:','Move page to:', array('class'=>'control-label')) }}
          {{
              Form::select(
                  'type',
                  $types,
                  $page->type,
                  array(
                      'class'=>'form-control input-lg page-type-select',
                      'name' => 'type'
                  )
              )
          }}
        </div>
      </div>
    @endif

    <br />

    <!-- WYSIWYG - Content Editor -->
    <div class="row">
        <div class="col-sm-12">
            {{ Form::label('Description:','', array('class'=>'control-label')) }}
            {{ Form::textarea('description', $page->description, ['id'=>'wysihtml5', 'class' => 'form-control wysihtml5', 'data-stylesheet-url' => 'assets/css/wysihtml5-color.css']) }}
        </div>
    </div>

    <br />

    <div class="row">
        <div class="col-sm-9">

        </div>
        <div class="col-sm-3 post-save-changes">
          <button type="submit" class="btn btn-success pull-right">
          @if ($pageType == 'wiki')
              @if (isset($page) && is_null($page->id)) Create a New Wiki Page @else Update the Page @endif
          @elseif ($pageType == 'intranet')
              @if (isset($page) && is_null($page->id)) Create a New Intranet Page @else Update the Page @endif
          @elseif ($pageType == 'forms')
              @if (isset($page) && is_null($page->id)) Create a New Form Page @else Update the Form @endif
          @endif
          </button>
        </div>
    </div>

{{ Form::close() }}

<script type="text/javascript">
  $(document).ready(function() {
    $('.page-type-select').on('change', function() {
      var types = {
        @foreach (array_flip(Page::$pageTypes) as $key=> $type)
          {{ $key }}: '{{$type}}',
        @endforeach
      },
        select = $(this);

      var url = '{{ action('pages.create', array('type' => '__TYPE__')) }}';
      $.get(url.replace('__TYPE__', types[$(this).val()]), function(response) {
        var $doc = $(response);
        $('#project_id').html($doc.find('#project_id').html());
        $('#parent_id').html($doc.find('#parent_id').html());
      });

    });
  });
</script>
