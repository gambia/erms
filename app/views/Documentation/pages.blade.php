<ol class="page-list">
	@foreach ($pages as $p)
		<li>
			<h2 id="p-{{$p->id}}"><a href="{{ action('documentation.show', $p->id) }}">{{ $p->title }}</a> @if (Auth::user()->isAdmin()) (<a href="{{ action('documentation.edit', $p->id) }}">edit</a>)@endif</h2><br>
        	<div class="page-content">{{ $p->description }}</div>

        	@if ($p->children->count())
        		@include('Documentation/pages', array('pages' => $p->children))
        	@endif
		</li>
	@endforeach
</ol>