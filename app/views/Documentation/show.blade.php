@extends('layouts.client')

@section('content')

@include('Documentation/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-6 col-sm-6 col-xs-5">
        <h1>{{ $page->title }}</h1>
    </div>

    @if ($canEdit)
    <div class="col-md-6 col-sm-6 col-xs-7 actionbar">
        <a href={{ action('documentation.edit', $page->id) }} class="btn btn-primary">
            <i class="entypo-plus"></i>
            Edit Documentation Page
        </a>

        <a class="btn btn-danger" data-toggle="modal" data-target="#deletePage-{{ $page->id }}">
            <i class="entypo-minus"></i>Delete
        </a>

    </div>
    @endif
</div>

@include('includes/admin/modalDelete', array(
    'modalId'=> 'deletePage-'.$page->id,
    'modalFormRoute' => ['route' => ['documentation.destroy', $page->id], 'method' => 'delete'],
    'modalTitle' => 'Deleting '.$page->title
))

@if ($page->children->count())
<div class="row">
    <div class="col-md-12">
        @include('Documentation/contents', array('contents' => $page->children))
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12 documentation-page">
        {{ $page->description }}
    </div>
</div>

@if ($page->children->count())
<div class="row documentation-page page-children-full">
    <div class="col-md-12">
        @include('Documentation/pages', array('pages' => $page->children))
    </div>
</div>
@endif

<br>

@if (Auth::user()->isAdmin())

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="#attachments">Files</a></li>
            </ul>
        </div>
    </div>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="attachments">
            @include('Documentation/show/attachments')
        </div>
    </div>

    <script type="text/javascript">
        var bindClickListener = function (object) {
            object.click(function (e) {
                e.preventDefault()
                object.tab('show')
            });
        }

        $(function() {
            $('.nav.nav-tabs').find('a').each(function(){
                bindClickListener($(this));
            });

            $('.nav.nav-tabs li:first a').tab('show');
        });
    </script>

@endif

@endsection
