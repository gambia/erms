<ol>
	@foreach ($contents as $contentPage)
		<li><a href="#p-{{$contentPage->id}}">{{ $contentPage->title }}</a>
			@if ($contentPage->children->count())
				@include('Documentation/contents-items', array('contents' => $contentPage->children))
			@endif
		</li>
	@endforeach
</ol>