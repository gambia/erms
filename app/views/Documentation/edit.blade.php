@extends('layouts.client')

@section('content')

@include('Documentation/crumbs')

<h2>
    Edit Documentation Page {{ $page->title }}
</h2>

<br />

@include('Documentation/form')

@endsection
