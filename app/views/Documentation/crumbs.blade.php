<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="{{ route('documentation.index') }}">
            User Manual
        </a>
    </li>
    @if (isset($page))
        @if (is_null($page->id))
            <li>
            <a href="{{ route('documentation.create') }}">
                Create a New Documentation Page
            </a>
            </li>
        @else

            @if ($page->depth > 1)
                @foreach ($page->getAncestors() as $parent)
                    @if ($parent->depth > 0)
                    <li>
                        <a href="{{ route('documentation.show', array('id' => $parent->id)) }}">{{ $parent->title }}</a>
                    </li>
                    @endif
                @endforeach
            @endif
            <li>
                <a href="{{ route('documentation.show', array('id' => $page->id)) }}">{{ $page->title }}</a>
            </li>
        @endif

    @endif
</ol>