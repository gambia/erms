@extends('layouts.client')

@section('content')

@include('Documentation/crumbs')

<h2>
    Create a New Documentation Page
</h2>

<br />

@include('Documentation/form')

@endsection
