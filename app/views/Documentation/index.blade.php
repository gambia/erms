@extends('layouts.client')

@section('content')

@include('Documentation/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-9 col-sm-7 col-xs-7">
        <h1>
            User Manual
        </h1>
    </div>

    @if ($canCreate)
    <div class="col-md-3 col-sm-5 col-xs-5">
        <a href={{ URL::route('documentation.create') }} class="btn btn-primary pull-right">
            <i class="entypo-plus"></i>
            Create a New User Manual Page
        </a>
    </div>
    @endif
</div>

@if ($documentation->count())
    <div class="documentation-tree documentation-page">
        <div id="list-1" class="nested-list dd with-margins">
            @include('Documentation/tree', array('pages' => $documentation->filter(function($item) { return $item->depth === 1; })))
        </div>
    </div>
@else
    <p>
        There is no Documentation yet.

        @if ($canCreate)
        <a href="{{ action('documentation.create') }}">Click here to create one.</a>
        @endif
    </p>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var markers = $('.project-marker.active');
        markers.on('click', function() {
            var m = $(this),
                li = $(this).parent('li');

                m.find('i').toggleClass('entypo-minus-circled');
                $('li[data-project="' + li.data('project') +'"]:not(:first)').toggleClass('collapsed');

        });

        var moveEventListener = function(e)
        {
            e.preventDefault();

            var a = $(this);

            $.get(a.attr('href'), function() {
                $.get('{{ action("documentation.index") }}', function(response) {
                    var $html = $(response);

                    $('body').find('.move').off('click');
                    $('body').find('.documentation-tree').html($html.find('.documentation-tree').html());
                    $('body').find('.move').on('click', moveEventListener);
                });
            });
        };

        $('body').find('.move').on('click', moveEventListener);
    });
</script>

@endsection
