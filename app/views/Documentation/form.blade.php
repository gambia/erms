@include('includes/admin/errors')

@if (isset($page) && !is_null($page->id))
    {{ Form::model($page, array('route' => array('documentation.update', $page->id), 'class'=>'form-horizontal form-groups-bordered', 'method' => 'put', 'role' => 'form')) }}
@else
    {{ Form::open(array('route' => array('documentation.store'), 'class'=>'form-horizontal form-groups-bordered', 'role' => 'form')) }}
@endif

<div class="row">
  <div class="col-sm-9">
    <strong>Is published: </strong><br>
    <div class="checkbox checkbox-info checkbox-inline">
      <input type="checkbox" name="is_published" id="public" value="1" @if ($page->published()) checked="checked"@endif >
      <label for="public">Publish Documentation Page</label>
    </div>
  </div>
</div>

<br>

<div class="row">
  <div class="col-sm-9">
    {{ Form::label('Title:','', array('class'=>'control-label')) }}<br />
    {{ Form::text('title', $page->name, array('class'=>'form-control input-lg')) }}
  </div>

<div class="col-sm-3">
  <?php
    function getPageTree($pages) {
      $items = array();
      foreach ($pages as $page) {
        $items[$page->id] = str_repeat('--', $page->depth - 1) . ' ' . $page->title;
        $items = $items + getPageTree($page->children);
      }
      return $items;
    }
    $tree = [$rootPage->id => 'None'] + getPageTree($pages);
  ?>

  {{ Form::label('parent_id','Parent Page:', array('class'=>'control-label')) }}
  {{
    Form::select(
      'parent_id',
      $tree,
      $page->parent_id,
      array(
        'class'=>'form-control input-lg',
        'name' => 'parent_id',
        'id' => 'parent_id'
      )
    )
}}
</div>

</div>

<br />

<!-- WYSIWYG - Content Editor -->
<div class="row">
  <div class="col-sm-12">
    {{ Form::label('Description:','', array('class'=>'control-label')) }}
    {{ Form::textarea('description', $page->description, ['id'=>'wysihtml5', 'class' => 'form-control wysihtml5', 'data-stylesheet-url' => 'assets/css/wysihtml5-color.css']) }}
  </div>
</div>

<br />

<div class="row">
  <div class="col-sm-9">

  </div>

  <div class="col-sm-3 post-save-changes">
    <button type="submit" class="btn btn-success pull-right">
      @if (isset($page) && is_null($page->id)) Create a New Documentation Page @else Update the Page @endif
    </button>
  </div>
</div>

{{ Form::close() }}