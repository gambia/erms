<h3>
    Upload Documentation attachments
</h3>

<div class="row">
    <div class="col-sm-12">
        <form action="{{ $uploadUrl }}" class="dropzone" id="page_dropzone">
            <input type="hidden" name="key" value="{{ $key }}">
            <input type="hidden" name="AWSAccessKeyId" value="{{ $accessKey }}">
            <input type="hidden" name="policy" value="{{ $policy }}">
            <input type="hidden" name="signature" value="{{ $signature }}">
            <input type="hidden" name="acl" value="{{ $acl }}">

            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </form>

        <div id="dropzone_info_table" class="hidden">

            <br />
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Dropzone Uploaded Files Info</div>
                </div>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="60%">File name</th>
                            <th width="20%">Size</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        DropzoneInitializer.initialize(
            '#page_dropzone',
            '#dropzone_info_table',
            '{{ URL::route('documentation.attachFile', ['id' => $page->id ]) }}'
        );
    });
</script>
