@if ($pages->count())
	<ul class="dd-list">
	@foreach ($pages as $page)
	    <li class="dd-item">
	    	@if (Auth::user()->isAdmin())
		    	<a href="{{ action('documentation.move', array('id' => $page->id, 'direction' => 'up')) }}" class="move move-up"><span class="entypo-up"></span></a>
		    	<a href="{{ action('documentation.move', array('id' => $page->id, 'direction' => 'down')) }}" class="move move-down"><span class="entypo-down"></span></a>
	    	@endif
	    	<a href="{{ action('documentation.show', $page->id) }}"><div class="dd-handle">
	            {{ $page->title }}
	        </div></a>
	        @include('Documentation/tree', array('pages' => $page->children))
	    </li>
	@endforeach
	</ul>
@endif