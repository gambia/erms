<!doctype html>
<html>
<head>
  <title>Swazi CMS</title>
</head>
<body>
  <menu>
    <a href={{ URL::to('logout') }}>Logout</a>
  </menu>

  <div class="container">
      @yield('content')
  </div>
</body>
</html>
