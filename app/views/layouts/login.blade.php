<!doctype html>
<html lang="en">
    @include('includes.admin.head')
    <body>
        @if (Auth::user())
            @include('includes.admin.header')
        @endif
        <div class="vertical-center">
            <div class="container">
                @yield('content')
            </div>
        </div>
        @include('includes.admin.foot')

    </body>
</html>