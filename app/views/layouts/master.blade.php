<!doctype html>
<html lang="en">
    @include('includes.admin.head')

    <body>
        
        @include('includes.admin.header')

        <!-- Modal -->
        <div class="modal fade" id="support" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Support</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            In case you require assistance, please contact K1CMS support (<a href="mailto:support@k1cms.com">support@k1cms.com</a>). Our speedy and knowledgeable support team will get back to you shortly.
                        <p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="k1-content-container">
            @include('includes.admin.sidebar')

            <div class="k1-page-content container col-md-12">
                

                @yield('content')

            </div>
        </div>

    @include('includes.admin.foot')
    </body>
</html>