<!doctype html>
<html lang="en">
    @include('includes.frontend.head')
    <body>
        <div class="wrap">

            @include('includes.frontend.header', array('client'=>isset($client) ? $client : NULL))

            @yield('content')

            <br /><br />

            @include('includes.frontend.footer', array('client'=>isset($client) ? $client : NULL))

        </div>

    @include('includes.frontend.foot')

    </body>
</html>