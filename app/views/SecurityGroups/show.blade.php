@extends('layouts.client')

@section('content')
  <h1>Security Group Detail</h1>

  <div>
    Name: {{$securityGroup->name}}
  </div>

  <div>
    {{ link_to_route('securitygroups.edit', 'Edit', $securityGroup->id) }}
  </div>
@endsection
