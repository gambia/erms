{{
    Form::model(
        $securityGroup,
        array(
            'route' => is_null($securityGroup->id) ? array('securitygroups.store') : array('securitygroups.update', $securityGroup->id),
            'class'=>'form-horizontal form-groups-bordered',
            'method' => is_null($securityGroup->id) ? 'POST' : 'PUT'
        )
    )
}}

<div class="row">
    <div class="col-md-6">
        {{ Form::label('Name:','', array('class'=>'control-label ')) }}
        {{ Form::text('name', $securityGroup->name, array('class'=>'form-control')) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::label('Members:','', array('class'=>'control-label')) }}
        {{
                Form::select(
                    'user_ids',
                    $users,
                    $selectedUsers,
                    array(
                        'class'=>'form-control',
                        'multiple' => 'multiple',
                        'name' => 'user_ids[]',
                        'id' => 'sg_users'
                    )
                )
            }}
    </div>
</div>   

<br>

<div class="row">
    <div class="col-md-6">
        <a href="{{ route('securitygroups.index') }}" class="btn btn-default">Cancel</a>
        {{ Form::submit('Save', array('class'=>'btn btn-success')) }}
    </div>
</div>

{{ Form::close() }}

<script type="text/javascript">
    $(document).ready(function() {
        $('#sg_users').select2();
    });
</script>