@extends('layouts.client')

@section('content')

<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        Security Groups
    </li>
</ol>

<div class="row k1-heading-row">
    <div class="col-md-9 col-sm-7">
        <h1>Security Groups</h1>
    </div>
</div>

@if ($currentUser->isClientAdmin() || $currentUser->isAdmin())
@include('includes/admin/errors')

<div class="row">
    <div class="col-sm-12">
        <label>Create a New Security Group: </label>
    </div>
</div>

{{ Form::model(
    $securityGroup,
    array(
        'route' => 'securitygroups.store',
        'class'=>'form-horizontal form-groups-bordered',
        'style'=>'width: 100%'
    ))
}}

{{ Form::hidden('client_id', $currentUser->client_id) }}

    <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-7">
            {{ Form::text('name', $securityGroup->name, array('class'=>'span2 form-control')) }}
        </div>

        <div class="col-md-5 col-sm-6 col-xs-5">
            {{ Form::submit('Create a New Security Group', array('class'=>'btn btn-primary')) }}
        </div>
    </div>
{{ Form::close() }}
<br /><br />
@endif

<div class="row">
    <div class="col-sm-12">

        <table class="table">
            <thead>
                <tr>
                    <th>Security Group Name</th>
                    <th class="noMobile">Members</th>
                </tr>
            </thead>
            @foreach($securityGroups as $securityGroup)
            <tr>
                <td style="vertical-align: middle; ">
                    @if ($currentUser->isClientAdmin() || $currentUser->isAdmin())
                        {{ link_to_route('securitygroups.edit', $securityGroup->name, $securityGroup->id) }}
                    @else
                        {{ $securityGroup->name }}
                    @endif
                </td>
                <td class="noMobile">
                    @foreach ($securityGroup->users()->orderBy('lastname', 'ASC')->get() as $user)
                        @if ($currentUser->isClientAdmin() || $currentUser->isAdmin())
                            <a href="{{ route('users.show', $user->id) }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ $user->getName() }}">
                                <img src="{{ $user->getAvatarLink('/assets/admin/images/member.jpg') }}" title="{{ $user->getName() }}" alt="{{ $user->getName() }}" class="avatar" width="36px" height="36px" />
                            </a>
                        @else
                            <img src="{{ $user->getAvatarLink('/assets/admin/images/member.jpg') }}" title="{{ $user->getName() }}" alt="{{ $user->getName() }}" class="avatar" width="36px" height="36px" />
                        @endif
                    @endforeach
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

@endsection
