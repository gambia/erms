@extends('layouts.client')

@section('content')
<h2>Create a New Security Group</h2>
<br />

@include('includes/admin/errors')
@include('SecurityGroups/form');

@endsection
