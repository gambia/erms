@extends('layouts.client')

@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="{{ action('securitygroups.index') }}">Security Groups</a>
    </li>
</ol>

<h2>Security Group - {{ $securityGroup->name }}</h2><br />

@include('includes/admin/errors')

@include('SecurityGroups/form')

@endsection


