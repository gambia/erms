@extends('layouts.client')

@section('content')

@include('Records/crumbs')


<div class="row k1-heading-row">
    <div class="col-md-7 col-sm-7 col-xs-7">
        <h1>
            <a href="{{ URL::route('cases.index') }}">{{ $record->title }}
        </h1>
    </div>

    <div class="col-md-5 col-sm-5 col-xs-5 actionbar">
        <a href={{ route('records.edit', array('id' => $record->id)) }} class="btn btn-primary">
            <i class="entypo-pencil"></i>Edit Record
        </a>

        @if ($user->isAdmin() || $user->isClientAdmin())
        <a class="btn btn-danger" data-toggle="modal" data-target="#deleteRecord-{{ $record->id }}">
            <i class="entypo-minus"></i>Delete Record
        </a>
        @endif
    </div>
</div>

@if ($user->isAdmin() || $user->isClientAdmin())
@include('includes/admin/modalDelete', array(
    'modalId'=> 'deleteRecord-'.$record->id,
    'modalFormRoute' => ['route' => ['records.destroy', $record->id], 'method' => 'delete'],
    'modalTitle' => $record->title
))
@endif

<div class="row">
    <div class="col-md-12">
        {{ $record->description }}
    </div>
</div>

<h2>Files</h2>

<div class="row">
    <div class="col-md-12">
        @if ($record->files->count())
        <div class="table-wrapper">
            <table class="table">
                <thead>
                    <tr>
                        <th>Filename</th>
                        <th width="80px">Size</th>
                        <th width="110px"><center>Downloads</center></th>
                        <th width="160px">Uploaded</th>
                        <th width="130px"></th>
                        @if ($user->isAdminOrClientAdmin())
                        <th width="110px"></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($record->files as $file)
                        <?php $downloads = $file->getDownloads(); // We process this further in tooltips ?>
                        <tr>
                            <td class="nowrap">{{ $file->filename }}</a></td>
                            <td>{{ $file->getHumanSize() }}</td>
                            <td>
                                <center>
                                    <span
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="@foreach ($downloads as $down) {{$down->ip_address}} - @if ($down->user_id) {{ $down->user->getName() }} @else Anonymous @endif - {{ Dates::localizeDateTime($down->created_at)->format('Y-m-d H:i') }}<br> @endforeach">
                                            {{ $downloads->count() }}
                                    </span>
                                </center>
                            </td>

                            <td>{{ Dates::localizeDateTime($file->created_at)->format('Y-m-d H:i') }}</td>
                            <td><a href="{{ URL::route('records.download', $file->id) }}" class="btn btn-primary" target="_blank">Download File</a></td>
                            @if ($user->isAdmin() || $user->isClientAdmin())
                            <td>

                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal-{{ $file->id }}">
                                  Delete File
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal-{{ $file->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Deleting {{ $file->filename }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                Do you really want to delete this file? You <b>CANNOT</b> undo this action.
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <a href="{{ URL::route('records.deleteFile', $file->id) }}"
                                                    class="btn btn-danger">Delete File</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
            <p>There are no files in this record.</p>
        @endif
    </div>
</div>

@include('Records/fileupload')

<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip({
    'html': true,
    'template': '<div class="tooltip bigger" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    });
});
</script>

@endsection
