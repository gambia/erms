@include('includes/admin/errors')

@if (isset($record) && !is_null($record->id))
    {{ Form::model($record, array('route' => array('records.update', $record->id), 'class'=>'form-horizontal form-groups-bordered', 'method' => 'put', 'role' => 'form')) }}
@else
    {{ Form::open(array('route' => array('records.store', $case->shortcut), 'class'=>'form-horizontal form-groups-bordered', 'role' => 'form')) }}
@endif

<div class="row">
    <div class="col-sm-12">
        {{ Form::label('title', 'Title:') }}<br>
        {{ Form::text('title', $record->title, array('placeholder' => 'Record title', 'class' => 'form-control input-lg')) }}
    </div>
</div>

<br />

<div class="row">
    <div class="col-sm-12">
        {{ Form::label('description', 'Description:') }}<br>
        {{ Form::textarea('description', $record->description, array('placeholder' => 'Description', 'class' => 'form-control wysihtml5', 'data-stylesheet-url' => 'assets/admin/css/wysihtml5-color.css')) }}
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-10">

    </div>
    <div class="col-sm-2 post-save-changes">
        <button type="submit" class="btn btn-success pull-right">
            @if ($record->id) Update Record @else Create Record @endif
        </button>
    </div>
</div>


{{ Form::close() }}

<br>
