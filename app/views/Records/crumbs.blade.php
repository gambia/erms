<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="{{ route('cases.index') }}">Cases</a>
    </li>
    <li>
        <a href="{{ route('cases.show', array('id' => $case->shortcut)) }}">{{ $case->getFullName() }}</a>
    </li>
    @if (isset($record))
    	<li>
        	Records
    	</li>
    	<li>
    		@if ($record->id)
        		<a href="{{ route('records.show', array('id' => $record->id)) }}">{{ $record->title }}</a>
        	@else
        		Create new record
        	@endif
    	</li>
    @endif
</ol>