@extends('layouts.client')

@section('content')

	@include('Records/crumbs')

	<div class="row">
	  <div class="col-md-9 col-sm-7">
	    <h2><a href="{{ URL::route('cases.index') }}">Cases</a> &raquo; Records &raquo; Edit record {{ $record->id }} in {{ $case->getFullName() }}</h2>
	  </div>
	</div>
	
	@include('Records/form')

@endsection
