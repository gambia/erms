@extends('layouts.client')

@section('content')
@include('Cases/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-8 col-sm-7 col-xs-6">
        <h1>
            {{$case->getFullName()}}
        </h1>
        @if ($case->is_confidential)
            <span class="label label-warning">Confidential</span>

            @foreach ($case->privilegedUsers as $user)
                <span class="label label-info">{{ $user->getName() }}</span>
            @endforeach
        @else
            @foreach ($case->project->securityGroups as $group)
                <span class="label label-info">{{ $group->name }}</span>
            @endforeach
        @endif
    </div>

    @if ($canEdit)
    <div class="col-md-4 col-sm-5 col-xs-6 actionbar">
        <a href="{{ route('cases.edit', array('id' => $case->shortcut)) }}" class="btn btn-primary"><i class="entypo-pencil"></i>Edit Case</a>

        @if ($canDelete)
        <a class="btn btn-danger" data-toggle="modal" data-target="#deleteCase-{{ $case->id }}">
            <i class="entypo-minus"></i>Delete Case
        </a>
        @endif
    </div>
    @endif
</div>

@if ($canDelete)
@include('includes/admin/modalDelete', array(
    'modalId'=> 'deleteCase-'.$case->id,
    'modalFormRoute' => ['route' => ['cases.destroy', $case->shortcut], 'method' => 'delete'],
    'modalTitle' => $case->summary
))
@endif

<div class="row">
    <div class="col-md-8">
        <h2>Summary</h2>
        <p>
            {{ $case->description }}
        </p>
    </div>

    <div class="col-md-3">
        <h2>Case Details</h2>
        <p>
            <strong>Priority:</strong> {{ $case->priority }}<br>
            <strong>Status:</strong> {{ $case->projectCaseStatus->name }}<br>
            <strong>Assigned To:</strong> {{ $case->assignedTo->name }}<br>
            <strong>Created:</strong> {{ Dates::localizeDateTime($case->created_at)->format('d M Y') }}<br>
            <strong>Last Update:</strong> {{ Dates::showTimeAgo($case->updated_at) }}<br><br>

            
        </p>


    </div>
</div>

<br />
<div class="row k1-heading-row">
    <div class="col-md-9 col-sm-9 col-xs-7">
        <h2>Records</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-5 actionbar">
        <a href="{{ URL::action('RecordsController@create', array('case_id' => $case->shortcut)) }}" class="btn btn-success"><i class="entypo-plus"></i>Add a New Record</a>
    </div>
</div>

@include('Cases/show/records')


<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="#attachments">Files</a></li>
            <li role="presentation"><a href="#comments">Comments</a></li>
            @if ($history->count())
                <li role="presentation"><a href="#history">History</a></li>
            @endif
        </ul>
    </div>
</div>


<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="attachments">
        @include('Cases/show/attachments')
    </div>

    <div role="tabpanel" class="tab-pane" id="comments">
            <!-- Button trigger modal -->
            <div class="row">
                <div class="col-md-12">
                    <a class="pull-right btn btn-success" data-toggle="modal" data-target="#myModal">
                      <i class="entypo-plus"></i>Add Comment
                    </a>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                {{ Form::open(array('route' => array('cases.postComment', $case->id), 'class'=>'comment-form', 'role' => 'form')) }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add Comment</h4>
                  </div>
                  <div class="modal-body">
                    
                    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::textarea('comment', null, array('placeholder' => 'Your comment', 'class' => 'form-control', 'rows' => 2)) }}
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                  </div>

                  <div class="modal-footer">
                    <button type="submit" class="btn btn-success pull-right">
                       Submit Comment
                    </button>
                  </div>
                </div>
                {{ Form::close() }}
              </div>
            </div>

        @include('Cases/show/comments', array('comments'=>$case->comments))

        <br /><br />
    </div>

    @if ($history->count())
        <div role="tabpanel" class="tab-pane" id="history">
            @include('includes/admin/history')
        </div>
    @endif
</div>

<script type="text/javascript">
    var bindClickListener = function (object) {
        object.click(function (e) {
            e.preventDefault()
            object.tab('show')
        });
    }

    $(function() {
        $('.nav.nav-tabs').find('a').each(function(){
            bindClickListener($(this));
        });

        $('.nav.nav-tabs li:first a').tab('show');
    });
</script>

@endsection
