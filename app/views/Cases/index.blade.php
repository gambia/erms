@extends('layouts.client')

@section('content')

@include('Cases/crumbs')

<div class="row k1-heading-row">
    <div class="col-md-9 col-sm-7 col-xs-7">
        <h1>
            @if ($project_case_status)
                Cases - {{ $project_case_status->name }}
            @else
                Cases - All
            @endif

            @if ($f_project)
                for {{ $project->name }} ({{ $project->project_key }})
            @endif
        </h1>
    </div>

    <div class="col-md-3 col-sm-5 col-xs-5">
        @if ($canCreate)
        <a href={{ URL::route('cases.create') }} class="btn btn-primary pull-right">
            <i class="entypo-plus"></i>
            Create a New Case
        </a>
        @endif
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-12">
        @include('Cases/list')
    </div>
</div>

@if ($count > $limit)
<div class="row">
    <div class="col-md-12">

        <?php
            $pages = ceil($count/$limit);

            $startPage = ($page < 5)? 1 : $page - 4;
            $endPage = 8 + $startPage;
            $endPage = ($pages < $endPage) ? $pages : $endPage;
            $diff = $startPage - $endPage + 8;
            $startPage -= ($startPage - $diff > 0) ? $diff : 0;
        ?>

        <div class="text-center">
            <ul class="pagination">
                @if ($from > 0 && $from >= $limit)
                    <li><a href="{{ URL::route('cases.index', array('from' => 0, 'project' => $f_project, 'filter' => isset($filter) ? $filter : null, 'sort' => $sortString)) }}">First</a></li>
                    <li><a href="{{ URL::route('cases.index', array('from' => $from - $limit, 'project' => $f_project, 'filter' => isset($filter) ? $filter : null, 'sort' => $sortString)) }}"><i class="entypo-left-open-mini">&nbsp;</i></a></li>
                @endif

                @for ($i=$startPage; $i<=$endPage; $i++)
                    <li @if ($page == $i-1) class="active" @endif><a href="{{ URL::route('cases.index', array('from' => ($i-1) * $limit, 'project' => $f_project, 'filter' => isset($filter) ? $filter : null, 'sort' => $sortString)) }}">{{ $i }}</a></li>
                @endfor

                @if (($from + $limit) <= $count)
                    <li><a href="{{ URL::route('cases.index', array('from' => $from + $limit, 'project' => $f_project, 'filter' => isset($filter) ? $filter : null, 'sort' => $sortString)) }}"><i class="entypo-right-open-mini">&nbsp;</i></a></li>
                    <li><a href="{{ URL::route('cases.index', array('from' => ($pages-1) * $limit, 'project' => $f_project, 'filter' => isset($filter) ? $filter : null, 'sort' => $sortString)) }}">Last</a></li>
                @endif

            </ul>
        </div>
    </div>
</div>
@endif

@endsection
