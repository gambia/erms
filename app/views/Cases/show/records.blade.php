@if ($records->count())
<div class="table-wrapper">
    <table class="table">
        <thead>
            <tr>
                <th>Title</th>
                <th width="160px">Created</th>
                <th width="160px">Updated</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($records as $record)
                <tr>
                    <td><a href="{{ URL::route('records.show', $record->id) }}">{{ $record->title }}</a></td>
                    <td>{{ Dates::localizeDateTime($record->created_at)->format('Y-m-d H:i') }}</td>
                    <td>{{ Dates::localizeDateTime($record->updated_at)->format('Y-m-d H:i') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@else
    <h3>There are no records for this case yet.</h3>
    <hr>
@endif
