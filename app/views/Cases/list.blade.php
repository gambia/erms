<div class="table-wrapper">
    <form action="{{ action('cases.index') }}" method="GET">
    <table class="table">
        <thead>
            <tr>
                <th width="100">
                    <?php $s = ($sortBy === 'id' ? 'id' . ($sortDirection == 'ASC' ? ',d' : ',a') : 'id,a'); ?>
                    <a href="{{ action('cases.index', array('sort' => $s, 'filter' => isset($filter) ? $filter : null)) }}">
                        ID
                        <?php if ($sortBy === 'id') { echo $sortDirection == 'DESC' ? '<i class="entypo-down-dir"></i>' : '<i class="entypo-up-dir"></i>'; } ?>
                    </a>
                </th>
                <th class="noMobile">
                    <?php $s = ($sortBy === 'summary' ? 'summary' . ($sortDirection == 'ASC' ? ',d' : ',a') : 'summary,a'); ?>

                    <a href="{{ action('cases.index', array('sort' => $s, 'filter' => isset($filter) ? $filter : null)) }}">
                        Name
                        <?php if ($sortBy === 'summary') { echo $sortDirection == 'DESC' ? '<i class="entypo-down-dir"></i>' : '<i class="entypo-up-dir"></i>'; } ?>
                    </a>
                </th>
                @if (!isset($excludes) || !in_array('assigned_to', $excludes))
                    <th class="noMobile" width="220">
                        <?php $s = ($sortBy === 'assigned_to' ? 'assigned_to' . ($sortDirection == 'ASC' ? ',d' : ',a') : 'assigned_to,a'); ?>
                        <a href="{{ action('cases.index', array('sort' => $s, 'filter' => isset($filter) ? $filter : null)) }}">
                            Assigned To
                            <?php if ($sortBy === 'assigned_to') { echo $sortDirection == 'DESC' ? '<i class="entypo-down-dir"></i>' : '<i class="entypo-up-dir"></i>'; } ?>
                        </a>
                    </th>
                @endif

                <th width="{{ isset($showFilter) ? '80px' : '60px' }}" class="noMobile">
                    <?php $s = ($sortBy === 'project_case_status_id' ? 'project_case_status_id' . ($sortDirection == 'ASC' ? ',d' : ',a') : 'project_case_status_id,a'); ?>
                    <a href="{{ action('cases.index', array('sort' => $s, 'filter' => isset($filter) ? $filter : null)) }}">
                        Status
                        <?php if ($sortBy === 'project_case_status_id') { echo $sortDirection == 'DESC' ? '<i class="entypo-down-dir"></i>' : '<i class="entypo-up-dir"></i>'; } ?>
                    </a>
                </th>
                @if (!isset($excludes) || !in_array('priority', $excludes))
                    <th class="noMobile" width="90">
                        <center>
                        <?php $s = ($sortBy === 'priority' ? 'priority' . ($sortDirection == 'ASC' ? ',d' : ',a') : 'priority,a'); ?>
                        <a href="{{ action('cases.index', array('sort' => $s, 'filter' => isset($filter) ? $filter : null)) }}">
                            Priority
                            <?php if ($sortBy === 'priority') { echo $sortDirection == 'DESC' ? '<i class="entypo-down-dir"></i>' : '<i class="entypo-up-dir"></i>'; } ?>
                        </a>
                        </center>
                    </th>
                @endif
                @if (!isset($excludes) || !in_array('created_at', $excludes))
                    <th class="noMobile" width="120">
                        <?php $s = ($sortBy === 'created_at' ? 'created_at' . ($sortDirection == 'ASC' ? ',d' : ',a') : 'created_at,a'); ?>
                        <a href="{{ action('cases.index', array('sort' => $s, 'filter' => isset($filter) ? $filter : null)) }}">
                            Created
                            <?php if ($sortBy === 'created_at') { echo $sortDirection == 'DESC' ? '<i class="entypo-down-dir"></i>' : '<i class="entypo-up-dir"></i>'; } ?>
                        </a>
                    </th>
                @endif
                <th width="115px">
                    <?php $s = ($sortBy === 'updated_at' ? 'updated_at' . ($sortDirection == 'ASC' ? ',d' : ',a') : 'updated_at,a'); ?>
                    <a href="{{ action('cases.index', array('sort' => $s, 'filter' => isset($filter) ? $filter : null)) }}">
                        Updated
                        <?php if ($sortBy === 'updated_at') { echo $sortDirection == 'DESC' ? '<i class="entypo-down-dir"></i>' : '<i class="entypo-up-dir"></i>'; } ?>
                    </a>
                </th>
            </tr>
            @if (isset($showFilter) && $showFilter)
            <tr class="cases-filter">
                <th><input type="text" name="filter[id]" placeholder="AAA-123" @if (isset($filter['id']) && !empty($filter['id'])) value="{{$filter['id']}}" @endif></th>
                <th><input type="text" name="filter[summary]" placeholder="My case" @if (isset($filter['summary']) && !empty($filter['summary'])) value="{{$filter['summary']}}" @endif></th>
                <th><select name="filter[assigned_to]">
                    <option value="">All</option>
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}" @if (isset($filter['assigned_to']) && !empty($filter['assigned_to']) && $filter['assigned_to'] == $user->id) selected="selected" @endif>{{ $user->getName() }}</option>
                    @endforeach
                </select></th>
                <th><select name="filter[project_case_status_id]">
                    <option value="">All</option>
                        @foreach ($statuses as $key => $status)
                        <option value="{{ $status->id }}" @if (isset($filter['project_case_status_id']) && !empty($filter['project_case_status_id']) && $filter['project_case_status_id'] == $status->id) selected="selected" @endif>{{ $status->name }}</option>
                    @endforeach
                </select></th>
                <th><select name="filter[priority]">
                    <option value="">All</option>
                    @for ($i=1;$i<=5; $i++)
                        <option value="{{$i}}" @if (isset($filter['priority']) && !empty($filter['priority']) && $filter['priority'] == $i) selected="selected" @endif>{{$i}}</option>
                    @endfor
                </select></th>
                <th><?php /* <select name="filter[created_at]">
                    <option value="">Anytime</option>
                    @foreach ($timeOptions as $k => $v)
                        <option value="{{ $k }}" @if (isset($filter['created_at']) && !empty($filter['created_at']) && $filter['created_at'] == $k) selected="selected" @endif>{{ $v }}</option>
                    @endforeach
                </select> */?></th>
                <th><?php /* select name="filter[updated_at]">
                    <option value="">Anytime</option>
                    @foreach ($timeOptions as $k => $v)
                    <option value="{{ $k }}" @if (isset($filter['updated_at']) && !empty($filter['updated_at']) && $filter['updated_at'] == $k) selected="selected" @endif>{{ $v }}</option>
                    @endforeach
                </select> */ ?>
                <input type="hidden" name="sort" value="{{ isset($sortString) ? $sortString : null }}">
                <input type="submit" class="hidden-submit"/></th>
            </tr>
            @endif
        </thead>
        <tbody>
            @foreach ($cases as $case)
                <tr>
                    <td class="nowrap">
                        <a href="{{ URL::route('cases.show', $case->shortcut) }}">
                            {{$case->shortcut }}
                        </a>
                    </td>

                    <td class="noMobile nowrap">
                        @if ($case->is_confidential)
                            <span class="label label-warning">Confidential</span> 
                        @endif

                        <a href="{{ URL::route('cases.show', $case->shortcut) }}">
                            {{ $case->summary }}
                        </a>
                    </td>

                    @if (!isset($excludes) || !in_array('assigned_to', $excludes))
                        <td class="nowrap noMobile">{{ $case->assignedTo->name }}</td>
                    @endif

                    <td class="noMobile">{{ $case->projectCaseStatus->name }}</td>

                    @if (!isset($excludes) || !in_array('priority', $excludes))
                        <td class="noMobile"><center><span class="{{$case->getPriorityClassName()}}">{{ $case->priority }}</span></center></td>
                    @endif

                    @if (!isset($excludes) || !in_array('created_at', $excludes))
                        <td class="nowrap noMobile">{{ date("d M Y",strtotime($case->created_at)) }} </td>
                    @endif

                    <td class="nowrap">{{ Dates::showTimeAgo($case->updated_at) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var form = $('form');
        form.find('select').on('change', function(e) {
            form.submit();
        });
    });
</script>
