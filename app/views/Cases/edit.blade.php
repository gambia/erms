@extends('layouts.client')

@section('content')
@include('Cases/crumbs')
<h2>{{$case->getFullName()}}</h2>
<br />

@include('Cases/form')

@endsection
