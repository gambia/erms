<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="{{ route('cases.index') }}">Cases</a>
    </li>
    @if (isset($case))
    @if (is_null($case->id))

        <li>
            <a href="{{ route('cases.create') }}">Create new case</a>
        </li>

    @else

        <li>
            <a href="{{ route('cases.show', array('id' => $case->shortcut)) }}">{{$case->getFullName()}}</a>
        </li>
        @endif

    @endif
</ol>