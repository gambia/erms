@include('includes/admin/errors')

@if (isset($case) && !is_null($case->id))
    {{ Form::model($case, array('route' => array('cases.update', $case->shortcut), 'class'=>'form-horizontal form-groups-bordered', 'method' => 'put', 'role' => 'form')) }}
@else
    {{ Form::model($case, array('route' => 'cases.store', 'class'=>'form-horizontal form-groups-bordered')) }}
@endif

{{ Form::model($case, array('route' => 'cases.store', 'class'=>'form-horizontal form-groups-bordered')) }}

    <div class="row">
        <div class="col-sm-6">
            {{ Form::label('Name:','', array('class'=>'control-label')) }}<br />
            {{ Form::text('summary', $case->summary, array('class'=>'form-control input-lg')) }}
        </div>

        <div class="col-sm-6">
          {{ Form::label('project_id','Project:', array('class'=>'control-label')) }}<br />
          <?php
            $projectsTree = array();
            foreach ($projects as $project)
            {
              if ($project->hasEnabledCases()) {
                $projectsTree[$project->id] = str_repeat('--', $project->depth-1) . ' ' . $project->getFullName();
              }
            }
          ?>

          {{
              Form::select(
                  'project_id',
                  $projectsTree,
                  $case->project_id,
                  array(
                      'class'=>'form-control input-lg',
                      'name' => 'project_id'
                  )
              )
          }}
        </div>

    </div>

    <br />

    <div class="row">
        <div class="col-sm-6">
          {{ Form::label('Assigned To:','', array('class'=>'control-label')) }}

          {{
              Form::select(
                  'assigned_to',
                  $users,
                  (is_null($case->id) ? '' : $case->assigned_to),
                  array(
                      'class'=>'form-control input-lg',
                      'name' => 'assigned_to'
                  )
              )
          }}
        </div>

        <div class="col-sm-3">
            {{ Form::label('Priority:','', array('class'=>'control-label')) }}

            {{
              Form::select(
                  'priority',
                  $priorities,
                  $case->priority,
                  array(
                      'class'=>'form-control input-lg',
                      'name' => 'priority'
                  )
              )
          }}
        </div>

        <div class="col-sm-3">
            {{ Form::label('project_case_status_id', 'Case Status:', array('class'=>'control-label')) }}

            {{
              Form::select(
                  'project_case_status_id',
                  $statuses,
                  $case->project_case_status_id,
                  array(
                      'class'=>'form-control input-lg',
                      'name' => 'project_case_status_id'
                  )
              )
          }}
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-sm-12">
            <div class="checkbox checkbox-primary">
                {{ Form::checkbox('is_confidential', 1, $case->is_confidential, array('id' => 'is_confidential')) }}
                   <label for="is_confidential"><strong>Mark this case as confidential and restrict access</strong></label>
            </div>
        </div>
    </div>
    <br>

    <div class="privilegedUsersRow">
        <div class="row ">
            <div class="col-sm-12">
                {{ Form::label('privileged_users', 'Authorized Staff:') }}<br>
                <select multiple="multiple" name="privileged_users[]" id="privileged_users" class="form-control input-lg">
                    @foreach ($users as $id=>$name)
                        <option value="{{ $id }}" @if (in_array($id, $privileged_users)) selected="selected" @endif>
                            {{ $name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <br />
    </div>

    <!-- WYSIWYG - Content Editor -->
    <div class="row">
        <div class="col-sm-12">
            {{ Form::label('Description:','', array('class'=>'control-label')) }}
            {{ Form::textarea('description', $case->description, ['class' => 'form-control wysihtml5', 'data-stylesheet-url' => 'assets/css/wysihtml5-color.css']) }}
        </div>
    </div>

    <br />

    <div class="row">
        <div class="col-sm-10">

        </div>
        <div class="col-sm-2 post-save-changes">
          @if (isset($case) && !is_null($case->id))
            {{ Form::submit('Update Case', array('class'=>'btn btn-success pull-right')) }}
          @else
            {{ Form::submit('Create Case', array('class'=>'btn btn-success pull-right')) }}
          @endif
        </div>
    </div>

{{ Form::close() }}

<script type="text/javascript">
    $(document).ready(function() {

        var $privilegedUsersRow = $('.privilegedUsersRow');

        $("#privileged_users").select2();

        if($('#is_confidential:checked').length > 0) {
            $privilegedUsersRow.show();
        } else {
            $privilegedUsersRow.hide();
        }

        $('#is_confidential').click(function() {
            var $this = $(this);
            console.log('click');
            // $this will contain a reference to the checkbox   
            if ($this.is(':checked')) {
                // the checkbox was checked
                $privilegedUsersRow.show();
            } else {
                // the checkbox was unchecked
                $privilegedUsersRow.hide();
            }
        });
    });
</script>