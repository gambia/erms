@extends('layouts.client')

@section('content')
@include('Cases/crumbs')

<h2>Create a New Case</h2>
<br />

@include('Cases/form')

@endsection
