<?php

use Policies\ProjectPolicy,
    Policies\ProjectCasePolicy;

class HomeController extends BaseController {
    public function __construct(){
        $this->beforeFilter('auth');
    }

    public function dashboard(){
        $user = Auth::User();

        $cases = ProjectCasePolicy::authorizedRecordsBuilder($user)->orderBy('updated_at', 'DESC')->take(25)->get();

        if ($cases->count()) {
            $comments = ProjectCaseComment::whereIn(
                'project_case_id', $cases->lists('id')
            )
                ->with('user', 'projectCase', 'projectCase.project')
                ->orderBy('updated_at', 'DESC')
                ->take(5)
                ->get();
        } else {
            $comments = new \Illuminate\Database\Eloquent\Collection();
        }

        $projects = $user->profileProjects()->filter(function($item) use ($user) {
            return ($item->hasManager($user) || $item->isMainManager($user));
        });

        return View::make('Home.dashboard', array(
            'cases' => $cases,
            'projects' => $projects,
            'sortBy' => null,
            'sortColumn' => null,
            'sortDirection' => null,
            'user' => $user,
            'timeOffset' => $user->getTimeOffset(),
            'comments' => $comments,
            'latestActivities' => $user->latestActivitiesExcept(
                5,
                [
                    History::PAGE_FILE_UPLOADED,
                    History::PAGE_FILE_DOWNLOADED,
                    History::RECORD_FILE_UPLOADED,
                    History::RECORD_FILE_DOWNLOADED,
                    History::USER_PASSWORD_CHANGED,
                    History::USER_BLOCKED,
                    History::USER_UNBLOCKED,
                    History::USER_UPDATED,
                    History::USER_LOGIN_SUCCESS,
                    History::USER_LOGIN_FAILED,
                    History::USER_LOGOUT
                ]
            )
        ));
    }
}
