<?php

class ActivityController extends \BaseController {

    public function index()
    {
    	$user = Auth::user();

    	if ($user->isAdmin()) {
    		if ($f_client = Input::get('client', NULL)) {
    			$history = History::where('client_id', '=', $f_client);
    		} else {
    			$history = History::where('client_id', '=', $user->client_id);
    		}
    		$clients = Client::all();
    	} else {
    		$history = History::where('client_id', '=', $user->client_id);
    	}

    	if ($f_action = Input::get('action', NULL)) {
    		$history->where('action', '=', $f_action);
    	}

    	if ($f_user = Input::get('user', NULL)) {
    		$history->where('user_id', '=', $f_user);
    	}

    	$countQuery = clone($history);
        $historyCount = $countQuery->count();

        $from = Input::get('from', 0);
        $limit = Input::get('limit', 30);

    	$history = $history
    		->with('User')
    		->take($limit)
    		->skip($from)
    		->orderBy('created_at', 'DESC')
    		->get();

    	$users = User::where('client_id', '=', isset($f_client) ? $f_client : Auth::user()->client_id)->get();

        return View::make('Activity.index', array(
        	'history' => $history,
        	'count' => $historyCount,
        	'from' => $from,
            'limit' => $limit,
            'page' => round($from/$limit),
            'users' => $users,
            'clients' => isset($clients) ? $clients : NULL,

            'f_action' => isset($f_action) ? $f_action : NULL,
            'f_user' => isset($f_user) ? $f_user : NULL,
            'f_client' => isset($f_client) ? $f_client : $user->client_id
        ));
    }

}
