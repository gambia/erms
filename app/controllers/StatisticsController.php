<?php

use Policies\StatisticsPolicy as AuthPolicy;

class StatisticsController extends \BaseController {

	public function index()
	{
        $user = Auth::User();
        $clientId = $user->client_id;

        AuthPolicy::authorize($user, 'index');

        $timeArray = array();
        for ($i = 0; $i<31; $i++) {
        	$timeArray[Carbon\Carbon::now()->subDays($i)->format('Y-m-d')] = 0;
        }
        $timeArray = array_reverse($timeArray);

        // --------------------------------------------------------------------------
        // User Logins
        // --------------------------------------------------------------------------

        $userLoginsResult = DB::table('history')
        	->select(DB::raw('COUNT(id) AS "count", DATE(created_at) as "date"'))
        	->where('action', '=', History::USER_LOGIN_SUCCESS)
        	->where('client_id', '=', $clientId)
        	->whereRaw('created_at BETWEEN \'' . Carbon\Carbon::now()->subDays(30) . '\' AND NOW()')
        	->groupBy(DB::raw('DATE(created_at)'))
        	->get();

        $userLogins = $timeArray;
        foreach ($userLoginsResult as $r) {
        	$userLogins[$r->date] = $r->count;
        }

        // --------------------------------------------------------------------------
        // Cases created / closed
        // --------------------------------------------------------------------------

        $casesCreatedResult = DB::table('history')
        	->select('created_at')
        	->where('action', '=', History::CASE_CREATED)
        	->where('client_id', '=', $clientId)
        	->whereRaw('created_at BETWEEN \'' . Carbon\Carbon::now()->subMonths(5) . '\' AND NOW()')
        	->get();

        $casesClosedResult = DB::table('history')
        	->select('created_at')
        	->where('action', '=', History::CASE_CLOSED)
        	->where('client_id', '=', $clientId)
        	->whereRaw('created_at BETWEEN \'' . Carbon\Carbon::now()->subMonths(5) . '\' AND NOW()')
        	->get();

        $cases = array();
        for ($i=0; $i<6; $i++) {
        	$cases[Carbon\Carbon::now()->subMonths($i)->format('Y-m')] = array('created' => 0, 'closed' => 0);
        }
        $cases = array_reverse($cases);

        foreach ($casesCreatedResult as $case) {
        	$dt = new \DateTime($case->created_at);
        	$cases[$dt->format('Y-m')]['created']++;
        }
        foreach ($casesClosedResult as $case) {
        	$dt = new \DateTime($case->created_at);
        	$cases[$dt->format('Y-m')]['closed']++;
        }

        // --------------------------------------------------------------------------
        // Records created
        // --------------------------------------------------------------------------

        $recordsCreatedResult = DB::table('history')
        	->select('created_at')
        	->where('action', '=', History::RECORD_CREATED)
        	->where('client_id', '=', $clientId)
        	->whereRaw('created_at BETWEEN \'' . Carbon\Carbon::now()->subMonths(5) . '\' AND NOW()')
        	->get();

        $records = array();
        for ($i=0; $i<6; $i++) {
        	$records[Carbon\Carbon::now()->subMonths($i)->format('Y-m')] = array('created' => 0, 'closed' => 0);
        }
        $records = array_reverse($records);

        foreach ($recordsCreatedResult as $record) {
            $dt = new \DateTime($record->created_at);
            $records[$dt->format('Y-m')]['created']++;
        }

        // --------------------------------------------------------------------------
        // Comments added
        // --------------------------------------------------------------------------

        $commentsResult = DB::table('history')
        	->select('created_at')
        	->where('action', '=', History::CASE_COMMENTED)
        	->orWhere('action', '=', History::PAGE_COMMENTED)
        	->where('client_id', '=', $clientId)
        	->whereRaw('created_at BETWEEN \'' . Carbon\Carbon::now()->subMonths(5) . '\' AND NOW()')
        	->get();

        $comments = array();
        for ($i=0; $i<11; $i++) {
        	$comments[Carbon\Carbon::now()->subMonths($i)->format('Y-m')] = 0;
        }
        $comments = array_reverse($comments);

        foreach ($commentsResult as $comment) {
            $dt = new \DateTime($comment->created_at);
            $key = $dt->format('Y-m');

            if(array_key_exists($key, $comments)) {
                $comments[$key]++;
            }
        }

		return View::make('Statistics.index', array(
			'userLogins' => $userLogins,
			'cases' => $cases,
			'records' => $records,
			'comments' => $comments
		));
	}

}
