<?php

class ClientHomeController extends \BaseController {

    public function index($clientKey, $type = null, $pageNumber = 1)
    {
        $client = Client::where('client_key','=', strtoupper($clientKey))->firstOrFail();

        if ($type === 'pages' && $pageNumber === null) {
            return Redirect::route('clientHome', array('clientKey' => $client->client_key));
        }

        $type = ($type === null) ? 'pages' : $type;
        if ($type !== 'pages' && $type !== 'forms') {
            App::abort(404);
        }

        /* for ($i = 0; $i<50; $i++) {
            Page::create(array(
                'title' => 'Random Page ' . $i,
                'project_id' => rand(2,10),
                'type' => (rand(1,100) % 2 === 0) ? Page::TYPE_INTRANET : Page::TYPE_FORM,
                'is_public' => 1,
                'description' => 'Random page description ' . $i,
                'client_id' => $client->id,
            ));
        } */

        $limit = 8;
        $skip = ($pageNumber-1) * $limit;

        $pages = Page::where('client_id', '=', $client->id)
            ->where('type', '=', Page::TYPE_INTRANET)
            ->orderBy('created_at', 'DESC');

        $pagesCount = clone($pages);

        $forms = Page::where('client_id', '=', $client->id)
            ->where('type', '=', Page::TYPE_FORM)
            ->where('is_public', '=', true)
            ->orderBy('created_at', 'DESC');

        $formsCount = clone($forms);


        $items = ($type === 'pages') ? $pages : $forms;
        $items = $items
            ->skip($skip)
            ->take($limit);

        return View::make('FrontEnd/ClientHome.index', array(
            'client' => $client,
            'items' => $items->with('files')->get(),
            'pageNumber' => $pageNumber,
            'limit' => $limit,
            'itemsCount' => $type === 'pages' ? $pagesCount->count() : $formsCount->count(),
            'pagesCount' => $pagesCount->count(),
            'formsCount' => $formsCount->count(),
            'crumb' => $type,
            'type' => $type
        ));
    }

    public function contact($clientKey)
    {
        $client = Client::where('client_key','=', strtoupper($clientKey))->first();

        $pages = Page::where('client_id', '=', $client->id)
            ->where('type', '=', Page::TYPE_INTRANET)
            ->orderBy('created_at', 'DESC');

        $pagesCount = clone($pages);

        $forms = Page::where('client_id', '=', $client->id)
            ->where('type', '=', Page::TYPE_FORM)
            ->where('is_public', '=', true)
            ->orderBy('created_at', 'DESC');

        $formsCount = clone($forms);

        return View::make('FrontEnd/ClientContact.index', array(
            'client' => $client,
            'crumb' => 'contact',
            'type' => 'pages',
            'pagesCount' => $pagesCount->count(),
            'formsCount' => $formsCount->count(),
        ));
    }

    public function contactSubmit($clientKey)
    {
        $client = Client::where('client_key','=', strtoupper($clientKey))->first();

        $data = Input::all();
        $rules = [
            'name' => 'required',
            'message' => 'required',
            'email' => 'required|email',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        MandrillEmailSender::send(
            'contact-form-question',
            [
                'contactform@k1cms.com' => [
                    'clientName' => $client->name,
                    'clientLogo' => $client->logo_url,
                    'qName' => $data['name'],
                    'qEmail' => $data['email'],
                    'qMessage' => $data['message']
                ]
            ]
        );

        return Redirect::back()->with('submitSuccess', true);
    }

    public function article($clientKey, $slug)
    {
        $client = Client::where('client_key','=', strtoupper($clientKey))->first();

        $article = Page::where('slug', '=' ,$slug)
            ->with('files')
            ->where('is_public', '=', 1)
            ->whereIn('type', [Page::TYPE_INTRANET, Page::TYPE_FORM])
            ->first();

        if (!$article) {
            App::abort(404);
        }

        $pages = Page::where('client_id', '=', $client->id)
            ->where('type', '=', Page::TYPE_INTRANET)
            ->orderBy('created_at', 'DESC');

        $forms = Page::where('client_id', '=', $client->id)
            ->where('type', '=', Page::TYPE_FORM)
            ->where('is_public', '=', true)
            ->orderBy('created_at', 'DESC');

        return View::make('FrontEnd/ClientHome.index', array(
            'client' => $client,
            'pagesCount' => $pages->count(),
            'formsCount' => $forms->count(),
            'crumb' => 'article',
            'type' => $article->type === Page::TYPE_INTRANET ? 'pages' : 'forms',
            'article' => $article
        ));
    }

    public function search($clientKey)
    {
        $client = Client::where('client_key','=', strtoupper($clientKey))->first();

        $query = Input::get('q', NULL);

        $page = Input::get('page', 1);
        $limit = 8;
        $skip = ($page-1) * $limit;

        $pages = Page::where('client_id', '=', $client->id)
            ->where('type', '=', Page::TYPE_INTRANET)
            ->orderBy('created_at', 'DESC');

        $forms = Page::where('client_id', '=', $client->id)
            ->where('type', '=', Page::TYPE_FORM)
            ->where('is_public', '=', true)
            ->orderBy('created_at', 'DESC');

        $engine = 'LikeFullTextSearchEngine';
        $searchFunction = $engine . '::search';

        $publicPages = Page::where('is_public', '=', 1)
            ->where('client_id', '=', $client->id)
            ->whereIn('type', [Page::TYPE_INTRANET, Page::TYPE_FORM])
            ->with('files');

        $items = call_user_func(
            $searchFunction,
            $publicPages,
            $query,
            ['title', 'description']
        );

        $itemsCount = $items->count();

        $items = $items
            ->skip($skip)
            ->take($limit)
            ->get();

        return View::make('FrontEnd/ClientHome.index', array(
            'client' => $client,
            'pagesCount' => $pages->count(),
            'formsCount' => $forms->count(),
            'items' => isset($items) ? $items : null,
            'itemsCount' => isset($itemsCount) ? $itemsCount : 0,
            'pageNumber' => $page,
            'limit' => $limit,
            'search' => true,
            'query' => $query,
            'results' => $items->count(),
            'crumb' => 'search',
            'type' => null
        ));

    }

}
