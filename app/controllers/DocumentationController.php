<?php

use \Policies\DocumentationPolicy as DocumentationPolicy;

class DocumentationController extends \BaseController {

	// --------------------------------------------------------------------------
	// CRUD
	// --------------------------------------------------------------------------

	public function index()
	{
		$documentation = DocumentationPolicy::authorizedRecordsBuilder(Auth::user())->get();

		return View::make('Documentation.index', array(
            'documentation' => $documentation,
            'canCreate' => DocumentationPolicy::isAuthorizedForCreate(Auth::user(), null, null)
        ));
	}

	public function create()
	{
		$documentationPage = new Documentation();

		DocumentationPolicy::isAuthorizedForCreate(Auth::user(), $documentationPage, null);

		return View::make('Documentation.create', array(
			'page' => $documentationPage,
			'rootPage' => Documentation::where('title', '=', '__ROOT__PAGE__')->firstOrFail(),
			'pages' => DocumentationPolicy::authorizedRecordsBuilder(Auth::user())->get()
		));
	}

	public function store()
	{
		$documentationPage = new Documentation();
		$data = Input::all();
		$rules = Documentation::$validationRules;

		DocumentationPolicy::isAuthorizedForCreate(Auth::user(), $documentationPage, $data);

		$validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $data['is_published'] = isset($data['is_published']) ? true : false;

        $documentationPage->fill($data);
        $documentationPage->save();

        $parent = Documentation::find($data['parent_id']);
        $documentationPage->makeFirstChildOf($parent);

        return Redirect::route(
            'documentation.show', array('id' => $documentationPage->id)
        );
	}

	public function show($id)
	{
        $user = Auth::User();
		$documentationPage = Documentation::findOrFail($id);

        DocumentationPolicy::authorize($user, 'show', $documentationPage);

        $uploadBasePath = 'clients/' . $user->client_id . '/documentation/' . $documentationPage->id;

        $params = array_merge(
            S3StorageEngine::getDirectUploadParams($uploadBasePath),
            [
                'page' => $documentationPage,
                'documentation' => DocumentationPolicy::authorizedRecordsBuilder(Auth::user())->get(),
                'canEdit' => DocumentationPolicy::isAuthorizedForEdit(Auth::user(), $documentationPage, null)
            ]
        );

		return View::make('Documentation.show', $params);
	}

	public function edit($id)
	{
		$documentationPage = Documentation::findOrFail($id);

		DocumentationPolicy::isAuthorizedForEdit(Auth::user(), $documentationPage, null);

		return View::make('Documentation.edit', array(
			'page' => $documentationPage,
			'rootPage' => Documentation::where('title', '=', '__ROOT__PAGE__')->firstOrFail(),
			'pages' => DocumentationPolicy::authorizedRecordsBuilder(Auth::user())->get()
		));
	}

	public function update($id)
	{
		$documentationPage = Documentation::findOrFail($id);
		$data = Input::all();
		$data['is_published'] = isset($data['is_published']) ? true : false;
		$rules = Documentation::$validationRules;

		DocumentationPolicy::isAuthorizedForUpdate(Auth::user(), $documentationPage, $data);

		$rules = Page::$validationRules;
        $validator = Validator::make($data, $rules);

        $documentationPage->fill($data);
        $documentationPage->save();

        // Update parent or make it root
        $parent = $documentationPage->parent_id;
        if ($data['parent_id'] != $parent && $data['parent_id'] != $documentationPage->id) {
            $parent = Documentation::find($data['parent_id']);
            $documentationPage->makeFirstChildOf($parent);
        }

        return Redirect::route('documentation.show', array('id' => $documentationPage->id));
	}

	public function destroy($id)
	{
		$user = Auth::user();
        $documentationPage = Documentation::findOrFail($id);

        DocumentationPolicy::authorize($user, 'update', $documentationPage);

        $documentationPage->destroy($id);
        return Redirect::route('documentation.index');
	}

	// --------------------------------------------------------------------------
	// Sorting
	// --------------------------------------------------------------------------

	public function move($id, $direction)
	{
		$user = Auth::user();
        $documentationPage = Documentation::findOrFail($id);

        DocumentationPolicy::authorize($user, 'update', $documentationPage);

        try {
			if ($direction === 'up') {
	        	$documentationPage->moveLeft();
	        } else {
	        	$documentationPage->moveRight();
	        }
        } catch (Exception $e) {
        	// We don't care
        }

        if (Request::ajax())
        {
        	return 'OK';
        }
        return Redirect::back();
	}

	// --------------------------------------------------------------------------
	//	File uploads / downloads
	// --------------------------------------------------------------------------

	public function attachFile($id)
	{
		$user = Auth::user();
		$page = Documentation::findOrFail($id);

		DocumentationPolicy::authorize($user, 'update', $page);

        DB::transaction(function() use ($page, $user)
        {
            $data = Input::all();

            $docFile = new DocumentationFile();
            $docFile->user_id = $user->id;
            $docFile->documentation_id = $page->id;

            $docFile->filename = $data['name'];
            $docFile->mimeType = $data['mime'];
            $docFile->size = $data['size'];
            $docFile->url = $data['path_to_file'];

            $docFile->save();
        });
	}

	public function chunkUploadFinished($id)
	{
		$user = Auth::user();
		$documentationPage = Documentation::findOrFail($id);

		DocumentationPolicy::authorize($user, 'update', $documentationPage);
		$count = Input::get('count', 1);
	}

	public function downloadFile($id)
	{
		$file = DocumentationFile::findOrFail($id);
		return Redirect::to($file->getQualifiedFilePath($file->filename));
	}

	public function deleteFile($id)
	{
		$user = Auth::user();
		$file = DocumentationFile::findOrFail($id);
		DocumentationPolicy::authorize($user, 'update', $file->documentation->id);
		$file->destroy($id);

		return Redirect::back();
	}

}
