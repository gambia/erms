<?php

use Policies\ProjectPolicy as ProjectPolicy;
use Policies\ProjectCasePolicy as ProjectCasePolicy;
use Policies\RecordPolicy as RecordPolicy;
use Policies\PagePolicy as PagePolicy;
use Policies\UserPolicy as UserPolicy;

class SearchController extends \BaseController {

    public function index()
    {

        $user = Auth::User();
    	$query = Input::get('q', NULL);

        $authorizedProjects = ProjectPolicy::authorizedRecordsBuilder($user);
        $authorizedCases = ProjectCasePolicy::authorizedRecordsBuilder($user);
        $authorizedRecords = RecordPolicy::authorizedRecordsBuilder($user);
        $authorizedPages = PagePolicy::authorizedRecordsBuilder($user);
        $authorizedUsers = UserPolicy::authorizedRecordsBuilder($user);

        if(!$query){
            return View::make('Search.index', array(
                'query' => $query,
                'results' => 0
            ));
        }

        $engine = 'LikeFullTextSearchEngine';

        // Leaving here for reference
    	//if (DB::getDriverName() === 'pgsql') {
        //    $engine = 'PostgresFullTextSearchEngine';
        //}

        $searchFunction = $engine . '::search';

        $stopwatch = new \Symfony\Component\Stopwatch\Stopwatch();
        $stopwatch->start('search');

        // Go get the data

        $projects = call_user_func(
            $searchFunction,
            $authorizedProjects,
            $query,
            ['name', 'project_key', 'summary']
        )->get();

        $cases = call_user_func(
            $searchFunction,
            $authorizedCases,
            $query,
            ['summary', 'description', 'shortcut']
        )->get();

        $records = call_user_func(
            $searchFunction,
            $authorizedRecords,
            $query,
            ['title', 'description']
        )->get();

        $pages = call_user_func(
            $searchFunction,
            $authorizedPages,
            $query,
            ['title', 'description']
        )->get();

        $users = call_user_func(
            $searchFunction,
            $authorizedUsers,
            $query,
            ['firstname', 'lastname', 'job_title', 'email']
        )->get();

        Log::info('Search duration: ' . $stopwatch->stop('search')->getDuration() . 'ms (user: ' . $user->email . ', query: ' . $query . ', engine: ' . $engine . ')');

        return View::make('Search.index', array(
            'projects' => $projects,
            'cases' => $cases,
            'pages' => $pages,
            'records' => $records,
            'users' => $users,
            'query' => $query,
            'results' => $projects->count() + $cases->count() + $pages->count() + $users->count() + $records->count()
        ));
    }

}
