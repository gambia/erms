<?php

use Policies\PagePolicy;
use Policies\ProjectPolicy;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class PagesController extends \BaseController {
    public function __construct()
    {
        $this->beforeFilter('csrf', array(
            'only' => array('store', 'update', 'postComment'))
        );
    }

    protected $pageType;

    protected function setPageType($type)
    {
        $this->validateUserFeatures($type);

        $validTypes = array_keys(Page::$pageTypes);

        if (!in_array($type, $validTypes)) {
            App::abort(404);
        }

        $this->pageType = $type;

        View::share('pageType', $this->pageType);
        PagePolicy::setPageType($this->pageType);
    }

    protected function validateUserFeatures($type){
        $user = Auth::User();

        $m = 'enable_' . $type;

        if (!$user->client->$m)
        {
            Redirect::to('/')->send();
        }
    }

    public function index($type)
    {
        $user = Auth::User();

        $this->setPageType($type);

        $pages = PagePolicy::authorizedRecords(Auth::user());

        return View::make('Pages.index', array(
            'pages' => $pages,
            'canCreate' => PagePolicy::isAuthorizedForCreate(
                $user, NULL, NULL
            )
        ));
    }

    public function show($type, $id)
    {
        $this->setPageType($type);
        $user = Auth::user();

        $page = Page::where('id', '=', $id)
            ->where('type', '=', Page::$pageTypes[$this->pageType])
            ->with('comments', 'comments.user')
            ->firstOrFail();

        PagePolicy::authorize($user, 'show', $page);

        $uploadBasePath = 'clients/' . $user->client_id . '/pages/' . $page->type . '/' . $page->id;

        $params = array_merge(
            S3StorageEngine::getDirectUploadParams($uploadBasePath),
            [
                'page' => $page,
                'canEdit' => PagePolicy::isAuthorizedForEdit($user, $page, NULL),
                'history' => $page->getHistory()
            ]
        );

        return View::make('Pages.show', $params);
    }

    public function create($type)
    {
       $this->setPageType($type);
       $user = Auth::user();

        PagePolicy::authorize($user, 'create');

        $page = new Page();

        $enableParam = PagePolicy::$projectFlags[$this->pageType];

        $projects = Policies\ProjectPolicy::authorizedRecordsBuilder(
            Auth::user()
        )
            ->where($enableParam, '=', true)
            ->orderBy('lft', 'ASC')
            ->where('depth', '>', 0)
            ->get();

        $pages = PagePolicy::authorizedRecords(Auth::user());

        return View::make('Pages.create', array(
            'page' => $page,
            'projects' => $projects,
            'pages' => $pages,
            'rootPage' => Page::where('title', '=', '__ROOT__PAGE__')
                ->where('client_id', '=', $user->client_id)
                ->firstOrFail(),
        ));
    }

    public function getProjectsForPage($type)
    {
        $projects = Policies\ProjectPolicy::authorizedRecordsBuilder(
            Auth::user()
        )
            ->where(PagePolicy::$projectFlags[$type], '=', true)
            ->get();

        return Response::json($projects);
    }

    public function edit($type, $id)
    {
        $this->setPageType($type);
        $user = Auth::user();

        $page = Page::where('id', '=', $id)
            ->where('type', '=', Page::$pageTypes[$this->pageType])
            ->with('comments', 'comments.user')
            ->firstOrFail();

        PagePolicy::authorize($user, 'edit', $page);

        $enableParam = PagePolicy::$projectFlags[$this->pageType];

        $projects = Policies\ProjectPolicy::authorizedRecordsBuilder(
            Auth::user()
        )
            ->where($enableParam, '=', true)
            ->orderBy('lft', 'ASC')
            ->where('depth', '>', 0)
            ->get();

        $descendantIds = $page->getDescendantsAndSelf()->lists('id');

        $pagesBuilder = PagePolicy::authorizedRecordsBuilder($user)
            ->whereNotIn('id', $descendantIds);

        return View::make('Pages.edit', array(
            'page' => $page,
            'projects' => $projects,
            'pages' => $pagesBuilder->get(),
            'rootPage' => Page::where('title', '=', '__ROOT__PAGE__')->where('client_id', '=', Auth::user()->client_id)->firstOrFail(),
        ));
    }

    public function store($type)
    {
        $this->setPageType($type);
        $user = Auth::user();
        $data = Input::all();

        $rules = Page::$validationRules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        PagePolicy::authorize($user, 'store', NULL, $data);

        $data['is_public'] = 0;

        $page = new Page();
        DB::transaction(function() use ($data, $page, $user) {

            $page->fill($data);
            $page->client_id = Auth::user()->client_id;
            $page->type = Page::$pageTypes[$this->pageType];
            $page->save();

            if (!empty($data['parent_id'])) {
                $parent = Page::find($data['parent_id']);
            } else {
                $parent = Page::where('title', '=', '__ROOT__PAGE__')->where('client_id', '=', Auth::user()->client_id)->firstOrFail();
            }
            $page->makeFirstChildOf($parent);

            History::log([
                'action' => History::PAGE_CREATED,
                'target_content' => $page->title,
                'target_id' => $page->id,
                'target_class' => get_class($page),
                'additional_data' => $page->type,
                'ref_id' => $page->project->id,
                'ref_class' => get_class($page->project),
                'ref_content' => $page->project->getFullName()
            ]);


            $emailData = $page->project->emailRecipients()->map(function($user) use (&$page){
                return [
                    $user->email => [
                        'recipientName' => $user->getName(),
                        'name' => $user->getName(),
                        'pageName' => $page->title,
                        'type' => $page->getTypeNameForEmail(),
                        'link' => action(
                            'pages.show', [
                                'type' => $page->getTypeName(),
                                'resource' => $page->id
                            ]
                        )
                    ]
                ];
            })->reduce(function($result, $item){
                return array_merge($result, $item);
            }, []);

            $emailData['subject'] = 'New '. $page->getTypeNameForEmail(true) .' Created - ' . $page->title;

            MandrillEmailSender::send(
                'page-created',
                $emailData
            );

        });

        return Redirect::route('pages.show', array('type' => $this->pageType, 'id' => $page->id));
    }

    public function update($type, $id)
    {
        $this->setPageType($type);
        $user = Auth::user();

        $page = Page::where('id', '=', $id)
            ->where('type', '=', Page::$pageTypes[$this->pageType])
            ->firstOrFail();

        $data = Input::all();

        $rules = Page::$validationRules;
        $validator = Validator::make($data, $rules);

        PagePolicy::authorize($user, 'update', $page, $data);

        $parentPageId = $data['parent_id'];
        $forbiddenIds = $page->getDescendantsAndSelf()->lists('id');

        if($parentPageId && in_array($parentPageId, $forbiddenIds)){
            $validator->getMessageBag()->add('parent_id', 'Wrong parent node');
        }

        if (!$validator->messages()->isEmpty()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::transaction(function() use ($data, $page) {

            $page->fill($data);
            $page->save();

            // Update parent or make it root
            $parent = $page->parent_id;
            if ($data['parent_id'] != $parent && $data['parent_id'] != $page->id) {
                $parent = Page::find($data['parent_id']);
                $page->makeFirstChildOf($parent);
            }

            History::log([
                'action' => History::PAGE_UPDATED,
                'target_content' => $page->title,
                'target_id' => $page->id,
                'target_class' => get_class($page),
                'ref_id' => $page->project->id,
                'ref_class' => get_class($page->project),
                'ref_content' => $page->project->getFullName(),
                'additional_data' => $page->type,
            ]);

            $emailData = $page->project->emailRecipients()->map(function($user) use (&$page){
                return [
                    $user->email => [
                        'recipientName' => $user->getName(),
                        'name' => $user->getName(),
                        'type' => $page->getTypeNameForEmail(),
                        'pageName' => $page->title,
                        'link' => action(
                            'pages.show', [
                                'type' => $page->getTypeName(),
                                'resource' => $page->id
                            ]
                        )
                    ]
                ];
            })->reduce(function($result, $item){
                return array_merge($result, $item);
            }, []);

            $emailData['subject'] = $page->getTypeNameForEmail(true) .' Updated - ' . $page->title;

            MandrillEmailSender::send(
                'page-updated',
                $emailData
            );

        });

        return Redirect::route('pages.show', array('type' => array_flip(Page::$pageTypes)[$page->type], 'id' => $page->id));
    }

    public function attachFile($type, $id)
    {
        $this->setPageType($type);
        $user = Auth::user();

        $page = Page::where('id', '=', $id)
            ->where('type', '=', Page::$pageTypes[$this->pageType])
            ->firstOrFail();

        $data = Input::all();
        PagePolicy::authorize($user, 'attachFile', $page, $data);

        DB::transaction(function() use ($page, $user, $data){
            $pageFile = new PageFile();
            $pageFile->user_id = $user->id;
            $pageFile->page_id = $page->id;

            $pageFile->filename = $data['name'];
            $pageFile->mimeType = $data['mime'];
            $pageFile->size = $data['size'];
            $pageFile->url = $data['path_to_file'];

            $pageFile->save();

            History::log([
                'action' => History::PAGE_FILE_UPLOADED,
                'ref_id' => $page->id,
                'ref_class' => get_class($page),
                'ref_content' => $page->title,
                'target_id' => $pageFile->id,
                'target_class' => get_class($pageFile),
                'target_content' => $pageFile->filename,
                'additional_data' => $page->type,
            ]);

        });

    }

    public function chunkUploadFinished($type, $id){
        $this->setPageType($type);
        $user = Auth::user();

        $page = Page::where('id', '=', $id)
            ->where('type', '=', Page::$pageTypes[$this->pageType])
            ->firstOrFail();

        PagePolicy::authorize($user, 'chunkUploadFinished', $page);

        $count = Input::get('count');

        if($count === NULL){
            $count = 1;
        }

        History::log([
            'action' => History::PAGE_CHUNK_UPLOAD_FINISHED,
            'target_id' => $page->id,
            'target_content' => $page->title,
            'target_class' => get_class($page),
            'ref_id' => $page->project->id,
            'ref_class' => get_class($page->project),
            'ref_content' => $page->project->name,
            'additional_data' => $page->type
        ]);

        $emailData = $page->project->emailRecipients()->map(function($user) use (&$page){
            return [
                $user->email => [
                    'recipientName' => $user->getName(),
                    'name' => $user->getName(),
                    'pageName' => $page->title,
                    'type' => $page->getTypeNameForEmail(),
                    'link' => action(
                        'pages.show', [
                            'type' => $page->getTypeName(),
                            'resource' => $page->id
                        ]
                    )
                ]
            ];
        })->reduce(function($result, $item){
            return array_merge($result, $item);
        }, []);

        $emailData['subject'] = 'New file(s) uploaded in '. $page->getTypeNameForEmail(true) .' - ' . $page->title;

        MandrillEmailSender::send(
            'page-files-uploaded',
            $emailData
        );
    }

    public function downloadFile($id)
    {
       $user = Auth::user();
       $pageFile = PageFile::where('id', '=', $id)
            ->with('page')
            ->firstOrFail();

        PagePolicy::authorize($user, 'download', $pageFile);

        History::log([
            'action' => History::PAGE_FILE_DOWNLOADED,
            'ref_id' => $pageFile->page_id,
            'ref_class' => get_class($pageFile->page),
            'ref_content' => $pageFile->page->title,
            'target_id' => $pageFile->id,
            'target_class' => get_class($pageFile),
            'target_content' => $pageFile->filename,
            'additional_data' => $pageFile->page->type,
            'client_id' => $pageFile->page->client_id
        ]);

       return Redirect::to($pageFile->getQualifiedFilePath($pageFile->filename));
    }

    public function postComment($type, $id)
    {
        $this->setPageType($type);
        $page = Page::where('id', '=', $id)
            ->where('type', '=', Page::$pageTypes[$this->pageType])
            ->firstOrFail();

        $data = Input::all();

        PagePolicy::authorize(Auth::user(), 'comment', $page, $data);

        if (!empty($data['comment'])) {
            $comment = new PageComment(array('comment' => $data['comment']));
            $comment->user_id = Auth::user()->id;

            $page->comments()->save($comment);

            History::log([
                'action' => History::PAGE_COMMENTED,
                'target_content' => $comment->comment,
                'target_id' => $comment->id,
                'target_class' => get_class($comment),
                'ref_id' => $page->id,
                'ref_class' => get_class($page),
                'ref_content' => $page->getFullName(),
                'additional_data' => $page->type
            ]);

            $emailData = $page->project->emailRecipients()->map(function($user) use (&$page){
                return [
                    $user->email => [
                        'recipientName' => $user->getName(),
                        'name' => $user->getName(),
                        'type' => $page->getTypeNameForEmail(),
                        'pageName' => $page->title,
                        'link' => action(
                            'pages.show', [
                                'type' => $page->getTypeName(),
                                'resource' => $page->id
                            ]
                        )
                    ]
                ];
            })->reduce(function($result, $item){
                return array_merge($result, $item);
            }, []);

            MandrillEmailSender::send(
                'page-commented',
                $emailData
            );
        }

        return Redirect::route('pages.show', array('type' => $this->pageType ,'id' => $page->id));
    }

    public function deleteFile($id)
    {
        $user = Auth::user();

        $file = PageFile::where('id', '=', $id)
            ->with('Page')
            ->firstOrFail();

        PagePolicy::authorize($user, 'deleteFile', $file);

        DB::transaction(function() use (&$file){
            History::log([
                'action' => History::PAGE_FILE_DELETED,
                'target_content' => $file->url,
                'target_id' => $file->id,
                'target_class' => get_class($file),
                'additional_data' => $file->page->type,
                'ref_id' => $file->page_id,
                'ref_class' => get_class($file->page),
                'ref_content' => $file->page->title,
            ]);

            PageFile::destroy($file->id);
        });

        return Redirect::back();
    }

    public function destroy($id)
    {
        $user = Auth::user();

        $page = Page::find($id);

        $pageType = $page->getTypeName();

        PagePolicy::authorize($user, 'edit', $page);

        DB::transaction(function() use ($page, $id) {
            History::log([
                'action' => History::PAGE_DELETED,
                'ref_id' => $page->id,
                'ref_content' => $page->title
            ]);

            $page->destroy($id);
        });

        return Redirect::route('pages.index', $pageType);
    }

}
