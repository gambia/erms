<?php

use Policies\UserPolicy as AuthPolicy;
use Policies\SecurityGroupPolicy;
use \ArrayHelpers;
use User as User;

class UsersController extends \BaseController {
    public $adminLevels = array(
        USER::NORMAL => 'Regular User',
        USER::CLIENT_ADMIN => 'Client Admin',
        USER::ADMIN => 'Admin'
    );

    public function __construct(){
        $this->beforeFilter(
            'auth.perishableToken', ['only' => ['changePassword']]
        );

        $this->beforeFilter('auth');
        $this->beforeFilter(
            'csrf',
            array('only' => array(
                'store',
                'update',
                'handleChangePassword'
            ))
        );
    }

    public function index()
    {
        $user = Auth::User();

        AuthPolicy::authorize($user, 'index');

        return View::make(
            'Users.index',
            array(
                'user' => $user,
                'users' => AuthPolicy::authorizedRecords($user)
            )
        );
    }

    public function create()
    {
        AuthPolicy::authorize(Auth::User(), 'create');

        $security_groups = SecurityGroupPolicy::authorizedRecords(Auth::User())
            ->lists('name', 'id');

        $clients = Client::lists('name', 'id');

        $user = new User();

        return View::make(
            'Users.create',
            array(
                'user' => $user,
                'currentUser' => Auth::User(),
                'securityGroups' => $security_groups,
                'clients' => $clients,
                'adminLevels' => $this->adminLevels
            )
        );
    }

    public function store()
    {
        $data = Input::all();
        AuthPolicy::authorize(Auth::User(), 'store', NULL, $data);

        $validator = Validator::make($data, User::$createRules);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::transaction(function() use ($data) {
            $securityGroupIds = ArrayHelpers::pop(
                $data, 'security_group_ids', []
            );

            $user = User::createAndUpload($data);
            $user->securityGroups()->sync($securityGroupIds);

            History::log([
                'action' => History::USER_CREATED,
                'target_content' => $user->getName(),
                'target_id' => $user->id,
                'target_class' => get_class($user),
            ]);

            $securityGroups = [];

            if(count($securityGroupIds) > 0){
                $securityGroups = SecurityGroup::whereIn(
                    'id', $securityGroupIds
                )->lists('name', 'id');
            }


            //OPTIMIZE: multiple insert
            foreach($securityGroupIds as $securityGroupId){
                History::log([
                    'action' => History::SECURITY_GROUP_USER_ADDED,
                    'target_content' => $securityGroups[$securityGroupId],
                    'target_id' => $securityGroupId,
                    'target_class' => 'SecurityGroup',
                    'ref_id' => $user->id,
                    'ref_class' => get_class($user),
                    'ref_content' => $user->getName()
                ]);
            }
        });

        return Redirect::route('users.index');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        AuthPolicy::authorize(Auth::User(), 'show', $user);

        return View::make(
            'Users.show',
            array(
                'user' => $user,
                'timeOffset' => $user->getTimeOffset(),
                'canBlock' => AuthPolicy::isAuthorizedForBlock(
                    Auth::User(), $user, null
                ),
                'lastLogin' => $user->lastLogin(),
                'latestActivities' => $user->latestActivitiesExcept(
                    10,
                    [
                        History::PAGE_FILE_UPLOADED,
                        History::RECORD_FILE_UPLOADED
                    ]
                )
            )
        );
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        AuthPolicy::authorize(Auth::User(), 'edit', $user);

        $security_groups = SecurityGroupPolicy::authorizedRecords(Auth::User())
            ->lists('name', 'id');

        $selectedSecurityGroups = $user->userSecurityGroups
            ->lists('security_group_id');

        $clients = Client::lists('name', 'id');

        return View::make(
            'Users.edit',
            array(
                'user' => $user,
                'currentUser' => Auth::User(),
                'securityGroups' => $security_groups,
                'selectedSecurityGroups' => $selectedSecurityGroups,
                'clients' => $clients,
                'adminLevels' => $this->adminLevels
            )
        );
    }

    public function update($id)
    {
        $data = Input::all();
        $user = User::findOrFail($id);
        AuthPolicy::authorize(Auth::User(), 'update', $user, $data);

        $validator = Validator::make($data, $user->get_update_rules());

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::transaction(function() use ($user, $data) {

            $user->updateAndUpload($data);

            History::log([
                'action' => History::USER_UPDATED,
                'target_content' => $user->getName(),
                'target_id' => $user->id,
                'target_class' => get_class($user),
            ]);
        });

        return Redirect::route('users.show', $user->id);
    }

    public function changePassword($id = null){
        $user = $id ? User::findOrFail($id) : Auth::User();
        AuthPolicy::authorize(Auth::User(), 'changePassword', $user);

        $passwordForgotten = Session::get(
            'logged_in_with_perishable_token',
            false
        );

        return View::make(
            'Users.changePassword',
            [
                'user' => $user,
                'passwordForgotten' => $passwordForgotten
            ]
        );
    }

    public function handleChangePassword($id){
        $data = Input::all();
        $user = User::findOrFail($id);
        AuthPolicy::authorize(Auth::User(), 'changePassword', $user);

        $passwordForgotten = Session::get(
            'logged_in_with_perishable_token',
            false
        );

        $validator = Validator::make($data, User::$changePasswordRules);

        if(!Auth::User()->isAdmin() && !$passwordForgotten){
            $oldPasswordInput = Input::get('old_password');
            if(!Hash::check($oldPasswordInput, $user->password)){
                $validator->getMessageBag()->add('old_password', 'old password is not correct');
            }
        }

        if(!$validator->messages()->isEmpty()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $plainTextPassword = $data['password'];

        DB::transaction(function() use (&$user, $plainTextPassword){
            $user->password = Hash::make($plainTextPassword);
            $user->save();

            History::log([
                'action' => History::USER_PASSWORD_CHANGED,
                'target_content' => $user->getName(),
                'target_id' => $user->id,
                'target_class' => get_class($user),
            ]);
        });

        MandrillEmailSender::send(
            'changed-password',
            [
                $user->email => [
                    'recipientName' => $user->getName(),
                    'name' => $user->getName()
                ]
            ]

        );

        return Redirect::route('users.show', $user->id);
    }

    public function block($id){
        $user = User::findOrFail($id);
        AuthPolicy::authorize(Auth::User(), 'block', $user);

        $user->is_blocked = true;
        $user->save();

        History::log([
            'action' => History::USER_BLOCKED,
            'target_content' => $user->getName(),
            'target_id' => $user->id,
            'target_class' => get_class($user),
        ]);
    }

    public function unblock($id){
        $user = User::findOrFail($id);
        AuthPolicy::authorize(Auth::User(), 'block', $user);

        $user->is_blocked = false;
        $user->save();

        History::log([
            'action' => History::USER_UNBLOCKED,
            'target_content' => $user->getName(),
            'target_id' => $user->id,
            'target_class' => get_class($user),
        ]);
    }
}
