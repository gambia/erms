<?php

use Policies\ProjectPolicy;
use Policies\ProjectCasePolicy;

class ProjectsController extends \BaseController {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('only' => array('store', 'update')));
    }

    public function index()
    {
        $user = Auth::user();

        $get_view_preference = Input::get('view', null);

        if($get_view_preference !== null) {
            $user->view_preference = $get_view_preference;
            $user->save();
        }

        $projects = Project::where('depth', '=', 1)
            ->where('client_id', '=', $user->client_id)
            ->orderBy('lft', 'asc')
            ->get();

        return View::make(
            'Projects.index',
            array('projects' => $projects, 'user' => $user)
        );
    }

    public function create()
    {
        $user = Auth::user();
        ProjectPolicy::authorize($user, 'create');

        $project = new Project();

        if ($user->isClientAdmin() || $user->isAdmin()) {
            $userProjects = Project::where('client_id', '=', $user->client_id)->get();
        } else {
            $userProjects = ProjectPolicy::authorizedRecords($user);

            $userProjects = $userProjects->filter(function($item){
                return $item->isMainManager($user) || $item->hasManager($user);
            });
        }

        // Managers
        $managers = User::where('client_id', '=', $user->client_id)->orderBy('lastname', 'ASC')->get();

        // Security groups
        if ($user->isAdmin() || $user->isClientAdmin()) {
            $securityGroups = SecurityGroup::where('client_id', '=', $user->client_id)->orderBy('name', 'ASC')->get();
        } else {
            $securityGroups = $user->securityGroups->orderBy('name', 'ASC');
        }

        return View::make('Projects.create', array(
            'project' => $project,
            'availableParents' => $userProjects,
            'managers' => $managers,
            'securityGroups' => $securityGroups,
            'rootProject' => Project::where('name', '=', '__ROOT__PROJECT__')->where('client_id', '=', Auth::user()->client_id)->firstOrFail(),
        ));
    }

    public function store()
    {
        $data = Input::all();

        $rules = Project::$validationRules;
        $rules['project_key'] = 'required|unique:projects,project_key,NULL,id,client_id,'.Auth::user()->client_id;

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        ProjectPolicy::authorize(Auth::User(), 'store', NULL, $data);

        $project = new Project;

        DB::transaction(function() use ($data, &$project) {

            // Save new project
            $project->fill($data);
            $project->client_id = Auth::user()->client_id;
            $project->save();

            // Save project node position
            $parent = Project::find($data['parent_project_id']);
            $project->makeFirstChildOf($parent);

            $managers = array(new ProjectManager(array('user_id' => $data['main_manager'], 'is_main_manager' => 1)));
            if (isset($data['managers'])) {
                foreach ($data['managers'] as $manager) {
                    $managers[] = new ProjectManager(array('user_id' => $manager));
                }
            }
            $project->managers()->saveMany($managers);

            History::log([
                'action' => History::PROJECT_CREATED,
                'target_content' => $project->getFullName(),
                'ref_content' => $project->project_key,
                'target_id' => $project->id,
                'target_class' => get_class($project),
            ]);

            // Save security groups
            $project->securityGroups()->sync($data['security_groups']);

            $emailData = $project->emailRecipients()->map(function($user) use (&$project){
                return [
                    $user->email => [
                        'recipientName' => $user->getName(),
                        'name' => $user->getName(),
                        'projectName' => $project->name,
                        'link' => action(
                            'projects.show', $project->project_key
                        )
                    ]
                ];
            })->reduce(function($result, $item){
                return array_merge($result, $item);
            }, []);

            $emailData['subject'] = 'New Project - ' . $project->name;

            MandrillEmailSender::send(
                'project-created',
                $emailData
            );

        });

        return Redirect::route(
            'projects.show', array('project_key' => $project->project_key)
        );
    }

    public function update($project_key)
    {
        $project = Project::where('project_key', '=', $project_key)
            ->where('client_id', '=', Auth::user()->client_id)
            ->firstOrFail();

        $data = Input::all();

        $rules = Project::$validationRules;
        $rules['project_key'] = 'required|unique:projects,project_key,'.$project->id.',id,client_id,'.Auth::user()->client_id;

        $validator = Validator::make($data, $rules);

        ProjectPolicy::authorize(Auth::User(), 'update', $project, $data);

        $parentProjectId = $data['parent_project_id'];
        $forbiddenIds = $project->getDescendantsAndSelf()->lists('id');

        if($parentProjectId && in_array($parentProjectId, $forbiddenIds)){
            $validator->getMessageBag()->add('parent_project_id', 'Wrong parent node');
        }

        if (!$validator->messages()->isEmpty()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::transaction(function() use ($data, &$project) {

            // Update managers
            $project->managers()->delete();
            $managers = array(
                new ProjectManager(array('user_id' => $data['main_manager'], 'is_main_manager' => 1))
            );
            if (isset($data['managers'])) {
                foreach ($data['managers'] as $manager) {
                    $managers[] = new ProjectManager(array('user_id' => $manager));
                }
            }
            $project->managers()->saveMany($managers);

            // Update project parent or make it root
            $parent = $project->parent_project_id;
            if ($data['parent_project_id'] != $parent && $data['parent_project_id'] != $project->id) {
                $parent = Project::find($data['parent_project_id']);
                $project->makeFirstChildOf($parent);
            }

            $data['enable_cases'] = isset($data['enable_cases']);
            $data['enable_wiki'] = isset($data['enable_wiki']);
            $data['enable_pages'] = isset($data['enable_pages']);
            $data['enable_forms'] = isset($data['enable_forms']);

            // Update security groups
            $project->securityGroups()->sync($data['security_groups']);

            $project->update($data);

            History::log([
                'action' => History::PROJECT_UPDATED,
                'target_content' => $project->getFullName(),
                'ref_content' => $project->project_key,
                'target_id' => $project->id,
                'target_class' => get_class($project),
            ]);

            $emailData = $project->emailRecipients()->map(function($user) use (&$project){
                return [
                    $user->email => [
                        'recipientName' => $user->getName(),
                        'projectName' => $project->name,
                        'name' => $user->getName(),
                        'link' => action(
                            'projects.show', $project->project_key
                        )
                    ]
                ];
            })->reduce(function($result, $item){
                return array_merge($result, $item);
            }, []);

            $emailData['subject'] = 'Project Update - ' . $project->name;

            MandrillEmailSender::send(
                'project-updated',
                $emailData
            );
        });

        return Redirect::route(
            'projects.show', array('project_key' => $project->project_key)
        );
    }

    public function show($project_key)
    {
        $project = Project::where('project_key', '=', $project_key)
            ->where('client_id', '=', Auth::User()->client_id)
            ->firstOrFail();

        $user = Auth::user();

        ProjectPolicy::authorize($user, 'show', $project);

        $subprojects = $project->getDescendants();

        $projects = $user->projectsQueryBuilder()->get();

        $cases = ProjectCasePolicy::authorizedRecordsBuilder(
            $user
        )
            ->whereIn('project_id', [$project->id] + $subprojects->lists('id'))
            ->with('project', 'assignedTo', 'projectCaseStatus')
            ->orderBy('cases.created_at', 'desc')
            ->take(10)
            ->get();

        if ($cases->count()) {
            $comments = ProjectCaseComment::whereIn(
                'project_case_id', $cases->lists('id')
            )
                ->with('user', 'projectCase', 'projectCase.project')
                ->orderBy('updated_at', 'DESC')
                ->take(10)
                ->get();
        } else {
            $comments = new \Illuminate\Database\Eloquent\Collection();
        }

        $pages = Page::whereIn('project_id', [$project->id] + $subprojects->lists('id'))
            ->where('depth', '>', 0)
            ->with('project')
            ->orderBy('created_at', 'DESC')
            ->take(10);

        if ($user->isNormalUser()) {
            $pages->where('is_public', '=', false);
        }
        $pages = $pages->get();

        return View::make('Projects.show', array(
            'project' => $project,
            'canEdit' => ProjectPolicy::isAuthorizedForEdit($user, $project, NULL),
            'canCreateCase' => ProjectCasePolicy::isAuthorizedForCreate($user, $project, NULL),
            'timeOffset' => Auth::User()->getTimeOffset(),
            'history' => $project->getHistory(),
            'subprojects' => $subprojects,
            'comments' => $comments,
            'cases'   => $cases,
            'pages' => $pages,
            'sortBy' => null,
            'sortColumn' => null,
            'sortDirection' => null,
            'user' => $user
        ));
    }

    public function edit($project_key)
    {
        $project = Project::where('project_key', '=', $project_key)
            ->where('client_id', '=', Auth::User()->client_id)
            ->with('securityGroups', 'managers', 'managers.user')
            ->firstOrFail();

        $user = Auth::user();

        ProjectPolicy::authorize($user, 'edit', $project);

        $descendantIds = $project->getDescendantsAndSelf()->lists('id');
        $userProjectsBuilder = ProjectPolicy::authorizedRecordsBuilder($user)
            ->whereNotIn('id', $descendantIds);

        // Managers
        $managers = User::where('client_id', '=', $project->client_id)->orderBy('lastname', 'ASC')->get();

        // Security groups
        if ($user->isAdmin() || $user->isClientAdmin()) {
            $securityGroups = SecurityGroup::where('client_id', '=', $project->client_id)->orderBy('name', 'ASC')->get();
        } else {
            $securityGroups = $user->securityGroups()->orderBy('name', 'ASC')->get();

            if($securityGroups->count()) {
                //$securityGroups = $securityGroups->orderBy('name', 'ASC');

            }
        }

        return View::make('Projects.edit', array(
            'project' => $project,
            'availableParents' => $userProjectsBuilder->get(),
            'managers' => $managers,
            'securityGroups' => $securityGroups,
            'rootProject' => Project::where('name', '=', '__ROOT__PROJECT__')->where('client_id', '=', Auth::user()->client_id)->firstOrFail(),
        ));
    }

    // --------------------------------------------------------------------------
    // Sorting
    // --------------------------------------------------------------------------

    public function move($id, $direction)
    {
        $user = Auth::user();
        $project = Project::findOrFail($id);

        ProjectPolicy::authorize($user, 'edit', $project);

        try {
            if ($direction === 'up') {
                $project->moveLeft();
            } else {
                $project->moveRight();
            }
        } catch (Exception $e) {
            // We don't care
        }

        if (Request::ajax())
        {
            return 'OK';
        }
        return Redirect::back();
    }

}
