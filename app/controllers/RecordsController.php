<?php

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

use Policies\RecordPolicy;
use Policies\ProjectCasePolicy;

class RecordsController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($case_id)
	{
        $user = Auth::user();

		$case = ProjectCase::where('shortcut', '=', $case_id)
            ->where('client_id', '=', $user->client_id)
			->firstOrFail();

        RecordPolicy::authorize(Auth::User(), 'create');

		return View::make('Records.create', array(
			'record' => new ProjectCaseRecord,
			'case' => $case
		));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($case_id)
	{
		$user = Auth::user();
		$case = ProjectCase::where('shortcut', '=', $case_id)
            ->where('client_id', '=', $user->client_id)
			->with('Project')
			->firstOrFail();

        $data = Input::all();

        $validator = Validator::make($data, ProjectCaseRecord::$validationRules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        ProjectCasePolicy::authorize($user, 'show', $case);
        RecordPolicy::authorize(Auth::User(), 'store', NULL, $data);

		$record = new ProjectCaseRecord($data);
		$record->user_id = $user->id;

		$case->records()->save($record);

        History::log([
            'action' => History::RECORD_CREATED,
            'target_content' => $record->title,
            'target_id' => $record->id,
            'target_class' => get_class($record),
            'ref_id' => $case->shortcut,
            'ref_class' => get_class($case),
            'ref_content' => $case->getFullName(),
        ]);

        $emailData = $case->emailRecipients()->map(function($user) use (&$record, &$case){
            return [
                $user->email => [
                    'recipientName' => $user->getName(),
                    'name' => $user->getName(),
                    'caseId' => $case->getFullName(),
                    'link' => action(
                        'records.show', $record->id
                    )
                ]
            ];
        })->reduce(function($result, $item){
            return array_merge($result, $item);
        }, []);

        $emailData['subject'] = 'New Record - Case ' . $case->getFullName();

        MandrillEmailSender::send(
            'record-created',
            $emailData
        );

		return Redirect::route('records.show', array('id' => $record->id));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $user = Auth::User();
		$record = ProjectCaseRecord::where('id', '=', $id)
			->with('projectCase')
			->firstOrFail();

        $uploadBasePath = 'clients/' . $user->client_id . '/records/' . $record->id;

        RecordPolicy::authorize($user, 'show', $record);

        $params = array_merge(
            S3StorageEngine::getDirectUploadParams($uploadBasePath),
            [
                'record' => $record,
                'case' => $record->projectCase,
                'user' => $user
            ]
        );

		return View::make('Records.show', $params);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$record = ProjectCaseRecord::where('id', '=', $id)
			->with('projectCase')
			->firstOrFail();

        RecordPolicy::authorize(Auth::User(), 'edit', $record);

		return View::make('Records.edit', array(
			'record' => $record,
			'case' => $record->projectCase
		));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = Auth::user();
		$record = ProjectCaseRecord::where('id', '=', $id)
			->with('ProjectCase')
			->firstOrFail();

        $data = Input::all();

        $validator = Validator::make($data, ProjectCaseRecord::$validationRules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        RecordPolicy::authorize(Auth::User(), 'update', $record, $data);

		$record->update($data);
        $case = $record->projectCase;

        History::log([
            'action' => History::RECORD_UPDATED,
            'target_content' => $record->title,
            'target_id' => $record->id,
            'target_class' => get_class($record),
            'ref_id' => $case->shortcut,
            'ref_class' => get_class($case),
            'ref_content' => $case->getFullName(),
        ]);

        $emailData = $case->emailRecipients()->map(function($user) use (&$record, &$case){
            return [
                $user->email => [
                    'recipientName' => $user->getName(),
                    'caseId' => $case->getFullName(),
                    'recordName' => $record->title,
                    'name' => $user->getName(),
                    'link' => action(
                        'records.show', $record->id
                    )
                ]
            ];
        })->reduce(function($result, $item){
            return array_merge($result, $item);
        }, []);

        $emailData['subject'] = 'Record Update - Case ' . $case->getFullName();

        MandrillEmailSender::send(
            'record-updated',
            $emailData
        );

		return Redirect::route('records.show', array('id' => $record->id));
	}

	public function attachFile($id)
	{
		$user = Auth::user();
		$record = ProjectCaseRecord::where('id', '=', $id)
			->with('ProjectCase')
			->firstOrFail();

        $data = Input::all();
        RecordPolicy::authorize(Auth::User(), 'attachFile', $record, $data);

        DB::transaction(function() use ($record, $data, &$user){
            $recordFile = new ProjectCaseRecordFile();
            $recordFile->project_case_record_id = $record->id;
            $recordFile->user_id = $user->id;

            $recordFile->filename = $data['name'];
            $recordFile->mimeType = $data['mime'];
            $recordFile->size = $data['size'];
            $recordFile->url = $data['path_to_file'];

            $recordFile->save();

            History::log([
                'action' => History::RECORD_FILE_UPLOADED,
                'target_content' => $recordFile->url,
                'target_id' => $recordFile->id,
                'target_class' => get_class($recordFile),
                'ref_id' => $record->id,
                'ref_class' => get_class($record),
                'ref_content' => $record->title,
            ]);
        });
	}

	public function chunkUploadFinished($id)
	{
		$user = Auth::user();

		$record = ProjectCaseRecord::where('id', '=', $id)
			->with('ProjectCase')
			->firstOrFail();

        RecordPolicy::authorize(Auth::User(), 'chunkUploadFinished', $record);

        $count = Input::get('count');

        if($count === NULL){
            $count = 1;
        }

        History::log([
            'action' => History::RECORD_CHUNK_UPLOAD_FINISHED,
            'target_id' => $record->id,
            'target_content' => $record->title,
            'target_class' => get_class($record),
            'ref_id' => $record->projectCase->shortcut,
            'ref_class' => get_class($record->projectCase),
            'ref_content' => $record->projectCase->summary,
            'additional_data' => $count
        ]);

        $case = $record->projectCase;

        $emailData = $case->emailRecipients()->map(function($user) use (&$record){
            return [
                $user->email => [
                    'recipientName' => $user->getName(),
                    'name' => $user->getName(),
                    'link' => action(
                        'records.show', $record->id
                    ),
                    'recordName' => $record->title,
                    'caseId' => $record->projectCase->shortcut
                ]
            ];
        })->reduce(function($result, $item){
            return array_merge($result, $item);
        }, []);

        $emailData['subject'] = 'Record Files Uploaded - Case ' . $case->getFullName();

        MandrillEmailSender::send(
            'record-attachments-uploaded',
            $emailData
        );

        return '';
	}

	public function downloadFile($id)
	{
		$user = Auth::user();
		$recordFile = ProjectCaseRecordFile::where('id', '=', $id)
            ->with('caseRecord')
			->firstOrFail();

        RecordPolicy::authorize(Auth::User(), 'downloadFile', $recordFile);

        $record = $recordFile->caseRecord;

        History::log([
            'action' => History::RECORD_FILE_DOWNLOADED,
            'target_content' => $recordFile->url,
            'target_id' => $recordFile->id,
            'target_class' => get_class($recordFile),
            'ref_id' => $record->id,
            'ref_class' => get_class($record),
            'ref_content' => $record->title,
        ]);

		return Redirect::to($recordFile->getQualifiedFilePath($recordFile->filename));
	}

	public function deleteFile($id)
	{
		$user = Auth::user();

        $file = ProjectCaseRecordFile::where('id', '=', $id)
            ->with('caseRecord')
            ->firstOrFail();

        RecordPolicy::authorize($user, 'deleteFile', $file);

        DB::transaction(function() use (&$file){
            History::log([
                'action' => History::RECORD_FILE_DELETED,
                'target_content' => $file->caseRecord->title,
                'target_id' => $file->caseRecord->id,
                'target_class' => get_class($file->caseRecord),
                'ref_id' => $file->caseRecord->projectCase->shortcut,
                'ref_class' => get_class($file->caseRecord->projectCase),
                'ref_content' => $file->caseRecord->projectCase->summary,
                'additional_data' => $file->url
            ]);

            ProjectCaseRecordFile::destroy($file->id);
        });

        return Redirect::back();
    }

    public function destroy($id)
    {
        $record = ProjectCaseRecord::where('id', '=', $id)->firstOrFail();

        $case = ProjectCase::where('id', '=', $record->projectCase->id)->firstOrFail();

        RecordPolicy::authorize(Auth::User(), 'edit', $record);

        DB::transaction(function() use ($case, $record) {
            History::log([
                'action' => History::RECORD_DELETED,
                'ref_id' => $record->id,
                'ref_content' => $record->title,
                'target_id' => $case->id,
                'target_content' => $case->shortcut,
                'client_id' => $case->client_id
            ]);

            $record->destroy($record->id);
        });

        return Redirect::route('cases.show', array('id' => $case->shortcut));
    }

}
