<?php

use Policies\ClientPolicy as AuthPolicy;

class ClientsController extends \BaseController {
    public function __construct()
    {
        $this->beforeFilter('setUser');
        $this->user = Auth::User();
    }

    public function index(){
        AuthPolicy::authorize($this->user, 'index');
        $clients = AuthPolicy::authorizedRecords($this->user);

        return View::make('Clients.index', [
            'clients' => $clients, 'user' => $this->user
        ]);
    }

    public function create(){
        AuthPolicy::authorize($this->user, 'create');

        return View::make('Clients.create', [
            'client' => new Client(),
            'user' => $this->user,
            'timezones' => Dates::timezonesForSelectBox()
        ]);
    }

    public function store(){
        $data = Input::all();

        $data['enable_intranet'] = isset($data['enable_intranet']);
        $data['enable_wiki']     = isset($data['enable_wiki']);
        $data['enable_forms']    = isset($data['enable_forms']);

        AuthPolicy::authorize($this->user, 'create', NULL, $data);

        $validator = Validator::make($data, Client::$validationRules);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::transaction(function() use ($data){
            $admin_firstname = \ArrayHelpers::pop($data, 'admin_firstname');
            $admin_lastname = \ArrayHelpers::pop($data, 'admin_lastname');
            $admin_job_title = \ArrayHelpers::pop($data, 'admin_job_title');
            $admin_email = \ArrayHelpers::pop($data, 'admin_email');

            $client = new Client($data);

            if(!$client->save()){
                throw new Exception('Client has not been saved');
            };

            $clientAdmin = new User();
            $clientAdmin->firstname = $admin_firstname;
            $clientAdmin->lastname = $admin_lastname;
            $clientAdmin->job_title = $admin_job_title;
            $clientAdmin->email = $admin_email;
            $clientAdmin->client_id = $client->id;
            $clientAdmin->admin_level = User::CLIENT_ADMIN;
            if(!$clientAdmin->save()){
                throw new Exception('Client admin has not been saved');
            };

            $rootProject = new Project();
            $rootProject->name = '__ROOT__PROJECT__';
            $rootProject->summary = '__ROOT__PROJECT__';
            $rootProject->project_key = 'ROOT-PROJECT';
            $rootProject->depth = 0;
            $rootProject->client_id = $client->id;
            if(!$rootProject->save()){
                throw new Exception('Root project has not been saved');
            };

            $rootPage = new Page();
            $rootPage->title = '__ROOT__PAGE__';
            $rootPage->description = '__ROOT__PAGE__';
            $rootPage->depth = 0;
            $rootPage->client_id = $client->id;
            $rootPage->type = Page::TYPE_WIKI;
            if(!$rootPage->save()){
                throw new Exception('Root page has not been saved');
            };
        });

        return Redirect::route('clients.index');
    }

    public function show($id){
        $client = Client::findOrFail($id);
        AuthPolicy::authorize($this->user, 'show', $client);

        return View::make('Clients.show', [
            'client' => $client, 'user' => $this->user
        ]);
    }

    public function edit($id){
        $client = Client::findOrFail($id);
        AuthPolicy::authorize($this->user, 'edit', $client);

        return View::make('Clients.edit', [
            'client' => $client,
            'user' => $this->user,
            'timezones' => Dates::timezonesForSelectBox()
        ]);
    }

    public function update($id){
        $data = Input::all();
        $client = Client::findOrFail($id);

        $data['enable_intranet'] = isset($data['enable_intranet']);
        $data['enable_wiki']     = isset($data['enable_wiki']);
        $data['enable_forms']    = isset($data['enable_forms']);


        AuthPolicy::authorize($this->user, 'update', $client, $data);

        $validator = Validator::make($data, Client::$updateValidationRules);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $client->update($data);

        return Redirect::route('clients.index');
    }
};
