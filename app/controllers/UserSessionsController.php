<?php

class UserSessionsController extends \BaseController {

    public function create()
    {
        return View::make('UserSessions/login');
    }

    public function store()
    {
        $email = Input::get('email');
        $user = User::where('email', '=', $email)->first();

        if($user && $user->is_blocked){
            return Redirect::to('login')
                ->withInput()
                ->with('account_blocked', true);
        }

        if(
            Auth::attempt(array(
                'email'     => $email,
                'password'  => Input::get('password')
            ))
        ) {
            History::log(['action' => History::USER_LOGIN_SUCCESS, 'target_id' => $user->id, 'target_class' => get_class($user)]);

            return Redirect::intended('/');
        }

        $target_class = NULL;
        $user_id = NULL;
        $client_id = NULL;

        if($user){
            $target_class = get_class($user);
            $user_id = $user->id;
            $client_id = $user->client_id;
        }

        History::log([
            'action' => History::USER_LOGIN_FAILED,
            'content' => $email,
            'target_id' => $user_id,
            'target_class' => $target_class,
            'user_id' => $user_id,
            'client_id' => $client_id
        ]);

        return Redirect::to('login')
            ->withInput()
            ->with('login_error', true);
    }

    public function destroy()
    {
        $user = Auth::user();

        if($user){
            History::log(['action' => History::USER_LOGOUT, 'target_id' => $user->id, 'target_class' => get_class($user)]);
        }

        Auth::logout();
        Session::flush();

        return Redirect::to('login');
    }
}
