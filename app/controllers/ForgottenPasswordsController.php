<?php

class ForgottenPasswordsController extends \BaseController {
    public function create(){
        return View::make(
            'ForgottenPasswords.create'
        );
    }

    public function store(){
        $email = ArrayHelpers::retrieve(Input::all(), 'email');
        $user = User::where('email', '=', $email)->first();

        if($user){
            MandrillEmailSender::send(
                'forgotten-password',
                [
                    $user->email => [
                        'recipientName' => $user->getName(),
                        'name' => $user->getName(),
                        'link' => route(
                            'users.forgottenPassword',
                            [
                                'perishable_token' => $user->perishable_token
                            ]
                        )
                    ]
                ]
            );

            History::log([
                'action' => History::USER_PASSWORD_RESET_INIT,
                'target_content' => $user->getName(),
                'target_id' => $user->id,
                'target_class' => get_class($user),
            ]);

            return Redirect::to('/login')
                ->with('forgotten_password_email_sent', true);
        }{
            return Redirect::to('/forgotten_password')
                ->withInput()
                ->with('email_not_found_error', true);
        }
    }
}
