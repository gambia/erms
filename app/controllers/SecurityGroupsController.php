<?php

use Policies\SecurityGroupPolicy as AuthPolicy;
use Policies\UserPolicy;

class SecurityGroupsController extends \BaseController {
    public function __construct(){
        $this->beforeFilter('csrf', array('only' => array('store', 'update')));
    }

    public function index()
    {
        AuthPolicy::authorize(Auth::User(), 'index');
        $securityGroup = new SecurityGroup();

        return View::make(
            'SecurityGroups.index',
            array(
                'securityGroups' => AuthPolicy::authorizedRecords(Auth::User()),
                'securityGroup' => $securityGroup,
                'currentUser' => Auth::User()
            )
        );
    }

    public function create()
    {
        AuthPolicy::authorize(Auth::User(), 'create');
        $user = Auth::User();

        $securityGroup = new SecurityGroup();
        $users = UserPolicy::authorizedRecords($user)->lists('name', 'id');

        return View::make(
            'SecurityGroups.create',
            array(
                'currentUser' => $user,
                'securityGroup' => $securityGroup,
                'users' => $users,
                'selectedUsers' => null,
            )
        );
    }

    public function store()
    {
        $user = Auth::User();
        $data = Input::all();

        $data['client_id'] = $user->client_id;

        AuthPolicy::authorize($user, 'store', NULL, $data);

        $validator = Validator::make($data, SecurityGroup::$rules);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::transaction(function() use ($data) {
            $userIds = ArrayHelpers::pop(
                $data, 'user_ids', []
            );

            $securityGroup = SecurityGroup::create($data);
            $securityGroup->users()->sync($userIds);

            History::log([
                'action' => History::SECURITY_GROUP_CREATED,
                'target_content' => $securityGroup->name,
                'target_id' => $securityGroup->id,
                'target_class' => get_class($securityGroup),
            ]);

            $users = [];
            if(count($userIds) > 0){
                $users = User::whereIn(
                    'id', $userIds
                )->lists('name', 'id');
            }

            //OPTIMIZE: multiple insert
            foreach($userIds as $userId){
                History::log([
                    'action' => History::SECURITY_GROUP_USER_ADDED,
                    'target_content' => $securityGroup->name,
                    'target_id' => $securityGroup->id,
                    'target_class' => get_class($securityGroup),
                    'ref_id' => $userId,
                    'ref_class' => 'User',
                    'ref_content' => $users[$userId]
                ]);
            }
        });

        return Redirect::route('securitygroups.index');
    }

    public function show($id)
    {
        $securityGroup = SecurityGroup::findOrFail($id);
        AuthPolicy::authorize(Auth::User(), 'show', $securityGroup);

        return View::make(
            'SecurityGroups.show',
            array('securityGroup' => $securityGroup)
        );
    }

    public function edit($id)
    {
        $user = Auth::User();

        $securityGroup = SecurityGroup::findOrFail($id);
        AuthPolicy::authorize($user, 'edit', $securityGroup);

        $users = UserPolicy::authorizedRecordsBuilder($user)
            ->get()
            ->lists('name', 'id');

        $selectedUsers = $securityGroup->userSecurityGroups->lists('user_id');

        return View::make(
            'SecurityGroups.edit',
            array(
                'securityGroup' => $securityGroup,
                'users' => $users,
                'currentUser' => $user,
                'selectedUsers' => $selectedUsers
            )
        );
    }

    public function update($id)
    {
        $user = Auth::User();
        $data = Input::all();

        $data['client_id'] = $user->client_id;

        $securityGroup = SecurityGroup::findOrFail($id);

        AuthPolicy::authorize($user, 'update', $securityGroup, $data);

        $validator = Validator::make($data, SecurityGroup::$rules);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::transaction(function() use ($data, $securityGroup) {
            $userIds = ArrayHelpers::pop(
                $data, 'user_ids', []
            );

            $securityGroup->update($data);
            History::log([
                'action' => History::SECURITY_GROUP_UPDATED,
                'target_content' => $securityGroup->name,
                'target_id' => $securityGroup->id,
                'target_class' => get_class($securityGroup),
            ]);

            $currentUserIds = $securityGroup->users->lists('id');
            $securityGroup->users()->sync($userIds);

            $removed = array_diff(
                $currentUserIds, $userIds
            );

            $added = array_diff(
                $userIds, $currentUserIds
            );

            $all = array_merge($currentUserIds, $userIds);

            $users = [];
            if(count($userIds) > 0){
                $users = User::whereIn(
                    'id', $all
                )->get()->lists('name', 'id');
            }

            //OPTIMIZE: multiple inserts
            foreach($removed as $rem){
                History::log([
                    'action' => History::SECURITY_GROUP_USER_REMOVED,
                    'target_content' => $securityGroup->name,
                    'target_id' => $securityGroup->id,
                    'target_class' => 'SecurityGroup',
                    'ref_id' => $rem,
                    'ref_class' => 'User',
                    'ref_content' => $users[$rem]
                ]);
            }

            foreach($added as $add){
                History::log([
                    'action' => History::SECURITY_GROUP_USER_ADDED,
                    'target_content' => $securityGroup->name,
                    'target_id' => $securityGroup->id,
                    'target_class' => 'SecurityGroup',
                    'ref_id' => $add,
                    'ref_class' => 'User',
                    'ref_content' => $users[$add]
                ]);
            }
        });

        return Redirect::route('securitygroups.index');
    }
}
