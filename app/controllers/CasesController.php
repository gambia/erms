<?php
use Policies\ProjectPolicy,
    Policies\ProjectCasePolicy;

class CasesController extends \BaseController {
    public function __construct(){
        $this->beforeFilter('csrf', array(
            'only' => array('store', 'update', 'comment'))
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $from = Input::get('from', 0);
        $limit = Input::get('limit', 15);
        $project_key = Input::get('project', NULL);
        $status = Input::get('status', NULL);
        $sortBy = Input::get('sort', 'updated_at,d');
        $user = Auth::user();

        $casesQuery = ProjectCasePolicy::authorizedRecordsBuilder(
            Auth::user()
        );

        if ($project_key !== NULL) {

            $project = Project::where('project_key', '=', $project_key)
                ->where('client_id', '=', Auth::user()->client_id)
                ->firstOrFail();

            if (!ProjectPolicy::isAuthorizedForShow(Auth::user(), $project)) {
                throw new Policies\UnauthorizedException;
            }

            $filteredProjects = $project->getDescendantsAndSelf()->lists('id');
            $casesQuery->whereIn('project_id', $filteredProjects);
        }

        if ($status !== NULL) {
            $casesQuery->where('project_case_status_id', '=', $status);
            $projectCaseStatus = ProjectCaseStatus::find($status);
        }

        $cases = $casesQuery;

        // --------------------------------------------------------------------------
        // Resultset filtering
        // --------------------------------------------------------------------------
        $filter = Input::get('filter', array());
        if (isset($filter['id']) && !empty($filter['id'])) {
            $cases = $cases->where(DB::raw('LOWER(shortcut)'), 'LIKE', '%'.strtolower($filter['id']).'%');
        }

        if (isset($filter['summary']) && !empty($filter['summary'])) {
            $cases = $cases->where(DB::raw('LOWER(summary)'), 'LIKE', '%'.strtolower($filter['summary']).'%');
        }

        if (isset($filter['assigned_to']) && !empty($filter['assigned_to'])) {
            $cases = $cases->where('assigned_to', '=', $filter['assigned_to']);
        }

        if (isset($filter['project_case_status_id']) && !empty($filter['project_case_status_id'])) {
            $cases = $cases->where('project_case_status_id', '=', $filter['project_case_status_id']);
        }

        if (isset($filter['priority']) && !empty($filter['priority'])) {
            $cases = $cases->where('priority', '=', $filter['priority']);
        }

        if (isset($filter['created_at']) && !empty($filter['created_at'])) {
            $cases= $cases->where('created_at', '>', $this->filterTimeWithCarbon($filter['created_at'], Carbon\Carbon::now()));
        }

        if (isset($filter['updated_at']) && !empty($filter['updated_at'])) {
            $cases= $cases->where('updated_at', '>', $this->filterTimeWithCarbon($filter['updated_at'], Carbon\Carbon::now()));
        }

        $countQuery = clone($casesQuery);
        $casesCount = $countQuery->count();

        $cases
            ->skip($from)
            ->take($limit);

        // --------------------------------------------------------------------------
        // Column sorting
        // --------------------------------------------------------------------------
        $sort = explode(',' ,$sortBy);
        $dir = 'DESC';
        if (count($sort) == 2 && in_array($sort[0], ProjectCase::$sortableColumns)) {
            $sortColumn = $sort[0];
            $dir = $sort[1] === 'a' ? 'ASC' : 'DESC';
        } elseif (count($sort) == 1  && in_array($sort[0], ProjectCase::$sortableColumns)) {
            $sortColumn = $sort[0];
        } else {
            $sortColumn = 'updated_at';
        }

        if ($sortColumn === 'assigned_to') {
            $cases = $cases->join('users as u', 'u.id', '=', 'assigned_to')
                ->orderBy('u.lastname', $dir);
        } else {
            $cases = $cases->orderBy($sortColumn, $dir);
        }

        $cases = $cases->get();

        return View::make('Cases.index', array(
            'cases' => $cases,
            'count' => $casesCount,
            'timeOffset' => Auth::User()->getTimeOffset(),
            'statuses' => ProjectCaseStatus::all(),
            'f_project' => $project_key,
            'f_status' => $status,
            'project_case_status' => isset($projectCaseStatus) ? $projectCaseStatus : null,
            'project' => isset($project) ? $project : null,
            'canCreate' => ProjectCasePolicy::isAuthorizedForCreate(Auth::User(), NULL, NULL),
            'from' => $from,
            'limit' => $limit,
            'page' => round($from/$limit),
            'sortBy' => $sortColumn,
            'sortDirection' => $dir,
            'sortString' => $sortBy,
            'users' => User::where('client_id', '=', Auth::user()->client_id)->orderBy('lastname', 'ASC')->get(),
            'showFilter' => true,
            'filter' => $filter,
            'timeOptions' => [
                '6h' => '6 Hours ago', '12h' => '12 Hours ago', '1d' => '1 Day ago', '1w' => '1 Week ago',
                '2w' => '2 Weeks ago', '1m' => '1 Month ago', '3m' => '3 Months ago', '1y' => '1 Year ago',
            ]
        ));
    }

    protected function filterTimeWithCarbon($value, $ts)
    {
        if (preg_match('/(\d+)h/', $value, $m)) {
            return $ts->subHours($m[1]);
        }

        if (preg_match('/(\d+)d/', $value, $m)) {
            return $ts->subDays($m[1]);
        }

        if (preg_match('/(\d+)w/', $value, $m)) {
            return $ts->subWeeks($m[1]);
        }

        if (preg_match('/(\d+)m/', $value, $m)) {
            return $ts->subMonths($m[1]);
        }

        if (preg_match('/(\d+)y/', $value, $m)) {
            return $ts->subYears($m[1]);
        }

        return $ts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user       = Auth::user();
        $client_id  = $user->client_id;

        ProjectCasePolicy::authorize($user, 'create');

        $case = new ProjectCase();
        $projects = ProjectPolicy::authorizedRecordsBuilder($user)->orderBy('lft', 'ASC')->where('depth', '>', 0)->get();

        $users      = User::where('client_id', '=', $client_id)->orderBy('lastname', 'ASC')->get()->lists('name', 'id');

        $priorities = ProjectCase::$priorities;

        if(!$user->isAdmin() && !$user->isClientAdmin()) {
            $projects = $projects->filter(function($item) use ($user){
                return $item->isMainManager($user) || $item->hasManager($user);
            });
        }

        return View::make('Cases.create', array(
            'case' => $case,
            'projects' => $projects,
            'users' => $users,
            'priorities' => $priorities,
            'statuses' => ProjectCaseStatus::whereNull('client_id')->orderBy('id', 'ASC')->get()->lists('name', 'id'),
            'privileged_users' => $case->privilegedUsers()->get()->lists('id')
        ));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data = Input::all();
        $data['is_confidential'] = isset($data['is_confidential']);
        $data['privileged_users'] = isset($data['privileged_users']) ? $data['privileged_users'] : [];

        $rules = ProjectCase::$rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }


        ProjectCasePolicy::authorize(Auth::User(), 'store', NULL, $data);
        $project = Project::where('id', '=', $data['project_id'])->firstOrFail();

        $case = new \ProjectCase;
        DB::transaction(function() use ($data, &$case, $project) {
            $case->fill($data);
            $case->client_id = $project->client_id;
            $case->save();

            $case->privilegedUsers()->sync($data['privileged_users']);

            History::log([
                'action' => History::CASE_CREATED,
                'target_content' => $case->getFullName(),
                'target_id' => $case->shortcut,
                'target_class' => get_class($case),
            ]);
        });

        $emailData = $case->emailRecipients()->map(function($user) use (&$case){
            return [
                $user->email => [
                    'recipientName' => $user->getName(),
                    'name' => $user->getName(),
                    'caseName' => $case->getFullName(),
                    'link' => action(
                        'cases.show', $case->shortcut
                    )
                ]
            ];
        })->reduce(function($result, $item){
            return array_merge($result, $item);
        }, []);

        $emailData['subject'] = 'New Case - ' . $case->getFullName();

        MandrillEmailSender::send(
            'case-created',
            $emailData
        );

        return Redirect::route('cases.show', array('id' => $case->shortcut));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = Auth::User();

        $case = ProjectCase::where('shortcut', '=', $id)
            ->where('client_id', '=', $user->client_id)
            ->with('comments', 'comments.user')
            ->firstOrFail();

        ProjectCasePolicy::authorize(Auth::User(), 'show', $case);

        $records = ProjectCaseRecord::where('project_case_id', $case->id)
            ->orderBy('updated_at', 'DESC')
            ->get();


        if ($records->count()) {
            $recordFiles = ProjectCaseRecordFile::whereIn('project_case_record_id', $records->lists('id'))
                ->orderBy('created_at', 'ASC')
                ->get();
        } else {
            $recordFiles = new \Illuminate\Database\Eloquent\Collection();
        }

        return View::make('Cases.show', array(
            'case' => $case,
            'canEdit' => ProjectCasePolicy::isAuthorizedForEdit(
                $user, $case, NULL
            ),
            'canStore' => ProjectCasePolicy::isAuthorizedForStore(
                $user, $case, NULL
            ),
            'canDelete' => ProjectCasePolicy::isAuthorizedForDelete(
                $user, $case, NULL
            ),
            'history' => $case->getHistory(),
            'records' => $records,
            'files' => $recordFiles,
            'user' => $user
        ));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $client_id = Auth::user()->client_id;

        $case = ProjectCase::where('shortcut', '=', $id)
            ->where('client_id', '=', $client_id)
            ->firstOrFail();

        ProjectCasePolicy::authorize(Auth::User(), 'edit', $case);

        $projects = ProjectPolicy::authorizedRecordsBuilder(Auth::user())->orderBy('lft', 'ASC')->where('depth', '>', 0)->get();

        $users     = User::where('client_id', '=', $client_id)->get()->lists('name', 'id');

        $priorities = ProjectCase::$priorities;

        return View::make('Cases.edit', array(
            'case' => $case,
            'projects' => $projects,
            'users' => $users,
            'priorities' => $priorities,
            'statuses' => ProjectCaseStatus::whereNull('client_id')->orderBy('id', 'ASC')->get()->lists('name', 'id'),
            'privileged_users' => $case->privilegedUsers()->get()->lists('id')
        ));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data = Input::all();
        $user = Auth::User();

        $data['is_confidential'] = isset($data['is_confidential']);
        $data['privileged_users'] = isset($data['privileged_users']) ? $data['privileged_users'] : [];

        $case = ProjectCase::where('shortcut', '=', $id)
            ->where('client_id', '=', $user->client_id)
            ->firstOrFail();

        ProjectCasePolicy::authorize(Auth::User(), 'update', $case, $data);

        $rules = ProjectCase::$rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $oldStatusId = $case->project_case_status_id;

        DB::transaction(function() use ($data, &$case, $oldStatusId) {

            if ($data['project_id'] != $case->project_id) {
                $cproject = Project::where('id', '=', $data['project_id'])->firstOrFail();
                $iterator = $cproject->last_case_id + 1;

                $cproject->last_case_id = $iterator;
                $cproject->timestamps = false;   // We don't want to project this change into timestamps
                $cproject->save();

                $case->shortcut = $cproject->project_key . '-' . $iterator;
            }

            $case->fill($data);
            $case->save();

            $case->privilegedUsers()->sync($data['privileged_users']);

            History::log([
                'action' => History::CASE_UPDATED,
                'target_content' => $case->getFullName(),
                'target_id' => $case->shortcut,
                'target_class' => get_class($case),
                'ref_content' => $case->shortcut,
            ]);

            if(
                $case->project_case_status_id == ProjectCaseStatus::STATUS_CLOSED &&
                $oldStatusId != ProjectCaseStatus::STATUS_CLOSED
            ){
                History::log([
                    'action' => History::CASE_CLOSED,
                    'target_content' => $case->getFullName(),
                    'target_id' => $case->shortcut,
                    'target_class' => get_class($case),
                    'ref_content' => $case->shortcut,
                ]);
            }
        });

        $emailData = $case->emailRecipients()->map(function($user) use (&$case){
            return [
                $user->email => [
                    'recipientName' => $user->getName(),
                    'name' => $user->getName(),
                    'caseId' => $case->getFullName(),
                    'link' => action(
                        'cases.show', $case->shortcut
                    )
                ]
            ];
        })->reduce(function($result, $item){
            return array_merge($result, $item);
        }, []);

        $emailData['subject'] = 'Case Update - ' . $case->getFullName();

        MandrillEmailSender::send(
            'case-updated',
            $emailData
        );

        return Redirect::route('cases.show', array('id' => $case->shortcut));
    }

    public function postComment($id)
    {
        $case = ProjectCase::findOrFail($id);
        $data = Input::all();

        ProjectCasePolicy::authorize(Auth::User(), 'comment', $case, $data);

        if (!empty($data['comment'])) {
            $comment = new ProjectCaseComment(
                array('comment' => $data['comment'])
            );

            $comment->user_id = Auth::user()->id;

            $case->comments()->save($comment);

            History::log([
                'action' => History::CASE_COMMENTED,
                'target_content' => $comment->comment,
                'target_id' => $comment->id,
                'target_class' => get_class($comment),
                'ref_id' => $case->shortcut,
                'ref_class' => get_class($case),
                'ref_content' => $case->getFullName(),
            ]);

            $emailData = $case->emailRecipients()->map(function($user) use (&$case){
                return [
                    $user->email => [
                        'recipientName' => $user->getName(),
                        'name' => $user->getName(),
                        'caseId' => $case->getFullName(),
                        'link' => action(
                            'cases.show', $case->shortcut
                        )
                    ]
                ];
            })->reduce(function($result, $item){
                return array_merge($result, $item);
            }, []);

            $emailData['subject'] = 'New Comment - ' . $case->getFullName();

            MandrillEmailSender::send(
                'case-commented',
                $emailData
            );
        }

        return Redirect::route('cases.show', array('id' => $case->shortcut));
    }

    public function destroy($id)
    {

        $user = Auth::user();

        $case = ProjectCase::where('shortcut', '=', $id)
            ->where('client_id', '=', $user->client_id)
            ->firstOrFail();

        ProjectCasePolicy::isAuthorizedForDelete($user, $case, NULL);

        DB::transaction(function() use ($case) {
            History::log([
                'action' => History::CASE_DELETED,
                'ref_id' => $case->shortcut,
                'ref_content' => $case->summary,
                'target_id' => $case->project->key,
                'target_content' => $case->project->title
            ]);

            $case->destroy($case->id);
        });

        return Redirect::route('cases.index');
    }
}
