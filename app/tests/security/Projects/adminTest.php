<?php

class ProjectsAsAdminTest extends TestCase {
    public function setUp(){
        parent::setUp();

        $this->password = 'password';

        $this->testClient = Client::create(array(
            'name' => 'Test client',
            'client_key' => 'test'
        ));

        $this->admin = new User(array(
            'name' => 'Admin Adminovic',
            'job_title' => 'Admin',
            'email' => 'admin@admin.ru',
            'password' => Hash::make($this->password),
        ));

        $this->admin->admin_level = User::ADMIN;
        $this->admin->regeneratePerishableToken();
        $this->admin->save();

		$this->client->request('POST', '/login', array(
            'email' => $this->admin->email,
            'password' => $this->password
        ));
    }

	public function testIndex()
	{
		$crawler = $this->client->request('GET', '/projects');
		$this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testCreate()
	{
		$crawler = $this->client->request('GET', '/projects/create');
		$this->assertTrue($this->client->getResponse()->isOk());
	}
}
