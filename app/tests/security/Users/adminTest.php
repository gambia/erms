<?php

class UsersAsAdminTest extends TestCase {
    public function setUp(){
        parent::setUp();

        $this->password = 'password';

        $this->testClient = Client::create(array(
            'name' => 'Test client',
            'client_key' => 'test'
        ));

        $this->user = new User(array(
            'name' => 'John Doe',
            'client_id' => $this->testClient->id,
            'job_title' => 'Regular User',
            'email' => 'john.doe@gmail.com',
            'password' => Hash::make($this->password)
        ));

        //Why is the UserObserver triggered only the first time?
        $this->user->regeneratePerishableToken();
        $this->user->save();

        $this->admin = new User(array(
            'name' => 'Admin Adminovic',
            'client_id' => $this->testClient->id,
            'job_title' => 'Admin',
            'email' => 'admin@admin.ru',
            'password' => Hash::make($this->password),
        ));

        $this->admin->admin_level = User::ADMIN;
        $this->admin->regeneratePerishableToken();
        $this->admin->save();

		$this->client->request('POST', '/login', array(
            'email' => $this->admin->email,
            'password' => $this->password
        ));
    }

	public function testIndex()
	{
		$crawler = $this->client->request('GET', '/users');
		$this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testCreate()
	{
		$crawler = $this->client->request('GET', '/users/create');
		$this->assertTrue($this->client->getResponse()->isOk());
	}

    //Failing because of the observer being run only once
	//public function testStore()
	//{
    //    $securityGroup = SecurityGroup::create(array(
    //        'client_id' => $this->testClient->id,
    //        'name' => 'TestSecurityGroup'
    //    ));

	//	$crawler = $this->client->request('POST', '/users', array(
    //        'client_id' => $this->testClient->id,
    //        'job_title' => 'Test Subject',
    //        'name' => 'Test User',
    //        'email' => 'testuser@swazicms.com',
    //        'password' => 'password',
    //        'password_confirmation' => 'password',
    //        'security_group_id' => $securityGroup->id
    //    ));

	//	$this->assertRedirectedTo('/users');
	//}

	public function testEdit()
	{
		$crawler = $this->client
            ->request('GET', '/users/'.$this->user->id.'/edit');

		$this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testUpdate()
	{
        $securityGroup = SecurityGroup::create(array(
            'client_id' => $this->testClient->id,
            'name' => 'TestSecurityGroup'
        ));

		$crawler = $this->client->request(
            'PUT',
            '/users/'.$this->user->id,
            array(
                'email' => 'somenewemail@swazicms.com'
            )
        );

		$this->assertRedirectedTo('/users/'.$this->user->id);
	}
}
