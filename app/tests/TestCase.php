<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {
    public function setUp(){
        parent::setUp();

        $_SERVER['HTTP_USER_AGENT'] = 'testUserAgent';

        $tables = [
            'clients',
            'users',
            'security_groups',
            'user_security_groups',
            'projects',
            'history',
            'project_manager',
            'project_security_groups',
        ];

        foreach($tables as $table){
            DB::delete('delete from ' . $table . ' cascade');
        }
    }

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../bootstrap/start.php';
	}

}
