<?php

namespace Policies;

use \ArrayHelpers;

class StatisticsPolicy extends Policy {

    public static function isAuthorizedForIndex($user, $resource, $submitData){
        return !$user->isNormalUser();
    }

}
