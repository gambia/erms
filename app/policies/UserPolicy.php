<?php

namespace Policies;

class UserPolicy extends Policy {
    public static function authorizedRecordsBuilder($user){
        return \User::where('client_id', '=', $user->client_id)->orderBy('lastname', 'ASC');
    }

    public static function isAuthorizedForIndex($user, $resource, $submitData){
        return true;
    }

    public static function isAuthorizedForCreate($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }
        return $user->isClientAdmin();
    }

    public static function isAuthorizedForStore($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }

        if(!$user->isClientAdmin()) { return false; }

        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId != $user->client_id) { return false; }

        $securityGroupIds = \ArrayHelpers::retrieve(
            $submitData, 'security_group_ids', []
        );

        $authorizedSecurityGroupIds = SecurityGroupPolicy::authorizedRecordIds(
            $user
        );

        foreach($securityGroupIds as $securityGroupId){
            if(!in_array($securityGroupId, $authorizedSecurityGroupIds)){
                return false;
            }
        }

        return true;
    }

    public static function isAuthorizedForShow($user, $resource){
        if($user->isAdminOrClientAdmin()) {
            return in_array($resource->id, static::authorizedRecordIds($user));
        }
        return ($resource->id == $user->id);
    }

    public static function isAuthorizedForEdit($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }
        return in_array($resource->id, static::authorizedRecordIds($user));
    }

    public static function isAuthorizedForChangePassword($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }
        return $resource->id == $user->id;
    }

    public static function isAuthorizedForUpdate($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }

        if(!in_array($resource->id, static::authorizedRecordIds($user))){
           return false;
        }

        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId != $user->client_id) { return false; }

        $securityGroupIds = \ArrayHelpers::retrieve(
            $submitData,
            'security_group_ids'
        );

        return $securityGroupIds ?
            static::isAuthorizedForSecurityGroupsUpdate(
                $user,
                $resource->id,
                $securityGroupIds
            ) :
            true;
    }

    public static function isAuthorizedForSecurityGroupsUpdate($user, $resource, $securityGroupIds){
        if($user->isNormalUser()){
            return false;
        }

        $authorizedSecurityGroupIds = SecurityGroupPolicy::authorizedRecordIds(
            $user
        );

        foreach($securityGroupIds as $securityGroupId){
            if(!in_array($securityGroupId, $authorizedSecurityGroupIds)){
                return false;
            }
        }

        return true;
    }

    public static function isAuthorizedForDestroy($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }
        return in_array($resource->id, static::authorizedRecordIds($user));
    }

    public static function isAuthorizedForBlock($user, $resource, $submitData){
        if($user->id == $resource->d) { return false; }
        if(static::masterCheck($user)) { return true; }
        if($user->isClientAdmin()){
            return in_array($resource->id, static::authorizedRecordIds($user));
        }

        return false;
    }
};
