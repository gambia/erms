<?php

namespace Policies;

use \ArrayHelpers;

abstract class Policy {
    //This MUST return a Illuminate\Database\Eloquent\Builder
    public static function authorizedRecordsBuilder($user){
        throw new Exception('Not implemented');
    }

    //This MUST return a collection object
    // NOT a plain array
    public static function authorizedRecords($user){
        return static::authorizedRecordsBuilder($user)->get();
    }

    public static function authorizedRecordIds($user){
        return static::authorizedRecords($user)
            ->map(function($user){
                return $user->id;
            })
            ->all();
    }

    public static function authorize(
        $user, $action, $resource = NULL, $submitData = NULL,
        $throwExceptions = true
    ){
        $authMethod = 'isAuthorizedFor' . ucfirst($action);

        if(method_exists(get_called_class(), $authMethod)){
            $authorized = static::$authMethod(
                $user, $resource, $submitData
            );
        }
        else {
            throw new \Exception('Cannot authorize unknown action: '. $action);
        }

        if(!$authorized && $throwExceptions){
            throw new UnauthorizedException;
        }

        return $authorized;
    }

    public static function masterCheck($user){
        return $user->isAdmin();
    }

    public static function isAuthorizedForIndex($user, $resource, $submitData){
        return static::masterCheck($user);
    }

    public static function isAuthorizedForCreate($user, $resource, $submitData){
        return static::masterCheck($user);
    }

    public static function isAuthorizedForStore($user, $resource, $submitData){
        return static::masterCheck($user);
    }

    public static function isAuthorizedForShow($user, $resource){
        return static::masterCheck($user);
    }

    public static function isAuthorizedForEdit($user, $resource, $submitData){
        return static::masterCheck($user);
    }

    public static function isAuthorizedForUpdate($user, $resource, $submitData){
        return static::masterCheck($user);
    }

    public static function isAuthorizedForDestroy($user, $resource, $submitData){
        return static::masterCheck($user);
    }
}
