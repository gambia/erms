<?php

namespace Policies;

class SecurityGroupPolicy extends Policy {
    public static function authorizedRecordsBuilder($user){
        if(static::masterCheck($user) || $user->isClientAdmin() || $user->isManagerOrRepresentative()) {
            return \SecurityGroup::where('client_id', '=', $user->client_id);
        }

        return \User::where('id', '=', 0);
    }

    public static function isAuthorizedForIndex($user, $resource, $submitData){
        return !$user->isNormalUser() || $user->isManagerOrRepresentative();
    }

    public static function isAuthorizedForCreate($user, $resource, $submitData){
        return !$user->isNormalUser();
    }

    public static function isAuthorizedForStore($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }

        if(!$user->isClientAdmin()) { return false; }

        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId != $user->client_id) { return false; }

        $userIds = \ArrayHelpers::pop($submitData, 'user_ids');

        if($userIds){
            $authorizedUserIds = UserPolicy::authorizedRecordIds($user);

            foreach($userIds as $userId){
                if(!in_array($userId, $authorizedUserIds)){
                    return false;
                }
            }
        }

        return true;
    }

    public static function isAuthorizedForShow($user, $resource){
        if(static::masterCheck($user)) { return true; }
        return in_array($resource->id, static::authorizedRecordIds($user));
    }

    public static function isAuthorizedForEdit($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }
        return in_array($resource->id, static::authorizedRecordIds($user));
    }

    public static function isAuthorizedForUpdate($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }

        if(!in_array($resource->id, static::authorizedRecordIds($user))){
           return false;
        }

        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId != $user->client_id) { return false; }

        $userIds = \ArrayHelpers::pop($submitData, 'user_ids');

        if($userIds){
            $authorizedUserIds = UserPolicy::authorizedRecordIds($user);

            foreach($userIds as $userId){
                if(!in_array($userId, $authorizedUserIds)){
                    return false;
                }
            }
        }

        return true;
    }
}
