<?php

namespace Policies;

use \ArrayHelpers;
use \Policies\ProjectPolicy;

class PagePolicy extends Policy {

	protected static $pageType = 'wiki';

	public static $projectFlags = array(
		'wiki' => 'enable_wiki',
		'intranet' => 'enable_pages',
		'forms' => 'enable_forms'
	);

	public static function setPageType($type)
	{
		self::$pageType = $type;
	}

    public static function getRoot($user){
		return \Page::where('title', '=', '__ROOT__PAGE__')
			->where('client_id', '=', $user->client_id)
			->firstOrFail();
    }

	public static function authorizedRecordsBuilder($user)
	{
        $root = static::getRoot($user);

        $projectFlag = self::$projectFlags[self::$pageType];
        $projectIds = ProjectPolicy::authorizedRecordsBuilder($user)
            ->where($projectFlag, '=', true)
            ->lists('id');

        if (count($projectIds)) {
            $pagesBuilder = \Page::where('lft', '>', $root->lft)
                ->where('rgt', '<', $root->rgt)
                ->with('project')
                ->where('client_id', '=', $user->client_id)
                ->where('type', '=', \Page::$pageTypes[self::$pageType])
                // ->orderBy('updated_at', 'DESC')
                ->orderBy('project_id', 'ASC')
                ->whereIn('project_id', $projectIds);
                // ->where('depth', '=', 1);

            if ($user->isNormalUser()) {
                $pagesBuilder->where('is_public', '=', 0);
            }

        	return $pagesBuilder;
        }

        return \Page::where('id', '=', 0);
	}

    public static function authorizedRecords($user){
        return static::authorizedRecordsBuilder($user)->get();
    }

	public static function isAuthorizedForCreate($user, $resource, $submitData){
        if (self::$pageType === 'wiki' || self::$pageType === 'forms') {
			return true;
		}
		return $user->isClientAdmin() || $user->isAdmin();
    }

    public static function isAuthorizedForStore($user, $resource, $submitData){
        return true;
        if(static::masterCheck($user)) { return true; }

        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId && $clientId != $user->client_id) { return false; }

        $projectId = \ArrayHelpers::retrieve(
            $submitData,
            'project_id'
        );

        if($projectId){
            if(!in_array(
                $projectId, ProjectPolicy::authorizedRecordIds($user)
            )){
                return false;
            }
        }

        $parentPageId = \ArrayHelpers::retrieve(
            $submitData,
            'parent_id'
        );

        if($parentPageId && $parentPageId != static::getRoot($user)->id){
            if(!in_array(
                $parentPageId, PagePolicy::authorizedRecordIds($user)
            )){
                return false;
            }
        }

        return true;
    }

    public static function isAuthorizedForShow($user, $resource){
        return true;
        if(!$user->isNormalUser()) { return true; }

    	if ($resource->type === \Page::$pageTypes['wiki']) {
    		return true;
    	}
        if ($resource->type === \Page::$pageTypes['forms']) {
            return $resource->isInternal();
        }
    }

    public static function isAuthorizedForEdit($user, $resource, $submitData){
        return true;
        if(static::masterCheck($user)) { return true; }
        if(!$user->isClientAdmin()) { return false; }

        return $user->client_id == $resource->client_id;
    }

    public static function isAuthorizedForUpdate($user, $resource, $submitData){
        return true;
        if(static::masterCheck($user)) { return true; }
        if(!$user->isClientAdmin()) { return false; }
        if($user->client_id != $resource->client_id) { return false; }

        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId && $clientId != $user->client_id) { return false; }

        $projectId = \ArrayHelpers::retrieve(
            $submitData,
            'project_id'
        );

        if($projectId){
            if(!in_array(
                $projectId, ProjectPolicy::authorizedRecordIds($user)
            )){
                return false;
            }
        }

        $parentPageId = \ArrayHelpers::retrieve(
            $submitData,
            'parent_id'
        );

        if($parentPageId && $parentPageId != static::getRoot($user)->id){
            if(!in_array(
                $parentPageId, PagePolicy::authorizedRecordIds($user)
            )){
                return false;
            }
        }

        return true;
    }

    public static function isAuthorizedForComment($user, $resource, $submitData) {
        return true;
        if(static::masterCheck($user)) { return true; }
        return $user->client_id == $resource->client_id;
    }

    public static function isAuthorizedForAttachFile($user, $resource, $submitData) {
        return true;
        return static::isAuthorizedForEdit($user, $resource, $submitData);
    }

    public static function isAuthorizedForChunkUploadFinished($user, $resource, $submitData) {
        return true;
        return static::isAuthorizedForEdit($user, $resource, $submitData);
    }

    public static function isAuthorizedForDownload($user, $resource, $submitData) {
        return true;
        if ($resource->page->type === \Page::TYPE_PUBLIC) {
            return true;
        }

        if ($resource->page->type === \Page::TYPE_FORM && $resource->page->isPublic()) {
            return true;
        }

        return $user && ($user->isAdmin() || $user->isClientAdmin());
    }

    public static function isAuthorizedForDeleteFile($user, $resource, $submitData) {
        return true;
        return static::isAuthorizedForEdit($user, $resource->page, $submitData);
    }

}
