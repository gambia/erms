<?php

namespace Policies;

use \ArrayHelpers;

class ProjectCasePolicy extends Policy {

    public static function authorizedRecordsBuilder($user)
    {
        if($user->isAdminOrClientAdmin()){
            $clientProjectIds = \Project::where(
                'client_id', '=', $user->client_id
            )->lists('id');

            if(count($clientProjectIds) > 0){
                return \ProjectCase::whereIn('project_id', $clientProjectIds);
            }

            return \ProjectCase::where('id', '=', 0);
        }

        $projectIds = $user->projectsQueryBuilder()->lists('id');

        if(count($projectIds) > 0){
             return \ProjectCase::whereIn('project_id', $projectIds)
                ->where(function($q1)  use ($user) {
                    $q1->where('is_confidential', '=', 0)
                        ->orWhereIn('id', \DB::table('project_case_users')->where('user_id', '=', $user->id)->lists('project_case_id'));
                });
        }

        return \ProjectCase::where('id', '=', 0);
    }

    public static function authorizedRecordsByProjectIdsBuilder($user, $projectIds){
        $authorizedRecordsBuilder = static::authorizedRecordsBuilder($user);

        if(count($projectIds) > 0){
            return $authorizedRecordsBuilder->whereIn(
                'project_id', $projectIds
            );
        }

        return $authorizedRecordsBuilder;
    }

    public static function authorizedRecordsByProjectIds($user, $projectIds){
        return static::authorizedRecordsByProjectIdsBuilder($user, $projectIds)
            ->with('project', 'assignedTo', 'projectCaseStatus')
            ->orderBy('cases.updated_at', 'desc');
    }

    public static function isAuthorizedForCreate($user, $resource, $submitData){
        return $user->isAdminOrClientAdmin();
    }

    public static function isAuthorizedForStore($user, $resource, $submitData){
        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId && $clientId != $user->client_id) { return false; }

        $projectId = \ArrayHelpers::retrieve($submitData, 'project_id');
        if(!in_array($projectId, ProjectPolicy::authorizedRecordIds($user))){
            return false;
        }

        return $user->isAdminOrClientAdmin();
    }

    public static function isAuthorizedForDelete($user, $resource, $submitData) {
        return self::isAuthorizedForCreate($user, $resource, $submitData);
    }

    public static function isAuthorizedForConfidential($user, $resource)
    {
        return in_array($user->id, $resource->privilegedUsers()->lists('user_id')) || $user->isAdminOrClientAdmin();
    }

    public static function isAuthorizedForShow($user, $resource){

        if ($resource->is_confidential) {
            if (self::isAuthorizedForConfidential($user, $resource)) {
                return true;
            }
            return false;
        }

        return ProjectPolicy::isAuthorizedForShow($user, $resource->project);
    }

    public static function isAuthorizedForEdit($user, $resource, $submitData){
        return self::isAuthorizedForCreate($user, $resource, $submitData);
    }

    public static function isAuthorizedForUpdate($user, $resource, $submitData){
        return self::isAuthorizedForStore($user, $resource, $submitData);
    }

    public static function isAuthorizedForComment($user, $resource, $submitData){
        return ProjectPolicy::isAuthorizedForShow($user, $resource->project);
    }
}
