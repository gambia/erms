<?php

namespace Policies;

class RecordPolicy extends Policy {

    public static function authorizedRecordsBuilder($user){
        $authorizedCaseIds = ProjectCasePolicy::authorizedRecordIds($user);

        if(count($authorizedCaseIds) > 0){
            return \ProjectCaseRecord::whereIn('project_case_id', $authorizedCaseIds);
        }
        else {
            return \ProjectCaseRecord::where('id', '=', 0);
        }
    }

    public static function isAuthorizedForIndex($user, $resource, $submitData){
        return true;
    }

    public static function isAuthorizedForCreate($user, $resource, $submitData){
        return true;
    }

    public static function isAuthorizedForStore($user, $resource, $submitData){
        $caseId = \ArrayHelpers::retrieve(
            $submitData,
            'project_case_id'
        );

        if($caseId){
            if(!in_array(
                $caseId, ProjectCasePolicy::authorizedRecordIds($user)
            )){
                return false;
            }
        }

        return true;
    }

    public static function isAuthorizedForShow($user, $resource){
        return ProjectCasePolicy::isAuthorizedForShow(
            $user, $resource->projectCase
        );
    }

    public static function isAuthorizedForEdit($user, $resource, $submitData){
        return ProjectCasePolicy::isAuthorizedForShow(
            $user, $resource->projectCase
        );
    }

    public static function isAuthorizedForUpdate($user, $resource, $submitData){
        $caseId = \ArrayHelpers::retrieve(
            $submitData,
            'project_case_id'
        );

        if($caseId){
            if(!in_array(
                $caseId, ProjectCasePolicy::authorizedRecordIds($user)
            )){
                return false;
            }
        }

        return true;
    }

    public static function isAuthorizedForAttachFile($user, $resource, $submitData){
        return static::isAuthorizedForEdit($user, $resource, $submitData);
    }

    public static function isAuthorizedForChunkUploadFinished($user, $resource, $submitData){
        return static::isAuthorizedForEdit($user, $resource, $submitData);
    }

    public static function isAuthorizedForDownloadFile($user, $resource, $submitData){
        return static::isAuthorizedForShow($user, $resource->caseRecord, $submitData);
    }

    public static function isAuthorizedForDeleteFile($user, $resource, $submitData){
        return static::isAuthorizedForEdit($user, $resource->caseRecord, $submitData) && $user->isAdminOrClientAdmin();
    }
}
