<?php

namespace Policies;

class ProjectPolicy extends Policy {

    public static function authorizedRecordsBuilder($user){
        return $user->projectsQueryBuilder();
    }

    public static function isAuthorizedForIndex($user, $resource, $submitData){
        return true;
    }

    public static function isAuthorizedForCreate($user, $resource, $submitData){
        return !$user->isNormalUser();
    }

    public static function isAuthorizedForStore($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }

        if(!$user->isClientAdmin()) { return false; }

        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId && $clientId != $user->client_id) { return false; }

        $parentProjectId = \ArrayHelpers::retrieve(
            $submitData,
            'parent_project_id'
        );

        if($parentProjectId){
            if(!in_array(
                $parentProjectId, ProjectPolicy::authorizedRecordIds($user)
            )){
                return false;
            }
        }

        $securityGroupIds = \ArrayHelpers::retrieve(
            $submitData,
            'security_group_ids',
            []
        );

        if(count($securityGroupIds) > 0 && \SecurityGroup::whereIn(
            'id',
            $securityGroupIds
        )->distinct()->lists('client_id') != [$user->client_id]){
            return false;
        }

        $managerId = \ArrayHelpers::retrieve($submitData, 'manager_id');
        if(
            $managerId &&
            \User::findOrFail($managerId)->client_id != $user->client_id
        ){
            return false;
        }

        $managerRepresentativeIds = \ArrayHelpers::retrieve(
            $submitData,
            'manager_representative_ids',
            []
        );

        if(count($managerRepresentativeIds) > 0 && \User::whereIn(
            'id',
            $managerRepresentativeIds
        )->distinct()->lists('client_id') != [$user->client_id]){
            return false;
        }

        return true;
    }

    public static function isAuthorizedForShow($user, $resource){
        if(static::masterCheck($user)) { return true; }

        if($user->isClientAdmin()){
            return $user->client_id == $resource->client_id;
        }

        $count = $user->projectsQueryBuilder()
            ->where('projects.id', '=', $resource->id)
            ->count();

        return $count > 0;
    }

    public static function isAuthorizedForEdit($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }

        if($user->isClientAdmin() || $user->isManagerOrRepresentativeOf($resource)){
            return $user->client_id == $resource->client_id;
        }

        return false;
    }

    public static function isAuthorizedForUpdate($user, $resource, $submitData){
        if(!static::isAuthorizedForEdit($user, $resource, $submitData)){
            return false;
        }

        $clientId = \ArrayHelpers::retrieve($submitData, 'client_id');
        if($clientId && $clientId != $user->client_id) { return false; }

        $parentProjectId = \ArrayHelpers::retrieve(
            $submitData,
            'parent_project_id'
        );


        $validParents = ProjectPolicy::authorizedRecordIds($user);
        $validParents[] = \Project::where('client_id', '=', $user->client_id)->where('depth', '=', 0)->first()->id;

        if($parentProjectId){
            if(!in_array(
                $parentProjectId, $validParents
            )){
                return false;
            }
        }


        $securityGroupIds = \ArrayHelpers::retrieve(
            $submitData,
            'security_group_ids',
            []
        );

        if(count($securityGroupIds) > 0 && \SecurityGroup::whereIn(
            'id',
            $securityGroupIds
        )->distinct()->lists('client_id') != [$user->client_id]){
            return false;
        }

        $managerId = \ArrayHelpers::retrieve($submitData, 'manager_id');
        if(
            $managerId &&
            \User::findOrFail($managerId)->client_id != $user->client_id
        ){
            return false;
        }

        $managerRepresentativeIds = \ArrayHelpers::retrieve(
            $submitData,
            'manager_representative_ids',
            []
        );

        if(count($managerRepresentativeIds) > 0 && \User::whereIn(
            'id',
            $managerRepresentativeIds
        )->distinct()->lists('client_id') != [$user->client_id]){
            return false;
        }

        return true;
    }

    public static function isAuthorizedForDestroy($user, $resource, $submitData){
        if(static::masterCheck($user)) { return true; }
        if(!$user->isClientAdmin()) { return false; }

        return $user->client_id == $resource->client_id;
    }
}
