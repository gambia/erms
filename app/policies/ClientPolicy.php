<?php

namespace Policies;

class ClientPolicy extends Policy {
    public static function authorizedRecordsBuilder($user){
        return static::masterCheck($user) ?
            \Client::where('id', '>', 0) : \Client::where('id', '=', 0);
    }

    public static function isAuthorizedForIndex($user, $resource, $submitData) {
        return static::masterCheck($user);
    }

    public static function isAuthorizedForCreate($user, $resource, $submitData) {
        return static::masterCheck($user);
    }

    public static function isAuthorizedForStore($user, $resource, $submitData) {
        return static::masterCheck($user);
    }

    public static function isAuthorizedForShow($user, $resource) {
        return static::masterCheck($user);
    }

    public static function isAuthorizedForEdit($user, $resource, $submitData) {
        return static::masterCheck($user);
    }

    public static function isAuthorizedForUpdate($user, $resource, $submitData) {
        return static::masterCheck($user);
    }
};
