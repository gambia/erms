<?php

namespace Policies;

use \ArrayHelpers;

class DocumentationPolicy extends Policy {

	/**
	 * Every user is authorized to view documentation pages
	 */
	public static function isAuthorizedForShow($user, $resource)
	{
		return true;
	}

	/**
	 * Every user is authorized to view published documentation pages
	 * Admins are authorized to get all documentation records
	 */
	public static function authorizedRecordsBuilder($user)
	{
		if (static::masterCheck($user)) {
			return \Documentation::where('depth', '>', 0)->orderBy('depth', 'ASC')->orderBy('lft', 'ASC');
		}

		return \Documentation::where('is_published', true)->where('depth', '>', 0)->orderBy('depth', 'ASC')->orderBy('lft', 'ASC');;
	}

}