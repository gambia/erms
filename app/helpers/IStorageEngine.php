<?php

interface IStorageEngine {
    public static function write($folder, $file, $uid);
    public static function getFile($path);
    public static function getFileLink($path);
    public static function read($path);
    public static function delete($path);
};
