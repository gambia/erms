<?php

interface IFullTextSearchEngine {
    public static function search($haystack, $query, $fields = []);
}
