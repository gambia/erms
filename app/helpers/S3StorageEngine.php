<?php

use Aws\S3\S3Client;

class S3StorageEngine implements IStorageEngine{
    const ACL = 'authenticated-read';
    const SIGNATURE = 'v4';

    public static function write($folder, $file, $uid){
        $path = $folder.'/'.$uid;
        $config = static::getConfig();

        $params = [
            'Bucket' => $config['bucket'],
            'Key' => $path,
            'SourceFile' => $file->getRealPath(),
            'ACL' => static::ACL
        ];

        static::getClient($config)->putObject($params);

        return $path;
    }

    public static function getFile($path){
        $config = static::getConfig();

        return static::getClient($config)->getObject(array(
            'Bucket' => $config['bucket'],
            'Key' => $path
        ));
    }

    public static function getFileLink($path){
        $config = static::getConfig();

        return static::getClient($config)->getObjectUrl(
            $config['bucket'],
            $path,
            '+1 minute'
        );
    }

    public static function getFullFileLink($path, $name) {
        $config = static::getConfig();

        return static::getClient($config)->getObjectUrl(
            $config['bucket'],
            $path,
            '+1 minute',
            array(
                'ResponseContentDisposition' => 'attachment; filename="' . $name . '"',
            )
        );
    }

    public static function read($path){
        return static::getFile($path)['Body'];
    }

    public static function delete($path){
        $config = static::getConfig();

        return static::getClient($config)->deleteObject(array(
            'Bucket' => $config['bucket'],
            'Key' => $path
        ));
    }

    public static function getClient($config){
        $client = S3Client::factory(array(
            'key' => $config['key'],
            'secret' => $config['secret'],
            'region' => $config['region'],
            'signature' => static::SIGNATURE
        ));

        return $client;
    }

    public static function getConfig(){
        $config = require(app_path() . '/config/services.php');
        return $config['aws']['s3'];
    }

    public static function getDirectUploadParams($pathToFile = 'securityfolder'){
        $config = static::getConfig();
        $client = static::getClient($config);
        $acl = static::ACL;

        $uid = uniqid();
        $key = $pathToFile ? $pathToFile . '/' . $uid : $uid;

        $postObject = new \Aws\S3\Model\PostObject(
            $client,
            $config['bucket'],
            [
                'acl' => $acl,
                'key' => '^'.$pathToFile
            ]
        );

        $formInputs = $postObject->prepareData()->getFormInputs();
        $formAttributes = $postObject->getFormAttributes();
        $policy = $formInputs['policy'];
        $signature = $formInputs['signature'];

        return [
            'uploadUrl' => $formAttributes['action'],
            'policy' => $policy,
            'signature' => $signature,
            'key' => $key,
            'accessKey' => $config['key'],
            'acl' => $acl,
        ];
    }
};
