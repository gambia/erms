<?php

class LikeFullTextSearchEngine implements IFullTextSearchEngine {
    public static function search($haystack, $query, $fields = []){
        $query = '%' . strtolower($query) . '%';
        $fieldsCount = count($fields);

        if($fieldsCount == 0){
            // Hack to return empty collection
            return $haystack->where('id', '=', 0);
        }

        $queryString = '(';
        $useOr = false;

        foreach($fields as $field){

            if($useOr){
                $queryString = $queryString . ' OR';
            }

            $queryString = $queryString . ' lower(' . $field . ') LIKE ?';
            $useOr = true;
        }

        $queryString = $queryString . ')';

        return $haystack->whereRaw(
            $queryString,
            array_fill(0, $fieldsCount, $query)
        );
    }
}
