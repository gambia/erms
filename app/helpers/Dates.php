<?php
class Dates {
    public static function localizeDateTime($dateTime, $timeZoneString = 'sast'){
        if($timeZoneString === NULL){
            $timeZoneString = 'sast';
        }

        return $dateTime->timezone($timeZoneString);
    }

    public static function timezonesForSelectBox(){
        $timezones = DateTimeZone::listAbbreviations();
        $timezonesForSelectBox = [];

        foreach($timezones as $timezoneKey => $timezoneData){
            $timezonesForSelectBox[$timezoneKey] = $timezoneData[0]['timezone_id'] . ' (' . $timezoneKey . ')';
        }

        return $timezonesForSelectBox;
    }

    public static function showTimeAgo($date) {
        if(empty($date)) {
            return "No date provided";
        }

        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60","60","24","7","4.35","12","10");

        $now = time();
        $unix_date = strtotime($date);

        // check validity of date
        if(empty($unix_date)) {
            return "Bad date";
        }

        // is it future date or past date
        if($now > $unix_date) {
            $difference     = $now - $unix_date;
            $tense         = "ago";
        } else {
            $difference     = $unix_date - $now;
            $tense         = "from now";
        }

        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if($difference != 1) {
            $periods[$j].= "s";
        }

        if($difference != 0) {
            return "$difference $periods[$j] {$tense}";
        } else {
            return "just now";
        }
    }
}
