<?php

class PostgresFullTextSearchEngine implements IFullTextSearchEngine {
    public static function search($haystack, $query, $fields = []){
        $query = str_replace(' ', ' & ', $query);

        return $haystack->whereRaw("searchtext @@ to_tsquery(?)", [$query]);
    }
}
