<?php

use Aws\S3\S3Client;

class LocalStorageEngine implements IStorageEngine{
    const ROOT = 'someroot';

    public static function write($folder, $file, $uid){
        throw new Exception('Not implemented');
    }

    public static function getFile($path){
        throw new Exception('Not implemented');
    }

    public static function getFileLink($path){
        throw new Exception('Not implemented');
    }

    public static function read($path){
        throw new Exception('Not implemented');
    }

    public static function delete($path){
        throw new Exception('Not implemented');
    }
};
