<?php

class MandrillEmailSender {
    public static function send($templateName, $data){
        $config = require(app_path() . '/config/services.php');
        $secret = $config['mandrill']['secret'];
        $mandrill = new Mandrill($secret);

        $to = [];
        $mergeVars = [];

        $subject = \ArrayHelpers::pop($data, 'subject', NULL);

        $emails = array_keys($data);

        if(count($emails) === 0){ return; }

        foreach($emails as $email){
            $dataForEmail = \ArrayHelpers::pop($data, $email, []);
            $recipientName = \ArrayHelpers::pop(
                $dataForEmail,
                'recipientName',
                $email
            );

            array_push($to, [
                'email' => $email,
                'name' => $recipientName
            ]);

            $mergeVarsForEmail = [];
            $keysForEmail = array_keys($dataForEmail);

            foreach($keysForEmail as $key){
                array_push($mergeVarsForEmail, [
                    'name' => $key,
                    'content' => $dataForEmail[$key]
                ]);
            }

            array_push($mergeVars, [
                'rcpt' => $email,
                'vars' => $mergeVarsForEmail
            ]);
        }

        $mandrillData = [
            'to' => $to,
            'merge_vars' => $mergeVars
        ];

        if($subject){
            $mandrillData['subject'] = $subject;
        }

        Log::info('Sending Email: '.$templateName.' with data: '.json_encode(
            $mandrillData
        ));

        try{
            $result = $mandrill->messages->sendTemplate(
                $templateName, [], $mandrillData
            );

            Log::info('Mandrill result: '.json_encode($result));
        }
        catch(Exception $e){
            Log::error('Call to Mandrill failed: ' . get_class($e) . ": ". $e->getMessage());
        }

        //if($result[0]['status'] != 'sent'){
        //    Log::error('Mandrill result: '.json_encode($result));
        //    throw new Exception('Email not sent');
        //}
    }
};
