<?php

class ArrayHelpers {
    public static function retrieve($container, $key, $defaultValue = NULL){
        return isset($container[$key]) ? $container[$key] : $defaultValue;
    }

    public static function pop(&$container, $key, $defaultValue = NULL){
        $value = self::retrieve($container, $key, $defaultValue);

        unset($container[$key]);

        return $value;
    }
};
