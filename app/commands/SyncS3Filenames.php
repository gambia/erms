<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SyncS3Filenames extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:syncs3';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Pushes the filename stored in db into the associated s3 files. Then the filenames will appear in client-only downloads.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $bucket = S3StorageEngine::getConfig()['bucket'];
        $recordFiles = ProjectCaseRecordFile::all();
        $pageFiles = PageFile::all();

        $this->info('I have '.count($recordFiles) . ' record files and ' . count($pageFiles) .  ' page files to do. I had better get started.');

        $retval = null;
        $fails = [];

        foreach($recordFiles as $recordFile){
            $newPath = $recordFile->url . '-' . $recordFile->filename;
            $this->info('Moving: ' . $recordFile->url . ' to ' . $newPath);

            system('aws s3 mv s3://' . $bucket . '/' . $recordFile->url . ' s3://' . $bucket . '/' . $newPath, $retval);

            if($retval == 0){
                $recordFile->url = $newPath;
                $recordFile->save();
            } else {
                array_push($fails, $recordFile->url);
                $this->error($recordFile->url . ' has not been copied.');
            }
        }

        foreach($pageFiles as $pageFile){
            $newPath = $pageFile->url . '-' . $pageFile->filename;
            $this->info('Moving: ' . $pageFile->url . ' to ' . $newPath);

            system('aws s3 mv s3://' . $bucket . '/' . $pageFile->url . ' s3://' . $bucket . '/' . $newPath, $retval);

            $pageFile->url = $newPath;
            $pageFile->save();

            if($retval == 0){
                $pageFile->url = $newPath;
                $pageFile->save();
            } else {
                array_push($fails, $pageFile->url);
                $this->error($pageFile->url . ' has not been copied.');
            }
        }

        if(count($fails) === 0){
            $this->info('All is done. Relax.');
            return;
        }

        $this->error('We have ' . count($fails) . ' fails.');

        foreach($fails as $fail){
            $this->error($fail);
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
