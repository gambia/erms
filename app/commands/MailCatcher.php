<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MailCatcher extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mail:catcher';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$mailString = '';
		while($f = fgets(STDIN)){
		    $mailString .= "$f";
		}

		$mailParser = new ZBateson\MailMimeParser\MailMimeParser();
		$mailParser->parse($mailString);

		Client::create([
            'name' => 'MailCatcher - sent to: ' . $this->argument('recipient'),
            'about' => $mailString,
            'client_key' => 'DEMO',
            'logo_url' => 'http://www.compco.co.sz/images/scc-logo.jpg',

            'billing_address_line_1' => 'Ground Floor, MV Tel Building',
            'billing_address_line_2' => 'Sidwashini',
            'billing_city' => 'Mbabane',
            'billing_state' => '',
            'billing_country' => 'Swaziland',
            'billing_zip' => 'H100',

            'shipping_address_line_1' => 'P.O. Box 1976',
            'shipping_address_line_2' => '',
            'shipping_city' => 'Mbabane',
            'shipping_state' => '',
            'shipping_country' => 'Swaziland',
            'shipping_zip' => 'H100',

            'opening_hours' => 'Monday through Thursday, excluding public holidays<br />
                08:00 - 17:00<br /><br />
                Friday 08.00 - 16.30',

            'phone' => '+268 422 0800 / 0802 / 0822 / 0805',
            'fax' => '+268 422 0807',
            'email' => 'css@k1cms.com',
            'website' => 'www.compco.co.sz/',
            'map_longitude' => '-26.298205',
            'map_latitude' => '31.115926',
            'map_zoom' => '14'
        ]);

		echo 'Done.';
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('recipient', InputArgument::REQUIRED, 'Recipient address.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
