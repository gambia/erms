<?php

class PagesTableSeeder extends Seeder {

    public $pages = [];

    public function createPage($project_id, $title, $description, $parent_id)
    {
        $page = [
            'project_id'    => $project_id,
            'title'         => $title,
            'description'   => $description,
            'parent_id'     => $parent_id,
            'client_id'     => 1
        ];

        return $page;

    }

    public function generatePageArray($project_id, $count, $parent_id = null) {

        for ($i = 0; $i < $count; $i++) {

            $title          = 'Page '. ($i+1);
            $description    = 'Description '. ($i+1);

            $this->pages[] = $this->createPage($project_id, $title, $description, $parent_id);
        }

        return;
    }

    public function run()
    {
        $users = User::all()->lists('id');

        $this->generatePageArray(3, 25);
        $this->generatePageArray(5, 25);
        $this->generatePageArray(8, 25);
        $this->generatePageArray(9, 25);
        $this->generatePageArray(10, 25);
        $this->generatePageArray(11, 25);
        $this->generatePageArray(12, 25);
        $this->generatePageArray(13, 25);
        $this->generatePageArray(14, 25);
        $this->generatePageArray(15, 25);
        $this->generatePageArray(16, 25);
        $this->generatePageArray(17, 25);
        $this->generatePageArray(18, 25);
        $this->generatePageArray(19, 25);
        $this->generatePageArray(20, 25);
        $this->generatePageArray(21, 25);
        $this->generatePageArray(22, 25);
        $this->generatePageArray(23, 25);
        $this->generatePageArray(24, 25);

        $pages = $this->pages;
        $created = 0;
        $createdPages = array();

        $groot = Page::create(array(
            'project_id' => 1,
            'title' => '__ROOT__PAGE__',
            'description' => '__ROOT__PAGE__',
            'client_id' => 1,
            'parent_id' => NULL
        ));

        for ($i = 0; $i < count($pages); $i++) {
            $page = Page::create($pages[$i]);
            if (!isset($createdPages[$page->project_id])) {
                $createdPages[$page->project_id] = [];
            }
            $createdPages[$page->project_id][] = $page;
            $randomizer = rand(0,100);
            if ($created > 10 && $randomizer % 2 === 0) {
                $root = $createdPages[$page->project_id][rand(0, count($createdPages[$page->project_id]) -1)];
                if ($root->id != $page->id) {
                    $page->makeChildOf($root);
                }
            } else {
                $page->makeChildOf($groot);
            }

            $k = rand(0,3);
            $comments = array();
            for ($j = 0; $j<$k; $j++) {
                $c = new PageComment(array('comment' => 'Comment #' .($j+1) . ' for ' . $page->title));
                $c->user_id = $users[rand(1, count($users) -1)];
                $comments[] = $c;
            }

            if (count($comments)) {
                $page->comments()->saveMany($comments);
            }

            $created++;
        }

        Page::create(array(
            'project_id' => 2,
            'title' => '__ROOT__PAGE__',
            'description' => '__ROOT__PAGE__',
            'client_id' => 2,
            'parent_id' => NULL
        ));


    }

}
