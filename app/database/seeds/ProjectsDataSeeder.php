<?php

class ProjectsTableSeeder extends Seeder {

    public $projects = [
        [
            // 1
            'name' => 'Investigation and Litigation',
            'project_key' => 'IAL',
            'summary' => ''
        ],[
            // 2
            'name' => 'Competition Policy and Research',
            'project_key' => 'CPR',
            'summary' => ''
        ],[
            // 3
            'name' => 'Consumer Protection',
            'project_key' => 'CP',
            'summary' => ''
        ],[
            // 4
            'name' => 'Finance and Administration',
            'project_key' => 'FA',
            'summary' => ''
        ],[
            // 5
            'name' => 'Advocacy, Communication and International Relations',
            'project_key' => 'ACIR',
            'summary' => ''
        ],[
            // 6
            'name' => 'Open',
            'project_key' => 'O',
            'summary' => ''
        ],[
            // 7
            'name' => 'Commissioners',
            'project_key' => 'COMM',
            'summary' => ''
        ],[
            // 8
            'name' => 'Partners',
            'project_key' => 'P',
            'summary' => ''
        ],


        // subprojects
        [
            // 9
            'name' => 'Mergers and Acquisitions',
            'project_key' => 'MA',
            'summary' => '',
            'parent_project_id' => 2
        ],[
            // 10
            'name' => 'Cartels & Abuse',
            'project_key' => 'CA',
            'summary' => '',
            'parent_project_id' => 2
        ],[
            // 11
            'name' => 'Other',
            'project_key' => 'OTHER',
            'summary' => '',
            'parent_project_id' => 2
        ],[
            //12
            'name' => 'Research Reports',
            'project_key' => 'RR',
            'summary' => '',
            'parent_project_id' => 3
        ],[
            //13
            'name' => 'Resources',
            'project_key' => 'R',
            'summary' => '',
            'parent_project_id' => 3
        ],[
            //14
            'name' => 'Human Resources',
            'project_key' => 'HR',
            'summary' => '',
            'parent_project_id' => 5
        ],[
            //15
            'name' => 'Finance',
            'project_key' => 'F',
            'summary' => '',
            'parent_project_id' => 5
        ],[
            //16
            'name' => 'General Admin',
            'project_key' => 'FGA',
            'summary' => '',
            'parent_project_id' => 5
        ],[
            //17
            'name' => 'HR Policies and Guidelines',
            'project_key' => 'HRPG',
            'summary' => '',
            'parent_project_id' => 7
        ],[
            //18
            'name' => 'Non-confidential Case Summaries',
            'project_key' => 'NCONF',
            'summary' => '',
            'parent_project_id' => 7
        ],[
            //19
            'name' => 'Research Summaries',
            'project_key' => 'RS',
            'summary' => '',
            'parent_project_id' => 7
        ],[
            //20
            'name' => 'General Admin',
            'project_key' => 'HRGA',
            'summary' => '',
            'parent_project_id' => 7
        ],[
            //21
            'name' => 'Competition Guidelines',
            'project_key' => 'CG',
            'summary' => '',
            'parent_project_id' => 7
        ],[
            //22
            'name' => 'Mergers & Acquisitions',
            'project_key' => 'CMA',
            'summary' => '',
            'parent_project_id' => 8
        ],[
            //23
            'name' => 'Cartels & Abuse',
            'project_key' => 'CCA',
            'summary' => '',
            'parent_project_id' => 8
        ],[
            //24
            'name' => 'Finance & Administration',
            'project_key' => 'CFA',
            'summary' => '',
            'parent_project_id' => 8
        ]
    ];



    public function run()
    {
        $groot = Project::create(array(
            'name' => '__ROOT__PROJECT__',
            'summary' => '__ROOT__PROJECT__',
            'project_key' => 'ROOT-PROJECT',
            'client_id' => 1,
        ));

        $sgs = SecurityGroup::where('client_id', '=', 1);
        foreach ($sgs as $key => $value) {
            ProjectSecurityGroup::create(array(
                'project_id' => $groot->id,
                'security_group_id' => $value->id
            ));
        }

        $projects = $this->projects;

        for ($i = 0; $i < count($projects); $i++)
        {

            $project = $projects[$i];
            $project['client_id'] = 1;

            if (!isset($project['parent_project_id'])) {
                $project['parent_project_id'] = $groot->id;
            }

            $p = Project::create($project);

            $securityGroup = array(
                'security_group_id' => (isset($project['parent_project_id']) && $project['parent_project_id'] != $groot->id) ? $project['parent_project_id']-1 : (($i+1 < 8) ? ($i+1) : $i),
                'project_id' => $p->id
            );

            ProjectSecurityGroup::create($securityGroup);
        };

        Project::create(array(
            'name' => '__ROOT__PROJECT__',
            'summary' => '__ROOT__PROJECT__',
            'project_key' => 'ROOT-PROJECT',
            'client_id' => 2,
        ));

        Project::rebuild();
    }
}
