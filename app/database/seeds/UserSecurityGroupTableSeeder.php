<?php

class UserSecurityGroupTableSeeder extends Seeder {

    public $usersPerGroups = [
        [
            'user_id' => 4,
            'groups' => [1,2,3,4]
        ],[
            'user_id' => 5,
            'groups' => [1,2,3,4]
        ],[
            'user_id' => 6,
            'groups' => [1,2,3,4]
        ],[
            'user_id' => 7,
            'groups' => [1,2,3,4]
        ],[
            'user_id' => 8,
            'groups' => [1,2,3,4]
        ],[
            'user_id' => 9,
            'groups' => [1,2,3,4]
        ],[
            'user_id' => 10,
            'groups' => [1,2,3,4]
        ],[
            'user_id' => 11,
            'groups' => [3,4]
        ],[
            'user_id' => 12,
            'groups' => [3,4,5]
        ],[
            'user_id' => 13,
            'groups' => [3,4]
        ],[
            'user_id' => 14,
            'groups' => [3,4]
        ],[
            'user_id' => 15,
            'groups' => [3,4]
        ],[
            'user_id' => 16,
            'groups' => [3,4,5]
        ],[
            'user_id' => 18,
            'groups' => [2,6,3,4]
        ],[
            'user_id' => 19,
            'groups' => [7]
        ],[
            'user_id' => 20,
            'groups' => [7]
        ],[
            'user_id' => 21,
            'groups' => [7]
        ],[
            'user_id' => 22,
            'groups' => [7]
        ],[
            'user_id' => 23,
            'groups' => [7]
        ],[
            'user_id' => 24,
            'groups' => [7]
        ],[
            'user_id' => 25,
            'groups' => [7]
        ],[
            'user_id' => 26,
            'groups' => [7]
        ]
    ];



    public function run()
    {
        $usersPerGroups = $this->usersPerGroups;

        for ($i = 0; $i < count($usersPerGroups); $i++) {
            $user_id    = $usersPerGroups[$i]['user_id'];
            $groups   = $usersPerGroups[$i]['groups'];

            for ($j = 0; $j < count($groups); $j++) {
                UserSecurityGroup::create(array(
                  'user_id' => $user_id,
                  'security_group_id' => $groups[$j]
                ));
            }
        };
    }

}