<?php

class CasesTableSeeder extends Seeder {

    public $cases = [];

    public function createCase($project_id, $summary, $priority, $status, $assigned_to)
    {
        $case = [
            'project_id'    => $project_id,
            'summary'       => $summary,
            'description'       => $summary,
            'priority'      => $priority,
            'project_case_status_id' => $status,
            'assigned_to'   => $assigned_to,
            'client_id' => 1,
        ];
        return $case;

    }

    public function generateCaseArray($project_id, $count) {

        for ($i = 0; $i < $count; $i++) {

            $summary = 'Case '. ($i+1);
            $priority = rand(1,5);
            $status = rand(1,2);
            $assigned_to = rand(1, 26);

            $this->cases[] = $this->createCase($project_id, $summary, $priority, $status, $assigned_to);
        }

        return;
    }

    public function run()
    {
        $users = User::all()->lists('id');

        $this->generateCaseArray(3, 25);
        $this->generateCaseArray(5, 25);
        $this->generateCaseArray(8, 25);
        $this->generateCaseArray(9, 25);
        $this->generateCaseArray(10, 25);
        $this->generateCaseArray(11, 25);
        $this->generateCaseArray(12, 25);
        $this->generateCaseArray(13, 25);
        $this->generateCaseArray(14, 25);
        $this->generateCaseArray(15, 25);
        $this->generateCaseArray(16, 25);
        $this->generateCaseArray(17, 25);
        $this->generateCaseArray(18, 25);
        $this->generateCaseArray(19, 25);
        $this->generateCaseArray(20, 25);
        $this->generateCaseArray(21, 25);
        $this->generateCaseArray(22, 25);
        $this->generateCaseArray(23, 25);
        $this->generateCaseArray(24, 25);

        $cases = $this->cases;

        for ($i = 0; $i < count($cases); $i++) {
            $case = ProjectCase::create($cases[$i]);

            $k = rand(0,3);
            $comments = array();
            for ($j = 0; $j<$k; $j++) {
                $c = new ProjectCaseComment(array('comment' => 'Comment #' .($j+1) . ' for ' . $case->summary));
                $c->user_id = $users[rand(1, count($users) -1)];
                $comments[] = $c;
            }

            if (count($comments)) {
                $case->comments()->saveMany($comments);
            }
        }

    }

}
