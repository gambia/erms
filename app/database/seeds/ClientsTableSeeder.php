<?php

class ClientsTableSeeder extends Seeder {

    public $clients = [
        [
            'name' => 'Swaziland Competition Commission',
            'about' => 'The Swaziland Competition Commission (SCC) is a statutory body charged with the administration and enforcement of the Swaziland Competition Act of 2007. The mission of the SCC is to strengthen the nation’s economy by promoting and securing robust economic competition and consumer protection. Through our enforcement of competition and consumer protection policies we seek to ensure better product quality, lower prices, and overall, the enhanced welfare of Swaziland Citizens.',
            'client_key' => 'SCC',
            'logo_url' => 'http://www.compco.co.sz/images/scc-logo.jpg',

            'billing_address_line_1' => 'Ground Floor, MV Tel Building',
            'billing_address_line_2' => 'Sidwashini',
            'billing_city' => 'Mbabane',
            'billing_state' => '',
            'billing_country' => 'Swaziland',
            'billing_zip' => 'H100',

            'shipping_address_line_1' => 'P.O. Box 1976',
            'shipping_address_line_2' => '',
            'shipping_city' => 'Mbabane',
            'shipping_state' => '',
            'shipping_country' => 'Swaziland',
            'shipping_zip' => 'H100',

            'opening_hours' => 'Monday through Thursday, excluding public holidays<br />
                08:00 - 17:00<br /><br />
                Friday 08.00 - 16.30',

            'phone' => '+268 422 0800 / 0802 / 0822 / 0805',
            'fax' => '+268 422 0807',
            'email' => 'css@k1cms.com',
            'website' => 'www.compco.co.sz/',
            'map_longitude' => '-26.298205',
            'map_latitude' => '31.115926',
            'map_zoom' => '14'
        ],[
            'name' => 'EDRC GmbH',
            'about' => 'EDRC GmbH is a Munich-based, multi-disciplinary research and consulting firm specializing in economic development and trade-related technical assistance to developing and least- developed countries (DCs/LDCs), with a specific focus on Sub-Saharan Africa, the Caribbean and the Pacific regions. The firm’s overarching thematic focus is policy and economic reforms in African, Caribbean and Pacific (ACP) countries and regions as a platform for addressing new global challenges and contributing to concrete advancement improving the livelihood of ACP citizens. EDRC seeks to address the needs of governments, private sector actors and aid donors on the basis of current academic research, best practices, empirically tailored policy recommendations, and by facilitating knowledge dissemination and effective collaboration among public and private stakeholders. It provides client-specific and culturally sensitive solutions through the utilization of leading experts with recognized commitment and expertise as well as an established record of on-the-ground experience. EDRC\'s unique project implementation approach and project-specific and versatile management structure places particular emphasis on the integration of local stakeholders to facilitate sustainable long-term outcomes.',
            'client_key' => 'EDRC',
            'logo_url' => 'http://www.edrc.eu/images/edrclogo_nb.png',

            'billing_address_line_1' => 'Nymphenburger Strasse 4',
            'billing_address_line_2' => '',
            'billing_city' => 'Munich',
            'billing_state' => '',
            'billing_country' => 'Germany',
            'billing_zip' => '80335',

            'shipping_address_line_1' => 'Nymphenburger Strasse 4',
            'shipping_address_line_2' => '',
            'shipping_city' => 'Munich',
            'shipping_state' => '',
            'shipping_country' => 'Germany',
            'shipping_zip' => '80335',

            'opening_hours' => '',

            'phone' => '+49 89 288 90 489',
            'fax' => '+49 89 288 90 45',
            'email' => 'contact@edrc.eu',
            'website' => 'www.edrc.eu',
            'map_longitude' => '48.148331',
            'map_latitude' => '11.556567',
            'map_zoom' => '14'
        ],[
            'name' => 'Swaziland Competition Commission',
            'about' => 'The Swaziland Competition Commission (SCC) is a statutory body charged with the administration and enforcement of the Swaziland Competition Act of 2007. The mission of the SCC is to strengthen the nation’s economy by promoting and securing robust economic competition and consumer protection. Through our enforcement of competition and consumer protection policies we seek to ensure better product quality, lower prices, and overall, the enhanced welfare of Swaziland Citizens.',
            'client_key' => 'SCC',
            'logo_url' => 'http://www.compco.co.sz/images/scc-logo.jpg',

            'billing_address_line_1' => 'Ground Floor, MV Tel Building',
            'billing_address_line_2' => 'Sidwashini',
            'billing_city' => 'Mbabane',
            'billing_state' => '',
            'billing_country' => 'Swaziland',
            'billing_zip' => 'H100',

            'shipping_address_line_1' => 'P.O. Box 1976',
            'shipping_address_line_2' => '',
            'shipping_city' => 'Mbabane',
            'shipping_state' => '',
            'shipping_country' => 'Swaziland',
            'shipping_zip' => 'H100',

            'opening_hours' => 'Monday through Thursday, excluding public holidays<br />
                08:00 - 17:00<br /><br />
                Friday 08.00 - 16.30',

            'phone' => '+268 422 0800 / 0802 / 0822 / 0805',
            'fax' => '+268 422 0807',
            'email' => 'css@k1cms.com',
            'website' => 'www.compco.co.sz/',
            'map_longitude' => '-26.298205',
            'map_latitude' => '31.115926',
            'map_zoom' => '14'
        ],[
            'name' => 'Swaziland Competition Commission - DEMO',
            'about' => 'The Swaziland Competition Commission (SCC) is a statutory body charged with the administration and enforcement of the Swaziland Competition Act of 2007. The mission of the SCC is to strengthen the nation’s economy by promoting and securing robust economic competition and consumer protection. Through our enforcement of competition and consumer protection policies we seek to ensure better product quality, lower prices, and overall, the enhanced welfare of Swaziland Citizens.',
            'client_key' => 'DEMO',
            'logo_url' => 'http://www.compco.co.sz/images/scc-logo.jpg',

            'billing_address_line_1' => 'Ground Floor, MV Tel Building',
            'billing_address_line_2' => 'Sidwashini',
            'billing_city' => 'Mbabane',
            'billing_state' => '',
            'billing_country' => 'Swaziland',
            'billing_zip' => 'H100',

            'shipping_address_line_1' => 'P.O. Box 1976',
            'shipping_address_line_2' => '',
            'shipping_city' => 'Mbabane',
            'shipping_state' => '',
            'shipping_country' => 'Swaziland',
            'shipping_zip' => 'H100',

            'opening_hours' => 'Monday through Thursday, excluding public holidays<br />
                08:00 - 17:00<br /><br />
                Friday 08.00 - 16.30',

            'phone' => '+268 422 0800 / 0802 / 0822 / 0805',
            'fax' => '+268 422 0807',
            'email' => 'css@k1cms.com',
            'website' => 'www.compco.co.sz/',
            'map_longitude' => '-26.298205',
            'map_latitude' => '31.115926',
            'map_zoom' => '14'
        ]
    ];

    /*
        'about' => '',
        'client_key' => '',
        'logo_url' => '',
        'billing_address_line_1' => '',
        'billing_address_line_2' => '',
        'billing_city' => '',
        'billing_state' => '',
        'billing_country' => '',
        'billing_zip' => '',
        'shipping_address_line_1' => '',
        'shipping_address_line_2' => '',
        'shipping_city' => '',
        'shipping_state' => '',
        'shipping_country' => '',
        'shipping_zip' => '',
        'opening_hours' => '',
        'phone' => '',
        'fax' => '',
        'email' => '',
        'website' => '',
        'map_longitude' => '',
        'map_latitude' => '',
        'map_zoom' => ''
    */

    public function run()
    {
        for ($i = 0; $i < count($this->clients); $i++) {

            $client = Client::create($this->clients[$i]);
            Page::create(array(
                'title' => '__ROOT__PAGE__',
                'description' => '__ROOT__PAGE__',
                'client_id' => $client->id,
            ));
        }
    }
}
