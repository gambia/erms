<?php

class DatabaseSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Eloquent::unguard();

    DB::transaction(function(){
        $this->call('ProductionDataSeeder');
    });
  }

}

class ProductionDataSeeder extends Seeder {

  public function run()
  {
    /**
    CLIENTS
    */

    $this->call('ClientsTableSeeder');
    $this->command->info('Clients table seeded!');

    /**
    USERS
    */

    $this->call('UsersDataSeeder');
    $this->command->info('Users table seeded!');

    /**
    SECURITY GROUPS
    */

    $this->call('SecurityGroupsDataSeeder');
    $this->command->info('SecurityGroup table seeded!');

    /**
    USERS PER SECURITY GROUPS
    */

    $this->call('UserSecurityGroupTableSeeder');
    $this->command->info('UserSecurityGroup table seeded!');

    /**
    PROJECTS
    **/
    $this->call('ProjectsTableSeeder');
    $this->command->info('Projects table seeded!');

    $projectManagers = [
        [
            // 1
            'project_key' => 'IAL',
            'user_id' => 3,
        ],[
            // 2
            'project_key' => 'CPR',
            'user_id' => 2
        ],[
            // 3
            'project_key' => 'CP',
            'user_id' => 18
        ],[
            // 4
            'project_key' => 'FA',
            'user_id' => 16
        ],[
            // 5
            'project_key' => 'ACIR',
            'user_id' => 11
        ],[
            // 6
            'project_key' => 'O',
            'user_id' => 11,
            'managers' => 15
        ],[
            // 7
            'project_key' => 'COMM',
            'user_id' => 1,
            'managers' => 13
        ],[
            // 8
            'project_key' => 'P',
            'user_id' => 3
        ],

        // subprojects
        [
            // 9
            'project_key' => 'MA',
            'user_id' => 4
        ],[
            // 10
            'project_key' => 'CA',
            'user_id' => 5
        ],[
            // 11
            'project_key' => 'OTHER',
            'user_id' => 5
        ],[
            //12
            'project_key' => 'RR',
            'user_id' => 2
        ],[
            //13
            'project_key' => 'R',
            'user_id' => 2
        ],[
            //14
            'project_key' => 'HR',
            'user_id' => 12
        ],[
            //15
            'project_key' => 'F',
            'user_id' => 14
        ],[
            //16
            'project_key' => 'FGA',
            'user_id' => 14
        ],[
            //17
            'project_key' => 'HRPG',
            'user_id' => 11,
            'managers' => 15
        ],[
            //18
            'project_key' => 'NCONF',
            'user_id' => 11,
            'managers' => 15
        ],[
            //19
            'project_key' => 'RS',
            'user_id' => 11,
            'managers' => 15
        ],[
            //20
            'project_key' => 'HRGA',
            'user_id' => 11,
            'managers' => 15
        ],[
            //21
            'project_key' => 'CG',
            'user_id' => 11,
            'managers' => 15
        ],[
            //22
            'project_key' => 'CMA',
            'user_id' => 1,
            'managers' => 13
        ],[
            //23
            'project_key' => 'CCA',
            'user_id' => 1,
            'managers' => 13
        ],[
            //24
            'project_key' => 'CFA',
            'user_id' => 1,
            'managers' => 13
        ]
    ];

    for ($i = 0; $i < count($projectManagers); $i++) {

        $item = [];
        $project = Project::where('project_key', '=', $projectManagers[$i]['project_key'])->first();

        $main = array(
            'project_id' => $project->id,
            'user_id' => $projectManagers[$i]['user_id'],
            'is_main_manager' => true,
        );
        ProjectManager::create($main);

        if (isset($projectManagers[$i]['managers'])) {
            $manager = array(
                'project_id' => $project->id,
                'user_id' => $projectManagers[$i]['managers'],
                'is_main_manager' => false,
            );
            ProjectManager::create($manager);
        }
    };

    $this->command->info('Project managers seeded!');

  }
}
