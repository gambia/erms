<?php

class PerfTestDataSeeder extends Seeder {
    public function run(){
        Eloquent::unguard();

        $masterCount = 1000;

        $clientsCount = $masterCount;
        $usersCount = $masterCount;
        $projectsCount = $masterCount;
        $casesCount = $masterCount;
        $recordsCount = 50000;
        $pagesCount = $masterCount;

        $infoThreshold = 100;

        $clientId = null;
        $securityGroupId = null;

        for($i = 0; $i < $clientsCount; $i++){
            $client = new Client();

            $client->name = 'Swaziland Competition Commission ' . $i;
            $client->about = 'The Swaziland Competition Commission (SCC) is a statutory body charged with the administration and enforcement of the Swaziland Competition Act of 2007. The mission of the SCC is to strengthen the nation’s economy by promoting and securing robust economic competition and consumer protection. Through our enforcement of competition and consumer protection policies we seek to ensure better product quality, lower prices, and overall, the enhanced welfare of Swaziland Citizens. ' . $i;
            $client->client_key = 'SCC-'.$i;
            $client->logo_url = 'http://www.compco.co.sz/images/scc-logo.jpg'.$i;

            $client->billing_address_line_1 = 'Ground Floor, MV Tel Building';
            $client->billing_address_line_2 = 'Sidwashini';
            $client->billing_city = 'Mbabane';
            $client->billing_state = '';
            $client->billing_country = 'Swaziland';
            $client->billing_zip = 'H100';

            $client->shipping_address_line_1 = 'P.O. Box 1976';
            $client->shipping_address_line_2 = '';
            $client->shipping_city = 'Mbabane';
            $client->shipping_state = '';
            $client->shipping_country = 'Swaziland';
            $client->shipping_zip = 'H100';

            $client->opening_hours = 'Monday through Thursday, excluding public holidays<br />
            08:00 - 17:00<br /><br />
            Friday 08.00 - 16.30';

            $client->phone = '+268 422 0800 / 0802 / 0822 / 0805';
            $client->fax = '+268 422 0807';
            $client->email = 'css@k1cms.com';
            $client->website = 'www.compco.co.sz/';
            $client->map_longitude = '-26.298205';
            $client->map_latitude = '31.115926';
            $client->map_zoom = '14';

            $client->save();

            if(!$clientId){ $clientId = $client->id; }

            $securityGroup = new SecurityGroup();

            $securityGroup->name = 'SomeSecurityGroupForClient'.$i;
            $securityGroup->client_id = $client->id;

            $securityGroup->save();

            if(!$securityGroupId) { $securityGroupId = $securityGroup->id; }

            if($i % $infoThreshold == 0){
                $this->command->info('Created Client: ' . $i);
            }
        }

        $this->command->info('Clients and security groups seeded!');

        $userId = null;

        for($iUser = 0; $iUser < $usersCount; $iUser++){
            $user = new User();

            $user->firstname = 'Test'.$iUser;
            $user->lastname = 'User'.$iUser;
            $user->job_title = 'testuser'.$iUser;
            $user->email = 'test'.$iUser.'@user.com';
            $user->admin_level = 1;

            $user->password = Hash::make('password');
            $user->perishable_token = Hash::make('password');

            $user->client_id = $clientId;

            $user->save();

            if(!$userId){ $userId = $user->id; }

            if($iUser % $infoThreshold == 0){
                $this->command->info('Created User: ' . $iUser);
            }
        }

        $this->command->info('Users seeded!');

        $rootProject = Project::create(array(
            'name' => '__ROOT__PROJECT__',
            'summary' => '__ROOT__PROJECT__',
            'project_key' => 'ROOT-PROJECT',
            'client_id' => $clientId,
        ));

        $projectId = null;

        for($iProject = 0; $iProject < $projectsCount; $iProject++){
            $project = new Project();

            $project->name = 'ProjectName'.$iProject;
            $project->project_key = 'KEY'.$iProject;
            $project->summary = 'ProjectSummary-'.$iProject;
            $project->parent_project_id = $rootProject->id;
            $project->client_id = $clientId;

            $project->save();

            if(!$projectId) { $projectId = $project->id; }

            $projectSecurityGroup = new ProjectSecurityGroup();
            $projectSecurityGroup->project_id = $project->id;
            $projectSecurityGroup->security_group_id = $securityGroupId;
            $projectSecurityGroup->save();

            if($iProject % $infoThreshold == 0){
                $this->command->info('Created Project: ' . $iProject);
            }
        }

        Project::rebuild();

        $this->command->info('Projects seeded!');

        $caseStatus = new ProjectCaseStatus();

        $caseStatus->id = ProjectCaseStatus::STATUS_OPEN;
        $caseStatus->name = 'open';
        $caseStatus->client_id = $clientId;

        $caseStatus->save();

        $caseId = null;

        for($iCase = 0; $iCase < $casesCount; $iCase++){
            $case = new ProjectCase();

            $case->project_id = $projectId;
            $case->summary = 'CaseSummary'.$iCase;
            $case->description = 'CaseDescription'.$iCase;
            $case->priority = 0;
            $case->project_case_status_id = ProjectCaseStatus::STATUS_OPEN;
            $case->assigned_to = $userId;
            $case->client_id = $clientId;

            $case->save();

            if(!$caseId) { $caseId = $case->id; }

            if($iCase % $infoThreshold == 0){
                $this->command->info('Created Case: ' . $iCase);
            }
        }

        $this->command->info('Cases seeded!');

        $recordId = null;

        for($iRecord = 0; $iRecord < $recordsCount; $iRecord++){
            $record = new ProjectCaseRecord();

            $record->title = 'RecordTitle'.$iRecord;
            $record->description = 'RecordDescription'.$iRecord;
            $record->project_case_id = $caseId;
            $record->user_id = $userId;

            $record->save();

            if(!$recordId) { $recordId = $record->id; }

            if($iRecord % $infoThreshold == 0){
                $this->command->info('Created Record: ' . $iRecord);
            }
        }

        $this->command->info('Records seeded!');

        $pageRoot = Page::create(array(
            'project_id' => $projectId,
            'title' => '__ROOT__PAGE__',
            'description' => '__ROOT__PAGE__',
            'client_id' => $clientId
        ));

        $pageId = null;

        for($iPage = 0; $iPage < $pagesCount; $iPage++){
            $page = new Page();

            $page->project_id = $projectId;
            $page->title = 'PageTitle'.$iPage;
            $page->description = 'PageDescription'.$iPage;
            $page->parent_id = $pageRoot->id;
            $page->client_id = $clientId;

            $page->save();

            if(!$pageId) { $pageId = $page->id; }

            if($iPage % $infoThreshold == 0){
                $this->command->info('Created Page: ' . $iPage);
            }
        }

        $this->command->info('Pages seeded!');
    }
}
