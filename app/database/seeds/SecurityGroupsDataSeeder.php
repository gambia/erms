<?php

class SecurityGroupsDataSeeder extends Seeder {

    public $securityGroups = [
        ['name' => 'IAL'],
        ['name' => 'CPR'],
        ['name' => 'ACIR'],
        ['name' => 'O'],
        ['name' => 'FA'],
        ['name' => 'CP'],
        ['name' => 'COMM']
    ];

    public function run()
    {
        $securityGroups = $this->securityGroups;
        $swaziland  = Client::find('1');

        for ($i = 0; $i < count($securityGroups); $i++) {
            $securityGroup = $securityGroups[$i];

            $securityGroup['client_id'] = $swaziland->id;

            SecurityGroup::create($securityGroup);
        };
    }

}