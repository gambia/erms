<?php

class TestDataSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        DB::transaction(function(){
            $john = User::create(
                array(
                    'name' => 'John Doe',
                    'job_title' => 'CTO',
                    'email' => 'john.doe@gmail.com',
                    'password' => Hash::make('password'),
                    'perishable_token' => Hash::make('token'),
                    'admin_level' => User::ADMIN
                )
            );

            $client = Client::create(array(
                'name' => 'Swaziland Competition Commission'
            ));

            $anotherClient = Client::create(array(
                'name' => 'Test client'
            ));

            $testUser = User::create(array(
                'name' => 'Test User',
                'client_id' => $anotherClient->id,
                'job_title' => 'Test user',
                'email' => 'test.user@seznam.sk',
                'password' => Hash::make('password'),
                'perishable_token' => Hash::make('token')
            ));

            $piratas = SecurityGroup::create(array(
                'name' => 'Foo',
                'client_id' => $anotherClient->id
            ));

            $janko = User::create(array(
                'name' => 'Janko Hrasko',
                'client_id' => $client->id,
                'job_title' => 'CEO',
                'email' => 'janko.hrasko@seznam.sk',
                'password' => Hash::make('password'),
                'perishable_token' => Hash::make('token')
            ));

            $martinko = User::create(array(
                'name' => 'Martinko Hrasko',
                'client_id' => $client->id,
                'job_title' => 'Dictator',
                'email' => 'martinko.hrasko@seznam.sk',
                'password' => Hash::make('password'),
                'perishable_token' => Hash::make('token')
            ));

            $jane = User::create(array(
                'name' => 'Jane Doe',
                'client_id' => $client->id,
                'job_title' => 'CTO',
                'email' => 'jane.doe@gmail.com',
                'password' => Hash::make('password'),
                'perishable_token' => Hash::make('token'),
                'admin_level' => User::SYSADMIN
            ));

            $piratas = SecurityGroup::create(array(
                'name' => 'Piratas',
                'client_id' => $client->id
            ));

            $lamas = SecurityGroup::create(array(
                'name' => 'Lamas',
                'client_id' => $client->id
            ));

            UserSecurityGroup::create(array(
                'user_id' => $janko['id'],
                'security_group_id' => $lamas['id']
            ));

            UserSecurityGroup::create(array(
                'user_id' => $jane['id'],
                'security_group_id' => $piratas['id']
            ));

            $p1 = Project::create(array(
                'name' => 'Top Secret Project',
                'client_id' => $client->id,
                'project_key' => 'TS',
                'summary' => 'We are building Ironman. Not really. Maybe. It\'s classified.',
            ));

            $pm1 = ProjectManager::create(array(
                'project_id' => $p1->id,
                'user_id' => $janko->id,
                'is_main_manager' => true
            ));

            $p1_1 = Project::create(array(
                'name' => 'Top Secret Subproject',
                'client_id' => $client->id,
                'project_key' => 'TSSUB',
                'summary' => 'Special project for the Ironman\'s torso',
                'parent_project_id' => $p1->id
            ));

            $pm2 = ProjectManager::create(array(
                'project_id' => $p1_1->id,
                'user_id' => $janko->id,
                'is_main_manager' => true
            ));

            $p2 = Project::create(array(
                'name' => 'Captain America',
                'client_id' => $client->id,
                'project_key' => 'CPTM',
                'summary' => 'In case the Ironman project fails, we need to move on to this.',
            ));

            $pm3 = ProjectManager::create(array(
                'project_id' => $p2->id,
                'user_id' => $janko->id,
                'is_main_manager' => true
            ));

            $pm4 = ProjectManager::create(array(
                'project_id' => $p2->id,
                'user_id' => $martinko->id,
            ));

            ProjectSecurityGroup::create(array(
                'project_id' => $p1->id,
                'security_group_id' => $piratas->id
            ));

            ProjectSecurityGroup::create(array(
                'project_id' => $p2->id,
                'security_group_id' => $piratas->id
            ));

            ProjectSecurityGroup::create(array(
                'project_id' => $p2->id,
                'security_group_id' => $lamas->id
            ));
        });
    }
};
