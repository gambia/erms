<?php

class UsersDataSeeder extends Seeder {

    public $users = [
        [
            // 1
            'firstname' => 'Thabisile',
            'lastname' => 'Langa',
            'job_title' => 'CEO',
            'email' => 'test1@k1cms.com',
            'admin_level' => User::CLIENT_ADMIN
        ],[
            // 2
            'firstname' => 'Sebenzile',
            'lastname' => 'Dlamini',
            'job_title' => 'Chief Economist',
            'email' => 'test2@k1cms.com',
            'admin_level' => User::CLIENT_ADMIN
        ],[
            // 3
            'firstname' => 'Njabulo',
            'lastname' => 'Dlamini',
            'job_title' => 'CILC',
            'email' => 'test3@k1cms.com',
            'admin_level' => User::CLIENT_ADMIN
        ],[
            // 4
            'firstname' => 'Phesheya',
            'lastname' => 'Malaza',
            'job_title' => 'Manager-Mergers & Acquisitions',
            'email' => 'test4@k1cms.com'
        ],[
            // 5
            'firstname' => 'Siphesihle',
            'lastname' => 'Hlanze',
            'job_title' => 'Manager-Cartels & Enforcement',
            'email' => 'test5@k1cms.com'
        ],[
            // 6
            'firstname' => 'Siboniselizulu',
            'lastname' => 'Maseko',
            'job_title' => 'Senior Analyst-Mergers',
            'email' => 'test6@k1cms.com'
        ],[
            // 7
            'firstname' => 'Thembelihle',
            'lastname' => 'Dube',
            'job_title' => 'Senior Analyst-Enforcement',
            'email' => 'test7@k1cms.com'
        ],[
            // 8
            'firstname' => 'Thobisa',
            'lastname' => 'Simelane',
            'job_title' => 'Junior Analyst',
            'email' => 'test8@k1cms.com'
        ],[
            // 9
            'firstname' => 'Siphe-Okuhle',
            'lastname' => 'Fakudze',
            'job_title' => 'Research Officer',
            'email' => 'test9@k1cms.com'
        ],[
            // 10
            'firstname' => 'Nkosingiphile',
            'lastname' => 'Ngwenya',
            'job_title' => 'Trainee',
            'email' => 'test10@k1cms.com'
        ],[
            // 11
            'firstname' => 'Mancoba',
            'lastname' => 'Mabuza',
            'job_title' => 'Advocacy & Communications Officer',
            'email' => 'test11@k1cms.com'
        ],[
            // 12
            'firstname' => 'Barbara',
            'lastname' => 'Mabuza',
            'job_title' => 'Human Resources Officer',
            'email' => 'test12@k1cms.com'
        ],[
            // 13
            'firstname' => 'Silindile',
            'lastname' => 'Chipa',
            'job_title' => 'Personal Assistant to CEO',
            'email' => 'test13@k1cms.com'
        ],[
            // 14
            'firstname' => 'Lindiwe',
            'lastname' => 'Kunene',
            'job_title' => 'Accountant',
            'email' => 'test14@k1cms.com'
        ],[
            // 15
            'firstname' => 'Sikumbuzo',
            'lastname' => 'Dube',
            'job_title' => 'Secretary',
            'email' => 'test15@k1cms.com'
        ],[
            // 16
            'firstname' => 'To Be',
            'lastname' => 'Hired',
            'job_title' => 'CFO',
            'email' => 'test16@k1cms.com'
        ],[
            // 17
            'firstname' => 'To Be',
            'lastname' => 'Hired',
            'job_title' => 'Registrar',
            'email' => 'test17@k1cms.com',
            'admin_level' => User::CLIENT_ADMIN
        ],[
            // 18
            'firstname' => 'To Be',
            'lastname' => 'Hired',
            'job_title' => 'Director-Legal Services',
            'email' => 'test18@k1cms.com'
        ],[
            // 19
            'firstname' => 'Nkonzo',
            'lastname' => 'Hlatshwayo',
            'job_title' => 'Commissioner',
            'email' => 'test19@k1cms.com'
        ],[
            // 20
            'firstname' => 'Primrose',
            'lastname' => 'Dlamini',
            'job_title' => 'Commissioner',
            'email' => 'test20@k1cms.com'
        ],[
            // 21
            'firstname' => 'Patrick',
            'lastname' => 'Mnisi',
            'job_title' => 'Commissioner',
            'email' => 'test21@k1cms.com'
        ],[
            // 22
            'firstname' => 'Walter',
            'lastname' => 'Matsebula',
            'job_title' => 'Commissioner',
            'email' => 'test22@k1cms.com'
        ],[
            // 23
            'firstname' => 'Portia',
            'lastname' => 'Dlamini',
            'job_title' => 'Commissioner',
            'email' => 'test23@k1cms.com'
        ],[
            // 24
            'firstname' => 'Sibusiso',
            'lastname' => 'Motsa',
            'job_title' => 'Commissioner',
            'email' => 'test24@k1cms.com'
        ],[
            // 25
            'firstname' => 'Bongani',
            'lastname' => 'Mtshali',
            'job_title' => 'Commissioner',
            'email' => 'test25@k1cms.com'
        ],[
            // 26
            'firstname' => 'Mabandla',
            'lastname' => 'Manzini',
            'job_title' => 'Commissioner',
            'email' => 'test26@k1cms.com'
        ],[ // 27
            'firstname' => 'Tomas',
            'lastname' => 'Felcman',
            'job_title' => 'CEO - EDRC GmbH',
            'email' => 'tomas.felcman@gmail.com',
            'admin_level' => User::CLIENT_ADMIN
        ],[
            // 27
            'firstname' => 'Milan',
            'lastname' => 'Sedliak',
            'job_title' => 'Senior Dev Manager',
            'email' => 'sedliak.milan@gmail.com',
            'admin_level' => User::ADMIN
        ],[
            // 28
            'firstname' => 'Jane',
            'lastname' => 'Doe',
            'job_title' => 'Fake Account',
            'email' => 'jane.doe@gmail.com',
            'admin_level' => User::ADMIN
        ],[
            'firstname' => 'John',
            'lastname' => 'Doe',
            'job_title' => 'Fake Account',
            'email' => 'john.doe@gmail.com',
            'admin_level' => User::CLIENT_ADMIN
        ],[
            'firstname' => 'Tomas',
            'lastname' => 'Voslar',
            'job_title' => 'SE',
            'email' => 't.voslar@gmail.com',
            'admin_level' => User::ADMIN
        ]
    ];

    public function run()
    {
        $swaziland  = Client::find('1');
        $edrc       = Client::find('2');

        $users      = $this->users;

        for ($i = 0; $i < count($users); $i++) {
            $user = $users[$i];
            $token = 'token'.$i;

            $user['password'] = Hash::make('password');
            $user['perishable_token'] = Hash::make($token);

            if ($i < 27) {
                $user['client_id'] = $swaziland->id;
            } else {
                $user['client_id'] = $edrc->id;
            }

            User::create($user);
        };
    }

}
