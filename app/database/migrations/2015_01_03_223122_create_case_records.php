<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaseRecords extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_records', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description');

			$table->integer('project_case_id')->unsigned();
            $table->foreign('project_case_id')->references('id')->on('cases')
                ->onDelete('cascade');

			$table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

			$table->timestamps();
		});

		Schema::create('case_record_files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('filename');
			$table->text('url')->nullable();

			$table->integer('project_case_record_id')->unsigned();
            $table->foreign('project_case_record_id')->references('id')->on('case_records')
                ->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

			$table->string('mimeType');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_record_files');
		Schema::drop('case_records');
	}

}
