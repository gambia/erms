<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCommnentsColumnType extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (DB::getDriverName() === 'mysql') {
            DB::statement('ALTER TABLE history MODIFY COLUMN ref_content TEXT');
            DB::statement('ALTER TABLE history MODIFY COLUMN target_content TEXT');

        } elseif (DB::getDriverName() === 'pgsql') {
            DB::statement('ALTER TABLE history ALTER COLUMN ref_content TYPE TEXT');
            DB::statement('ALTER TABLE history ALTER COLUMN target_content TYPE TEXT');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (DB::getDriverName() === 'mysql') {
            DB::statement('ALTER TABLE history MODIFY COLUMN ref_content VARCHAR(255)');
            DB::statement('ALTER TABLE history MODIFY COLUMN target_content VARCHAR(255)');
        } elseif (DB::getDriverName() === 'pgsql') {
            DB::statement('ALTER TABLE history ALTER COLUMN ref_content TYPE VARCHAR(255)');
            DB::statement('ALTER TABLE history ALTER COLUMN target_content TYPE VARCHAR(255)');
        }
    }

}
