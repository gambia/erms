<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('users', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')
                ->onDelete('cascade');

            $table->string('firstname');
            $table->string('lastname');
            $table->string('job_title');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('is_blocked')->default(false);
            $table->smallInteger('admin_level')->default(0);
            $table->string('avatar_uri')->nullable();
            $table->string('perishable_token')->unique();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::drop('users');
    }

}
