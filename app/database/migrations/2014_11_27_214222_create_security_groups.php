<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecurityGroups extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
    */
    public function up()
    {
        Schema::create('security_groups', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')
            ->onDelete('cascade');

            $table->string('name');
            $table->timestamps();
        });
    }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::drop('security_groups');
    }

}