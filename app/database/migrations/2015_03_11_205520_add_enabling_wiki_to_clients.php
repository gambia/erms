<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnablingWikiToClients extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('clients', function(Blueprint $table)
        {
            $table->boolean('enable_intranet')->default(1);
            $table->boolean('enable_wiki')->default(1);
            $table->boolean('enable_forms')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function(Blueprint $table)
        {
            $table->dropColumn('enable_intranet');
            $table->dropColumn('enable_wiki');
            $table->dropColumn('enable_forms');
        });
    }

}