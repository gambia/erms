<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->text('summary');
            $table->string('project_key');

            $table->integer('parent_project_id')->unsigned()->nullable()->index();
            $table->foreign('parent_project_id')->references('id')->on('projects')
                ->onDelete('cascade');

            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable()->index();
            $table->integer('depth')->nullable();

            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('project_manager', function(Blueprint $table2)
        {
            $table2->integer('project_id')->unsigned();
            $table2->foreign('project_id')->references('id')->on('projects')
                ->onDelete('cascade');

            $table2->integer('user_id')->unsigned();
            $table2->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table2->boolean('is_main_manager')->default(false);
        });

        Schema::create('project_security_groups', function(Blueprint $table3)
        {
            $table3->integer('project_id')->unsigned();
            $table3->foreign('project_id')->references('id')->on('projects')
                ->onDelete('cascade');

            $table3->integer('security_group_id')->unsigned();
            $table3->foreign('security_group_id')->references('id')->on('security_groups')
                ->onDelete('cascade');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_manager');
        Schema::drop('project_security_groups');
        Schema::drop('projects');
    }

}
