<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdStringToHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('history', function(Blueprint $table)
		{
			if (DB::getDriverName() === 'mysql') {
				DB::statement('ALTER TABLE history MODIFY target_id VARCHAR(255)');
				DB::statement('ALTER TABLE history MODIFY ref_id VARCHAR(255)');
			} elseif (DB::getDriverName() === 'pgsql') {
				DB::statement('ALTER TABLE history ALTER COLUMN target_id TYPE CHARACTER VARYING(255)');
				DB::statement('ALTER TABLE history ALTER COLUMN ref_id TYPE CHARACTER VARYING(255)');
			}
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('history', function(Blueprint $table)
		{
			DB::statement('DELETE FROM history WHERE action IN (400, 401, 402, 403, 500, 501, 504, 505)');

			if (DB::getDriverName() === 'mysql') {
				DB::statement('ALTER TABLE history MODIFY target_id INTEGER(11)');
				DB::statement('ALTER TABLE history MODIFY ref_id INTEGER(11)');
			} elseif (DB::getDriverName() === 'pgsql') {
				DB::statement('ALTER TABLE history ALTER COLUMN target_id TYPE numeric(10,0) USING target_id::numeric');
				DB::statement('ALTER TABLE history ALTER COLUMN ref_id TYPE numeric(10,0) USING ref_id::numeric');
			}
		});
	}

}
