<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagsToProjects extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->dropColumn('is_public');

			$table->boolean('enable_cases')->default(1);
			$table->boolean('enable_wiki')->default(1);
			$table->boolean('enable_pages')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->boolean('is_public')->default(0);

			$table->dropColumn('enable_cases');
			$table->dropColumn('enable_wiki');
			$table->dropColumn('enable_pages');
		});
	}

}
