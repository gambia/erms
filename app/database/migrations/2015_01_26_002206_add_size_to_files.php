<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSizeToFiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('case_record_files', function(Blueprint $table)
		{
			$table->integer('size')->nullable();
		});

		Schema::table('page_files', function(Blueprint $table)
		{
			$table->integer('size')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('case_record_files', function(Blueprint $table)
		{
			$table->dropColumn('size');
		});

		Schema::table('page_files', function(Blueprint $table)
		{
			$table->dropColumn('size');
		});
	}

}
