<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documentation', function(Blueprint $table)
		{
			$table->increments('id');

			$table->text('title');
			$table->longText('description');
			$table->boolean('is_published');
			$table->string('slug')->nullable();

            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable()->index();
            $table->integer('depth')->nullable();

            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('documentation')
                ->onDelete('cascade');

			$table->timestamps();
		});

		Schema::create('documentation_files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('filename');
			$table->text('url')->nullable();

			$table->integer('documentation_id')->unsigned();
            $table->foreign('documentation_id')->references('id')->on('documentation')
                ->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

			$table->string('mimeType');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documentation');
		Schema::drop('documentation_files');
	}

}
