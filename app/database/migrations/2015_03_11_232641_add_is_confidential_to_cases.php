<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsConfidentialToCases extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        //
        Schema::table('cases', function(Blueprint $table)
        {
            $table->boolean('is_confidential')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cases', function(Blueprint $table)
        {
            $table->dropColumn('is_confidential');
        });
    }

}
