<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('action');
            $table->integer('target_id')->nullable();
            $table->string('target_class')->nullable();
            $table->string('target_content')->nullable();
            $table->integer('ref_id')->nullable();
            $table->string('ref_class')->nullable();
            $table->string('ref_content')->nullable();

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->string('user_agent')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('additional_data')->nullable();
            $table->timestamps();

            $table->index('action');
            $table->index('created_at');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('history');
    }

}
