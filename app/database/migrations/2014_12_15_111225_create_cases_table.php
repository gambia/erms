<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_status', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('name');

            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')->on('clients')
                ->onDelete('cascade');
        });

        Schema::create('cases', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');

            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')
                ->onDelete('cascade');

            $table->text('summary');

            $table->longText('description');
            $table->string('shortcut')->nullable();
            $table->index('shortcut');
            $table->unique( array('shortcut','project_id') );

            $table->integer('priority');
            $table->integer('project_case_status_id')->unsigned();
            $table->foreign('project_case_status_id')->references('id')->on('case_status');

            $table->integer('assigned_to')->unsigned();
            $table->foreign('assigned_to')->references('id')->on('users');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cases');
        Schema::drop('case_status');
    }

}
