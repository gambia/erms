<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClients extends Migration {

    public function up()
    {
        Schema::create('clients', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->text('about');

            $table->string('client_key');
            $table->string('logo_url');
            $table->string('opening_hours');

            $table->string('billing_address_line_1');
            $table->string('billing_address_line_2');
            $table->string('billing_city');
            $table->string('billing_state');
            $table->string('billing_country');
            $table->string('billing_zip');

            $table->string('shipping_address_line_1');
            $table->string('shipping_address_line_2');
            $table->string('shipping_city');
            $table->string('shipping_state');
            $table->string('shipping_country');
            $table->string('shipping_zip');

            $table->string('phone');
            $table->string('fax');
            $table->string('email');
            $table->string('website');

            $table->string('map_longitude');
            $table->string('map_latitude');
            $table->string('map_zoom');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('clients', function(Blueprint $table)
        {
            Schema::drop('clients');
        });
    }

}