<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFulltextIndexes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (DB::getDriverName() === 'mysql') {
			DB::statement('ALTER TABLE projects ADD FULLTEXT search(name, summary, project_key)');
			DB::statement('ALTER TABLE cases ADD FULLTEXT search(summary, description, shortcut)');
			DB::statement('ALTER TABLE pages ADD FULLTEXT search(title, description)');
			DB::statement('ALTER TABLE case_records ADD FULLTEXT search(title, description)');
			DB::statement('ALTER TABLE users ADD FULLTEXT search(firstname, lastname)');
		}

		if (DB::getDriverName() === 'pgsql') {
			DB::statement('ALTER TABLE projects ADD COLUMN searchtext TSVECTOR;');
			DB::statement("UPDATE projects SET searchtext = to_tsvector('english', name || '' || summary || '' || project_key)");
			DB::statement('CREATE INDEX searchtext_gin_projects ON projects USING GIN(searchtext)');
			DB::statement("CREATE TRIGGER ts_searchtext_projects BEFORE INSERT OR UPDATE ON projects FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('searchtext', 'pg_catalog.english', 'name', 'summary', 'project_key')");

			DB::statement('ALTER TABLE cases ADD COLUMN searchtext TSVECTOR;');
			DB::statement("UPDATE cases SET searchtext = to_tsvector('english', summary || '' || description || '' || shortcut)");
			DB::statement('CREATE INDEX searchtext_gin_cases ON cases USING GIN(searchtext)');
			DB::statement("CREATE TRIGGER ts_searchtext_cases BEFORE INSERT OR UPDATE ON cases FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('searchtext', 'pg_catalog.english', 'summary', 'description', 'shortcut')");

			DB::statement('ALTER TABLE pages ADD COLUMN searchtext TSVECTOR;');
			DB::statement("UPDATE pages SET searchtext = to_tsvector('english', title || '' || description)");
			DB::statement('CREATE INDEX searchtext_gin_pages ON pages USING GIN(searchtext)');
			DB::statement("CREATE TRIGGER ts_searchtext_pages BEFORE INSERT OR UPDATE ON pages FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('searchtext', 'pg_catalog.english', 'title', 'description')");

			DB::statement('ALTER TABLE case_records ADD COLUMN searchtext TSVECTOR;');
			DB::statement("UPDATE case_records SET searchtext = to_tsvector('english', title || '' || description)");
			DB::statement('CREATE INDEX searchtext_gin_records ON case_records USING GIN(searchtext)');
			DB::statement("CREATE TRIGGER ts_searchtext_records BEFORE INSERT OR UPDATE ON case_records FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('searchtext', 'pg_catalog.english', 'title', 'description')");

			DB::statement('ALTER TABLE users ADD COLUMN searchtext TSVECTOR;');
			DB::statement("UPDATE users SET searchtext = to_tsvector('english', firstname || '' || lastname)");
			DB::statement('CREATE INDEX searchtext_gin_users ON users USING GIN(searchtext)');
			DB::statement("CREATE TRIGGER ts_searchtext_users BEFORE INSERT OR UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('searchtext', 'pg_catalog.english', 'firstname', 'lastname')");
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects', function($table) {
			if (DB::getDriverName() === 'mysql') {
				$table->dropIndex('search');
			} elseif (DB::getDriverName() === 'pgsql') {
				DB::statement('DROP TRIGGER ts_searchtext_projects ON projects;');
				$table->dropIndex('searchtext_gin_projects');
				$table->dropColumn('searchtext');
			}
        });

        Schema::table('cases', function($table) {
            if (DB::getDriverName() === 'mysql') {
				$table->dropIndex('search');
			} elseif (DB::getDriverName() === 'pgsql') {
				DB::statement('DROP TRIGGER ts_searchtext_cases ON cases;');
				$table->dropIndex('searchtext_gin_cases');
				$table->dropColumn('searchtext');
			}
        });

        Schema::table('pages', function($table) {
            if (DB::getDriverName() === 'mysql') {
				$table->dropIndex('search');
			} elseif (DB::getDriverName() === 'pgsql') {
				DB::statement('DROP TRIGGER ts_searchtext_pages ON pages;');
				$table->dropIndex('searchtext_gin_pages');
				$table->dropColumn('searchtext');
			}
        });

        Schema::table('case_records', function($table) {
            if (DB::getDriverName() === 'mysql') {
				$table->dropIndex('search');
			} elseif (DB::getDriverName() === 'pgsql') {
				DB::statement('DROP TRIGGER ts_searchtext_records ON case_records;');
				$table->dropIndex('searchtext_gin_records');
				$table->dropColumn('searchtext');
			}
        });

        Schema::table('users', function($table) {
            if (DB::getDriverName() === 'mysql') {
				$table->dropIndex('search');
			} elseif (DB::getDriverName() === 'pgsql') {
				DB::statement('DROP TRIGGER ts_searchtext_users ON users;');
				$table->dropIndex('searchtext_gin_users');
				$table->dropColumn('searchtext');
			}
        });
	}

}
