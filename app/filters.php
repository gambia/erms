<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('setUser', function()
{
    $user = Auth::User();
});

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
    else if (Auth::User()->is_blocked){
        $controller = new UserSessionsController();
        return $controller->destroy()
            ->with('account_blocked', true);
    }
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

Route::filter('auth.perishableToken', function()
{
    $perishable_token = ArrayHelpers::retrieve($_GET, 'perishable_token');

    if($perishable_token){
        $user = User::where('perishable_token', '=', $perishable_token)
            ->first();

        if($user){
            Auth::login($user);
            $user->regeneratePerishableToken();
            $user->save();

            Session::put('logged_in_with_perishable_token', true);

            if (Auth::User()->is_blocked){
                $controller = new UserSessionsController();

                return $controller->destroy()
                    ->with('account_blocked', true);
            }
        }

    }
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
