<?php

class PageComment extends \Eloquent
{
	protected $fillable = [ 'comment' ];
	protected $table = 'page_comments';

	public function page()
	{
		return $this->belongsTo('Page');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

}