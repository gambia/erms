<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Page extends Baum\Node implements SluggableInterface {

    use SluggableTrait;

    const
        TYPE_WIKI = 1,
        TYPE_INTRANET = 2,
        TYPE_FORM = 3;

    protected $fillable = ['project_id', 'title', 'description', 'type', 'is_public'];
    protected $table = 'pages';

    protected $scope = ['client_id'];

    protected $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

    public static $pageTypes = array(
        'wiki' => self::TYPE_WIKI,
        'intranet' => self::TYPE_INTRANET,
        'forms' => self::TYPE_FORM
    );

    public static $pageTypeNames = array(
        self::TYPE_WIKI => 'wiki',
        self::TYPE_INTRANET => 'intranet',
        self::TYPE_FORM => 'forms'
    );

    public static $validationRules = array(
        'title' => 'required',
        'description' => 'required',
        'project_id' => 'required'
    );

    public function project()
    {
        return $this->belongsTo('Project');
    }

    public function parent()
    {
        return $this->belongsTo('Page', 'parent_id');
    }

    public function getTypeName(){
        return static::$pageTypeNames[$this->type];
    }

    public function getTypeNameForEmail($capitalize = false){
        if($this->type === static::TYPE_WIKI){
            return $capitalize ? 'Wiki Page' : 'wiki page';
        }

        if($this->type === static::TYPE_INTRANET){
            return $capitalize ? 'Intranet Page' : 'intranet page';
        }

        if($this->type === static::TYPE_FORM){
            return $capitalize ? 'Form' : 'form';
        }

        throw new Exception('Unknown type: '.$this->type);
    }

    public function getFullName()
    {
        return $this->project->project_key . ' - ' . $this->title;
    }

    public function comments()
    {
        return $this->hasMany('PageComment')->orderBy('updated_at', 'DESC');
    }

    public function files()
    {
        return $this->hasMany('PageFile');
    }

    public function isWiki()
    {
        return $this->type === self::TYPE_WIKI;
    }

    public function isPublicPage()
    {
        return $this->type === self::TYPE_INTRANET;
    }

    public function isForm()
    {
        return $this->type === self::TYPE_FORM;
    }

    public function isInternal()
    {
        return $this->is_public === false;
    }

    public function isPublic()
    {
        return $this->is_public === true;
    }

    public function getHistory()
    {
        $logs = new \Illuminate\Support\Collection([]);

        $pagesLogs = History::whereIn(
            'action',
            [
                History::PAGE_CREATED,
                History::PAGE_UPDATED,
                History::PAGE_COMMENTED,
                History::PAGE_FILE_UPLOADED,
                History::PAGE_FILE_DOWNLOADED,
                History::PAGE_FILE_DELETED
            ]
        )
            ->where(function($query) {
                $query->where(function($q2) {
                    $q2->where('target_class', '=', 'Page')
                        ->where('target_id', '=', $this->id);
                })->orWhere(function($q2) {
                    $q2->where('ref_class', '=', 'Page')
                        ->where('ref_id', '=', $this->id);
                });
            })
            ->with('user')
            ->orderBy('created_at', 'DESC')
            ->get();

        $logs = $logs->merge($pagesLogs);

        $logs = $logs->sort(function($a, $b) {
            return $a->created_at < $b->created_at;
        });

        return $logs;
    }

}
