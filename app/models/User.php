<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

    const NORMAL = 0;
    const ADMIN = 1;
    const CLIENT_ADMIN = 2;

    private $avatarTypeDefinitions = [
        'thumbnail' => [115, 115]
    ];

    protected $table = 'users';
    protected $hidden = array('password', 'remember_token');
    protected $fillable = array(
        'firstname', 'lastname', 'job_title', 'email', 'client_id', 'profile_picture', 'admin_level'
    );

    //Validations
    public static $createRules = array(
        'firstname' => 'required',
        'lastname' => 'required',
        'job_title' => 'required',
        'email' => 'required|email|unique:users',
        'client_id' => 'required|numeric'
    );

    public static $changePasswordRules = [
        'password' => 'required|min:8|confirmed'
    ];

    public function get_update_rules(){
        return array(
            'firstname' => 'required',
            'lastname' => 'required',
            'job_title' => 'required',
            'email' => 'email|unique:users,email,'.$this->id,
            'client_id' => 'required|numeric'
        );
    }

    protected $accessibleProjects = null;

    //Validations

    //Observers
    public static function boot(){
        parent::boot();
        self::observe(new UserObserver);
    }
    //Observers

    //Associations
    public function userSecurityGroups(){
        return $this->hasMany('UserSecurityGroup');
    }

    public function securityGroups(){
        return $this->belongsToMany(
            'SecurityGroup',
            'user_security_groups',
            'user_id',
            'security_group_id'
        );
    }

    public function projectManagers(){
        return $this->hasMany('ProjectManager');
    }

    public function client() {
        return $this->belongsTo('Client');
    }

    public function activities(){
        return $this->hasMany('History', 'user_id');
    }
    //Associations

    public function hasAccessToProject($project)
    {
        if ($this->isAdminOrClientAdmin()) {
            return true;
        }

        $projects = $this->getAccessibleProjects();
        return in_array($project->id, $projects);
    }

    protected function getAccessibleProjects()
    {
        if ($this->accessibleProjects !== NULL) {
            return $this->accessibleProjects;
        }

        $securityGroupIds = $this->securityGroups->lists('id');

        $projectIdsFromSecurityGroups = [];

        if(count($securityGroupIds) > 0){
            $this->accessibleProjects = ProjectSecurityGroup::whereIn(
                'security_group_id',
                $securityGroupIds
            )->lists('project_id');
        } else {
            $this->accessibleProjects = [];
        }

        return $this->getAccessibleProjects();

    }

    public function regeneratePerishableToken(){
        $this->perishable_token = Hash::make($this->email.time());
    }

    public function generateNewPassword(){
        $plainTextPassword = Hash::make($this->email.time());
        $plainTextPassword = substr($plainTextPassword, 10, 6);
        $this->password = Hash::make($plainTextPassword);

        return $plainTextPassword;
    }

    public function projectsQueryBuilder(){
        if ($this->isClientAdmin() || $this->isAdmin()) {
            return Project::where('client_id', '=', $this->client_id);
        }

        $securityGroupIds = $this->securityGroups->lists('id');

        $projectIdsFromSecurityGroups = [];
        if(count($securityGroupIds) > 0){
            $projectIdsFromSecurityGroups = ProjectSecurityGroup::whereIn(
                'security_group_id',
                $securityGroupIds
            )->lists('project_id');
        }

        $projectIdsAsManager = $this->projectManagers()->lists('project_id');

        $projectIds = array_merge(
            $projectIdsFromSecurityGroups,
            $projectIdsAsManager
        );

        if(count($projectIds) == 0){
            return Project::where('id', '=', 0);
        }

        return Project::whereIn('id', $projectIds);
    }

    public function projects() {
        return $this->projectsQueryBuilder()->where('depth', '>', 0)->get();
    }

    public function profileProjects() {
        return $this->projectsQueryBuilder()->where('depth', '=', 1)->orderBy('lft', 'asc')->get();
    }

    public function managedProjects() {
        if ($this->projectManagers()->count()) {
            return Project::whereIn('id', $this->projectManagers->lists('project_id'))->get();
        }
        return new \Illuminate\Database\Eloquent\Collection();
    }

    public function isManager(){
        return $this->projectManagers()
            ->where('project_manager.is_main_manager', '=', true)
            ->count() > 0;
    }

    public function isManagerRepresentative(){
        return $this->projectManagers()
            ->where('project_manager.is_main_manager', '=', false)
            ->count() > 0;
    }

    public function canEditProject()
    {
        return $this->isManagerOrRepresentative() || $this->isClientAdmin() || $this->isAdmin();
    }

    public function isManagerOrRepresentative(){
        return $this->projectManagers()
            ->count() > 0;
    }

    public function isManagerOf($project){
        return $this->projectManagers()
            ->where('project_manager.is_main_manager', '=', true)
            ->where('project_manager.project_id', '=', $project->id)
            ->count() > 0;
    }

    public function isManagerRepresentativeOf($project){
        return $this->projectManagers()
            ->where('project_manager.is_main_manager', '=', false)
            ->where('project_manager.project_id', '=', $project->id)
            ->count() > 0;
    }

    public function isManagerOrRepresentativeOf($project){
        return $this->projectManagers()
            ->where('project_manager.project_id', '=', $project->id)
            ->count() > 0;
    }

    public function isNormalUser(){
        return $this->admin_level == static::NORMAL;
    }

    public function isAdmin(){
        return $this->admin_level == static::ADMIN;
    }

    public function isClientAdmin(){
        return $this->admin_level == static::CLIENT_ADMIN;
    }

    public function isAdminOrClientAdmin() {
        return ($this->isAdmin() || $this->isClientAdmin());
    }

    public static function createAndUpload($data){
        $user = null;

        DB::transaction(function() use (&$user, $data) {
            $avatar = \ArrayHelpers::pop($data, 'profile_picture');
            $user = User::create($data);

            if($avatar){
                $user->uploadAvatar($avatar);
                $user->save();
            }
        });

        return $user;
    }

    public function updateAndUpload($data){
        DB::transaction(function() use ($data) {
            $avatar = \ArrayHelpers::pop($data, 'profile_picture');
            $this->update($data);

            if($avatar){
                $this->uploadAvatar($avatar);
                $this->save();
            }
        });
    }

    public function uploadAvatar($avatar){
        if(!$this->id || !$this->client_id) {
            throw new Exception('User must have id and client_id first');
        }

        $manipulator = new ImageManipulator($avatar);

        $desiredSize = $this->avatarTypeDefinitions['thumbnail'];

        $manipulator->cropMiddleSquare()->resample($desiredSize[0], $desiredSize[1], true);

        $path = 'app/storage/uploadedFiles/'.uniqid();
        $manipulator->save($path);

        $thumbnail = new Symfony\Component\HttpFoundation\File\UploadedFile(
            $path,
            'thumbnail'
        );

        $this->avatar_uri = S3StorageEngine::write(
            'clients/'. $this->client_id .'/users/avatars',
            $avatar,
            $this->id
        );

        $this->avatar_thumbnail_uri = S3StorageEngine::write(
            'clients/'. $this->client_id .'/users/avatars',
            $thumbnail,
            $this->id.'_thumbnail'
        );

        unlink($path);
    }

    public function getAvatarLink($defaultLink, $type = 'thumbnail'){
        $avatarMethod = 'avatar_'.$type.'_uri';

        if($this->$avatarMethod){
            return S3StorageEngine::getFileLink($this->$avatarMethod);
        }

        if($this->avatar_uri){
            return S3StorageEngine::getFileLink($this->avatar_uri);
        }

        return $defaultLink;
    }

    public function createdRecordsCount(){
        return $this->activityCount(History::RECORD_CREATED);
    }

    public function closedCasesCount(){
        return $this->activityCount(History::CASE_CLOSED);
    }

    private function activityCount($activity){
        return $this->activities()
            ->where('action', '=', $activity)
            ->count();
    }

    public function lastLogin(){
        $lastLogin = null;

        $lastLoginHistory = $this->activities()
            ->where('action', '=', History::USER_LOGIN_SUCCESS)
            ->orderBy('created_at', 'desc')
            ->limit(2)  //without offset it would be the current login
            ->get();

        if($lastLoginHistory->count()){
            if ($lastLoginHistory->count() === 1) {
                $lastLogin = $lastLoginHistory[0]->created_at;
            } else {
                $lastLogin = $lastLoginHistory[1]->created_at;
            }
        }

        return $lastLogin;
    }

    public function latestActivitiesBuilder($count){
        return $this->activities()
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($count);
    }

    public function latestActivitiesExceptBuilder($count, $exceptionList){
        return $this->latestActivitiesBuilder($count)
            ->whereNotIn('action', $exceptionList);
    }

    public function latestActivities($count = 10){
        return $this->latestActivitiesBuilder($count)->get();
    }

    public function latestActivitiesExcept($count = 10, $exceptionList = []){
        return $this->latestActivitiesExceptBuilder($count, $exceptionList)
            ->get();
    }

    public function getTimeOffset(){
        return $this->client ? $this->client->time_offset : 0;
    }

    public function getName() {
        return $this->firstname.' '.$this->lastname;
    }

    public function getNameAttribute() {
        return $this->getName();
    }
}
