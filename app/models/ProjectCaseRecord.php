<?php

class ProjectCaseRecord extends \Eloquent
{
	protected $table = 'case_records';
	protected $fillable = ['title', 'description'];

    public static $validationRules = [
        'title' => 'required'
    ];

	public function projectCase()
	{
		return $this->belongsTo('ProjectCase');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function files()
	{
		return $this->hasMany('ProjectCaseRecordFile')
            ->orderBy('created_at', 'ASC');
	}

	public function getHistory()
    {
        $actions = [
            History::RECORD_CREATED, History::RECORD_UPDATED, History::RECORD_FILE_UPLOADED,
        ];

        return History::whereIn('action', $actions)
            ->with('user')
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
