<?php

class ProjectSecurityGroup extends \Eloquent {

	protected $primaryKey = null;
    public $timestamps = false;
    public $incrementing = false;
    
    //Associations
    public function project(){
        return $this->belongsTo('Project');
    }

    public function securityGroup(){
        return $this->belongsTo('SecurityGroup');
    }
    //Associations

    protected $fillable = [];
}
