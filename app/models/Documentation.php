<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Documentation extends Baum\Node implements SluggableInterface {

	use SluggableTrait;

	protected $table = 'documentation';

	protected $fillable = ['title', 'description', 'is_published'];

	protected $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

	public static $validationRules = array(
        'title' => 'required',
    );

	public function parent()
    {
        return $this->belongsTo('Documentation', 'parent_id');
    }

    public function files()
    {
        return $this->hasMany('DocumentationFile');
    }

	public function published()
	{
		return $this->is_published == true;
	}

}