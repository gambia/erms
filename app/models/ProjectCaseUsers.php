<?php

class ProjectCaseUsers extends \Eloquent {

    protected $primaryKey = null;
    public $timestamps = false;
    public $incrementing = false;
    
    //Associations
    public function projectCase(){
        return $this->belongsTo('ProjectCase');
    }

    public function user(){
        return $this->belongsTo('User');
    }
    //Associations

    protected $fillable = [];
}
