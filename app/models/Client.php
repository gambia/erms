<?php

class Client extends \Eloquent {

    protected $fillable = [
        'name',
        'client_key',
        'about',
        'logo_url',
        'opening_hours',
        'billing_address_line_1',
        'billing_address_line_2',
        'billing_city',
        'billing_state',
        'billing_country',
        'billing_zip',
        'shipping_address_line_1',
        'shipping_address_line_2',
        'shipping_city',
        'shipping_state',
        'shipping_country',
        'shipping_zip',
        'phone',
        'fax',
        'email',
        'website',
        'map_longitude',
        'map_latitude',
        'map_zoom',
        'timezone',
        'enable_intranet',
        'enable_wiki',
        'enable_forms'
    ];

    protected $table = 'clients';

    public static $validationRules = [
        'name' => 'required',
        'client_key' => 'required',
        'admin_firstname' => 'required',
        'admin_lastname' => 'required',
        'admin_job_title' => 'required',
        'admin_email' => 'required'
    ];

    public static $updateValidationRules = [
        'name' => 'required',
        'client_key' => 'required',
    ];

    public function clientAdminsBuilder(){
        return User::where('client_id', '=', $this->id)
            ->where('admin_level', '=', User::CLIENT_ADMIN);
    }

    public function clientAdmins(){
        return $this->clientAdminsBuilder()->get();
    }

}
