<?php

class DocumentationFile extends S3File
{
	protected $table = 'documentation_files';

	public function documentation()
	{
		return $this->belongsTo('Documentation');
	}
}
