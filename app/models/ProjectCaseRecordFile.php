<?php

class ProjectCaseRecordFile extends S3File
{
	protected $table = 'case_record_files';

	public function caseRecord()
	{
		return $this->belongsTo('ProjectCaseRecord', 'project_case_record_id');
	}

}
