<?php

class UserObserver {
    public function saving($user){
        if(!$user->perishable_token){
            $user->regeneratePerishableToken();
        }

        if(!$user->password){
            $password = $user->generateNewPassword();

            MandrillEmailSender::send(
                'send-password',
                [
                    $user->email => [
                        'recipientName' => $user->name,
                        'link' => action('UserSessionsController@create'),
                        'name' => $user->name,
                        'login' => $user->email,
                        'password' => $password
                    ]
                ]
            );

        }
    }
}
