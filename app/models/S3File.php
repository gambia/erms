<?php

abstract class S3File extends \Eloquent
{
	protected $fillable = ['filename', 'url', 'size'];

	public function getFullFilePath()
	{
		return S3StorageEngine::getFileLink($this->url);
	}

	public function getQualifiedFilePath($filename)
	{
		return S3StorageEngine::getFullFileLink($this->url, $filename);
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function getHumanSize()
	{
		if ($this->size >= 1<<30) {
			return number_format($this->size/(1<<30),2)."GB";
		} else if ($this->size >= 1<<20) {
			return number_format($this->size/(1<<20),2)."MB";
		} else if ($this->size >= 1<<10) {
			return number_format($this->size/(1<<10),2)."KB";
		} else {
			number_format($this->size)." bytes";
		}
	}

	public function getDownloads()
	{
		return History::where('target_class', '=', get_class($this))
			->where('target_id', '=', $this->id)
			->whereIn('action', [ History::RECORD_FILE_DOWNLOADED, History::PAGE_FILE_DOWNLOADED ])
			->get();
	}
}