<?php

class ProjectCaseStatus extends \Eloquent
{
	protected $table = 'case_status';

	public $timestamps = false;

	protected $fillable = array(
		'name'
	);

	const
		STATUS_OPEN = 1,
		STATUS_CLOSED = 2;

	public function client()
	{
		return $this->belongsTo('Client');
	}
}