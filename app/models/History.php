<?php

class History extends \Eloquent {
    const USER_LOGIN_SUCCESS   = 100;
    const USER_LOGIN_FAILED   = 101;
    const USER_LOGOUT         = 102;

    const USER_PASSWORD_CHANGED       = 103;
    const USER_PASSWORD_RESET_INIT    = 104;

    const USER_CREATED        = 110;
    const USER_UPDATED        = 111;
    const USER_BLOCKED        = 113;
    const USER_UNBLOCKED      = 114;

    const SECURITY_GROUP_CREATED   = 200;
    const SECURITY_GROUP_UPDATED   = 201;
    const SECURITY_GROUP_USER_ADDED     = 202;
    const SECURITY_GROUP_USER_REMOVED   = 203;

    const PROJECT_CREATED     = 300;
    const PROJECT_UPDATED     = 301;

    const CASE_CREATED        = 400;
    const CASE_UPDATED        = 401;
    const CASE_COMMENTED      = 402;
    const CASE_CLOSED         = 403;
    const CASE_DELETED        = 404;

    const RECORD_CREATED      = 500;
    const RECORD_UPDATED      = 501;
    const RECORD_FILE_UPLOADED = 502;
    const RECORD_FILE_DOWNLOADED = 503;
    const RECORD_FILE_DELETED = 504;
    const RECORD_CHUNK_UPLOAD_FINISHED = 505;
    const RECORD_DELETED      = 506;

    const PAGE_CREATED      = 600;
    const PAGE_UPDATED      = 601;
    const PAGE_FILE_UPLOADED = 602;
    const PAGE_FILE_DOWNLOADED = 603;
    const PAGE_COMMENTED = 604;
    const PAGE_FILE_DELETED = 605;
    const PAGE_CHUNK_UPLOAD_FINISHED = 606;
    const PAGE_DELETED      = 607;

    protected $fillable = [
        'action',
        'target_id',
        'target_class',
        'target_content',
        'ref_id',
        'ref_class',
        'ref_content',
        'additional_data',
        'ip_address',
        'user_agent',
        'user_id',
        'client_id'
    ];

    public static $actionMapping = [
        self::USER_LOGIN_SUCCESS   => 'Login success',
        self::USER_LOGIN_FAILED   => 'Login failed',
        self::USER_LOGOUT         => 'Logout',

        self::USER_PASSWORD_CHANGED       => 'Password changed',
        self::USER_PASSWORD_RESET_INIT    => 'Password reset initialized',

        self::USER_CREATED        => 'User created',
        self::USER_UPDATED        => 'User updated',
        self::USER_BLOCKED        => 'User blocked',
        self::USER_UNBLOCKED      => 'User unblocked',

        self::SECURITY_GROUP_CREATED   => 'Security group created',
        self::SECURITY_GROUP_UPDATED   => 'Security group updated',
        self::SECURITY_GROUP_USER_ADDED     => 'Security group user added',
        self::SECURITY_GROUP_USER_REMOVED   => 'Security group user removed',

        self::PROJECT_CREATED     => 'Project created',
        self::PROJECT_UPDATED     => 'Project updated',

        self::CASE_CREATED        => 'Case created',
        self::CASE_UPDATED        => 'Case updated',
        self::CASE_COMMENTED      => 'Case commented',
        self::CASE_CLOSED         => 'Case closed',
        self::CASE_DELETED         => 'Case deleted',

        self::RECORD_CREATED      => 'Record created',
        self::RECORD_UPDATED      => 'Record update',
        self::RECORD_FILE_UPLOADED => 'Record file uploaded',
        self::RECORD_CHUNK_UPLOAD_FINISHED => 'Record chunk file finished',
        self::RECORD_FILE_DOWNLOADED => 'Record file downloaded',
        self::RECORD_FILE_DELETED   => 'Record file deleted',
        self::RECORD_DELETED        => 'Record deleted',

        self::PAGE_CREATED          => 'Page created',
        self::PAGE_UPDATED          => 'Page updated',
        self::PAGE_FILE_UPLOADED    => 'Page file uploaded',
        self::PAGE_FILE_DOWNLOADED  => 'Page file downloaded',
        self::PAGE_FILE_DELETED     => 'Page file deleted',
        self::PAGE_COMMENTED        => 'Page commented',
        self::PAGE_CHUNK_UPLOAD_FINISHED => 'Page attachments uploaded',
        self::PAGE_DELETED        => 'Page deleted',
    ];

    protected $table = 'history';

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function client()
    {
        return $this->belongsTo('Client');
    }

    public static function log($data)
    {
        if(Auth::user() && Auth::user()->isAdmin()){ return; }

        $record = new History();
        $record->fill($data);
        $record->user_id = Auth::user() ? Auth::user()->id : null;
        $record->user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'CLI';
        $record->ip_address = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'CLI';

        if (!isset($data['client_id'])) {
            $record->client_id = Auth::user() ? Auth::user()->client_id : null;
        }

        $record->save();
    }

}
