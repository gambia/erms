<?php

class ProjectManager extends \Eloquent {

    protected $fillable = ['is_main_manager', 'user_id'];
    protected $table = 'project_manager';

    protected $primaryKey = null;
    public $timestamps = false;
    public $incrementing = false;

    public function project()
    {
        return $this->belongsTo('Project');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

}
