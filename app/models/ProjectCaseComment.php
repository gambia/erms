<?php

class ProjectCaseComment extends \Eloquent
{
	protected $fillable = [ 'comment' ];
	protected $table = 'case_comments';

	public function projectCase()
	{
		return $this->belongsTo('ProjectCase');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

}
