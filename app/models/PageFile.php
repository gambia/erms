<?php

class PageFile extends S3File
{
	protected $table = 'page_files';

	public function page()
	{
		return $this->belongsTo('Page');
	}
}
