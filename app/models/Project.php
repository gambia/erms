<?php

class Project extends Baum\Node {

    protected $fillable = ['name', 'project_key', 'summary', 'enable_cases', 'enable_wiki', 'enable_pages', 'enable_forms', 'theme_color', 'last_case_id'];
    protected $table = 'projects';

    protected $parentColumn = 'parent_project_id';

    protected $scope = ['client_id'];

    public static $validationRules = array(
        'name' => 'required',
        'security_groups' => 'required'
    );

    public static function boot()
    {
        parent::boot();

        Project::updated(function($project) {
            $original = $project->getOriginal();
            if ($original['project_key'] !== $project->project_key) {

                DB::statement(DB::raw("UPDATE cases SET shortcut = REPLACE(shortcut, '".$original['project_key']."', '".$project->project_key."') WHERE project_id = " . $project->id));
            }
        });
    }

    public function managers()
    {
        return $this->hasMany('ProjectManager');
    }

    public function securityGroups()
    {
        return $this->belongsToMany(
            'SecurityGroup',
            'project_security_groups',
            'project_id',
            'security_group_id'
        );
    }

    public function usersQueryBuilder(){
        $securityGroupIds = $this->securityGroups->lists('id');

        if(count($securityGroupIds) == 0){
            return User::where('id', '=', 0);
        }

        return User::join(
            'user_security_groups',
            'user_security_groups.user_id',
            '=',
            'users.id'
        )
            ->whereIn(
                'user_security_groups.security_group_id',
                $securityGroupIds
            )
            ->select('users.*');
    }

    public function emailRecipients(){
        $admins = $this->client->clientAdmins();
        return $admins->merge($this->usersQueryBuilder()->get());
    }

    public function subprojects()
    {
        return $this->hasMany('Project', 'parent_project_id');
    }

    public function parentProject()
    {
        return $this->belongsTo('Project', 'parent_project_id');
    }

    public static function root()
    {
        return $this->isRoot() ? $this : $this->parentProject->root();
    }

    public function isRoot()
    {
        return !!$this->parent_project_id;
    }

    public function client()
    {
        return $this->belongsTo('Client');
    }

    public function cases()
    {
        return $this->hasMany('ProjectCase');
    }

    public function pages()
    {
        return $this->hasMany('Page');
    }


    // --------------------------------------------------------------------------
    // Getters and helpers
    // --------------------------------------------------------------------------

    public function hasEnabledCases()
    {
        return $this->enable_cases == 1;
    }

    public function getAllCases()
    {

        $cases = DB::table('projects')
                    ->whereBetween('lft', array($this->lft, $this->rgt))
                    ->join('cases', 'projects.id', '=', 'cases.project_id')
                    ->get();

        // var_dump($cases);

        return $cases;
    }

    public function isInSecurityGroup($group) {
        $groups = $this->securityGroups->filter(function($item) use ($group) {
            return $item->id === $group->id;
        });
        return $groups->count() === 1;
    }

    public function hasManager($manager) {
        $managers = $this->managers->filter(function($item) use ($manager) {
            return !$item->is_main_manager && $item->user->id === $manager->id;
        });
        return $managers->count() === 1;
    }

    public function isMainManager($manager) {
        $managers = $this->managers->filter(function($item) use ($manager) {
            return $item->is_main_manager && $item->user->id === $manager->id;
        });
        return $managers->count() === 1;
    }

    public function scopeBySecurityGroups($query){

        return $query
            ->select('projects.*')
            ->leftJoin('project_security_groups', 'project_security_groups.project_id', '=', 'projects.id')
            ->leftJoin('user_security_groups', 'project_security_groups.security_group_id', '=', 'user_security_groups.security_group_id')
            ->where('user_security_groups.user_id', '=', Auth::user()->id);
    }

    public function getFullName()
    {
        return $this->name . ' (' . $this->project_key . ')';
    }

    public function getHistory()
    {
        $projects = $this->getDescendantsAndSelf();
        $user = Auth::user();
        $projectLogs = History::whereIn('action', [History::PROJECT_CREATED, History::PROJECT_UPDATED])
            ->where('target_class', '=', 'Project')
            ->whereIn('target_id', $this->getDescendantsAndSelf()->lists('id'))
            ->with('user')
            ->orderBy('created_at', 'DESC')
            ->get();

        $logs = new \Illuminate\Support\Collection([]);
        $logs = $logs->merge($projectLogs);

        $cases = Policies\ProjectCasePolicy::authorizedRecordsByProjectIds(
            Auth::user(), $projects->lists('id')
        )->get();

        if (count($cases)) {
            $casesLogs = History::whereIn('action', [History::CASE_CREATED, History::CASE_UPDATED, History::CASE_COMMENTED, History::CASE_CLOSED])
                ->where(function($query) use ($cases) {
                    $query->where(function($q2) use ($cases) {
                        $q2->where('target_class', '=', 'ProjectCase')
                            ->whereIn('target_id', $cases->lists('id'));
                    })->orWhere(function($q2) use ($cases) {
                        $q2->where('ref_class', '=', 'ProjectCase')
                            ->whereIn('ref_id', $cases->lists('id'));
                    });
                })
                ->with('user')
                ->orderBy('created_at', 'DESC')
                ->get();

            $logs = $logs->merge($casesLogs);

            $records = ProjectCaseRecord::whereIn('project_case_id', $cases->lists('id'))->get();
            if (count($records)) {
                $recordsLogs = History::whereIn('action', [History::RECORD_CREATED, History::RECORD_UPDATED, History::RECORD_FILE_UPLOADED, History::RECORD_FILE_DOWNLOADED])
                    ->where(function($query) use ($records) {
                        $query->where(function($q2) use ($records) {
                            $q2->where('target_class', '=', 'ProjectCaseRecord')
                                ->whereIn('target_id', $records->lists('id'));
                        })->orWhere(function($q2) use ($records) {
                            $q2->where('ref_class', '=', 'ProjectCaseRecord')
                                ->whereIn('ref_id', $records->lists('id'));
                        });
                    })
                    ->with('user')
                    ->orderBy('created_at', 'DESC')
                    ->get();

                $logs = $logs->merge($recordsLogs);
            }
        }

        $pages = new \Illuminate\Support\Collection([]);
        foreach ($projects as $proj) {
            $pages = $pages->merge($proj->pages);
        }

        if (count($pages)) {
            $pagesLogs = History::whereIn('action', [History::PAGE_CREATED, History::PAGE_UPDATED, History::PAGE_COMMENTED, History::PAGE_FILE_UPLOADED, History::PAGE_FILE_DOWNLOADED])
                ->where(function($query) use ($pages) {
                    $query->where(function($q2) use ($pages) {
                        $q2->where('target_class', '=', 'Page')
                            ->whereIn('target_id', $pages->lists('id'));
                    })->orWhere(function($q2) use ($pages) {
                        $q2->where('ref_class', '=', 'Page')
                            ->whereIn('ref_id', $pages->lists('id'));
                    });
                })
                ->with('user')
                ->orderBy('created_at', 'DESC')
                ->get();

            $logs = $logs->merge($pagesLogs);
        }

        $logs = $logs->sort(function($a, $b) {
            return $a->created_at < $b->created_at;
        });

        return $logs;
    }

}
