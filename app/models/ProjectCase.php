<?php

use Policies\ProjectPolicy;

class ProjectCase extends \Eloquent {

    public static $priorities = [0, 1, 2, 3, 4, 5];

    protected $fillable = ['project_id', 'summary', 'priority','project_case_status_id', 'assigned_to', 'description', 'is_confidential'];
    protected $table = 'cases';
    protected $with = ['project', 'projectCaseStatus', 'assignedTo'];

    public static $sortableColumns = ['id', 'summary', 'assigned_to', 'project_case_status_id', 'priority', 'created_at', 'updated_at'];

    public static $rules = array(
        'summary' => 'required',
        'project_case_status_id' => 'required',
        'assigned_to' => 'required',
        'project_id' => 'required',
        'priority' => 'required'
    );

    public static function boot()
    {
        parent::boot();

        ProjectCase::creating(function($case) {
            $project = $case->project;
            $iterator = $project->last_case_id + 1;
            $case->shortcut = $project->project_key . '-' . $iterator;

            $project->last_case_id = $iterator;
            $project->timestamps = false;   // We don't want to project this change into timestamps
            $project->save();
        });
    }

    public function projectCaseStatus()
    {
        return $this->belongsTo('ProjectCaseStatus');
    }

    public function getFullName()
    {
        return $this->shortcut . ' ' . $this->summary;
    }

    public function project()
    {
        return $this->belongsTo('Project');
    }

    public function privilegedUsers() {
        return $this->belongsToMany('User', 'project_case_users');
    }

    public function assignedTo()
    {
        return $this->belongsTo('User', 'assigned_to');
    }

    public function comments()
    {
        return $this->hasMany('ProjectCaseComment')->orderBy('updated_at', 'DESC');
    }

    public function records()
    {
        return $this->hasMany('ProjectCaseRecord');
    }

    public function getPriorityClassName()
    {
        $classes = array(
            0 => '',
            1 => 'label-danger',
            2 => 'label-warning',
            3 => 'label-primary',
            4 => 'label-success',
            5 => 'label-default'
        );

        return 'label ' . $classes[$this->priority];
    }

    public function getHistory()
    {
        $logs = new \Illuminate\Support\Collection([]);

        $casesLogs = History::whereIn('action', [History::CASE_CREATED, History::CASE_UPDATED, History::CASE_COMMENTED, History::CASE_CLOSED])
            ->where(function($query) {
                $query->where(function($q2) {
                    $q2->where('target_class', '=', 'ProjectCase')
                        ->where('target_id', '=', $this->id);
                })->orWhere(function($q2) {
                    $q2->where('ref_class', '=', 'ProjectCase')
                        ->where('ref_id', '=', $this->id);
                });
            })
            ->with('user')
            ->orderBy('created_at', 'DESC')
            ->get();

        $logs = $logs->merge($casesLogs);

        $records = ProjectCaseRecord::where('project_case_id', '=', $this->id)->get();
        if (count($records) > 0) {
            $recordsLogs = History::whereIn(
                'action',
                [
                    History::RECORD_CREATED,
                    History::RECORD_UPDATED,
                    History::RECORD_FILE_UPLOADED,
                    History::RECORD_FILE_DOWNLOADED,
                    History::RECORD_FILE_DELETED
                ]
            )
                ->where(function($query) use ($records) {
                    $query->where(function($q2) use ($records) {
                        $q2->where('target_class', '=', 'ProjectCaseRecord')
                            ->whereIn('target_id', $records->lists('id'));
                    })->orWhere(function($q2) use ($records) {
                        $q2->where('ref_class', '=', 'ProjectCaseRecord')
                            ->whereIn('ref_id', $records->lists('id'));
                    });
                })
                ->with('user')
                ->orderBy('created_at', 'DESC')
                ->get();

            $logs = $logs->merge($recordsLogs);
        }

        $logs = $logs->sort(function($a, $b) {
            return $a->created_at < $b->created_at;
        });

        return $logs;

    }

    public function emailRecipients() {
        if(!$this->is_confidential){
            return $this->project->emailRecipients();
        }

        $admins = $this->project->client->clientAdmins();

        $result = $this->privilegedUsers()->get()->merge($admins);

        return $result;
    }

}
