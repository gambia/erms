<?php

class SecurityGroup extends \Eloquent {
    protected $fillable = ['client_id', 'name'];

    //Validations
    public static $rules = array(
        'name' => 'required',
        'client_id' => 'required|integer'
    );
    //Validations

    //Associations
    public function userSecurityGroups(){
        return $this->hasMany('UserSecurityGroup');
    }

    public function projectSecurityGroups(){
        return $this->hasMany('ProjectSecurityGroup');
    }

    public function users(){
        return $this->belongsToMany(
            'User',
            'user_security_groups',
            'security_group_id',
            'user_id'
        );
    }

    public function projects(){
        return $this->belongsToMany(
            'Project',
            'project_security_groups',
            'security_group_id',
            'project_id'
        );
    }
    //Associations
}
