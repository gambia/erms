<?php

class UserSecurityGroup extends \Eloquent {
    //Associations
    public function user(){
        return $this->belongsTo('User');
    }

    public function securityGroup(){
        return $this->belongsTo('SecurityGroup');
    }
    //Associations

    protected $fillable = ['user_id', 'security_group_id'];
	public $timestamps = false;
}
